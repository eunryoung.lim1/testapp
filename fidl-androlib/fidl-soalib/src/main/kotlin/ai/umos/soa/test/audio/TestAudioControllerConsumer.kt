/*
 * Copyright (c) 2023 42dot All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ai.umos.soa.test.audio

import android.content.Context
import android.os.IBinder
import android.os.RemoteCallbackList
import android.util.Log

import ai.umos.soa.common.IServiceStatusListener
import ai.umos.soa.test.audio.interfaces.ITestAudioControllerAttrListener
import ai.umos.soa.test.audio.interfaces.ITestAudioControllerAttrAsyncCallback
import ai.umos.soa.test.audio.interfaces.ITestAudioControllerEventListener
import ai.umos.soa.test.audio.interfaces.ITestAudioControllerAsyncCallback
import ai.umos.soa.test.audio.data.*

class TestAudioControllerConsumer {
    private var TAG = "TestAudioControllerConsumer"

    private var mContext : Context
	private var mServiceStatusListener : IServiceStatusListener

    constructor(context: Context, serviceStatusListener: IServiceStatusListener) : super(){
        mContext = context
        mServiceStatusListener = serviceStatusListener
    }
    
    fun startClient() {
        nativeStartClient(mServiceStatusListener)
    }

    fun stopClient() {
        nativeStopClient()
    }

    external fun nativeStartClient(serviceStatusListener: IServiceStatusListener)
	external fun nativeStopClient()
	external fun setAttributeListener(attrListener: ITestAudioControllerAttrListener)
	external fun setBroadcastEventListener(eventListener: ITestAudioControllerEventListener)
	
	external fun setIVIDeviceInfoAttr(newIVIDeviceInfoValue: KeyValueString) : Boolean
	external fun setIVIDeviceInfoAttrAsync(newIVIDeviceInfoValue: KeyValueString, callback: ITestAudioControllerAttrAsyncCallback.IIVIDeviceInfoAttrCallback)
	external fun getIVIDeviceInfoAttr() : KeyValueString
	external fun getIVIDeviceInfoAttrAsync(callback: ITestAudioControllerAttrAsyncCallback.IIVIDeviceInfoAttrCallback)
	external fun getAudioSourceTypesAttr() : Array<String>
	external fun getAudioSourceTypesAttrAsync(callback: ITestAudioControllerAttrAsyncCallback.IAudioSourceTypesAttrCallback)
	external fun getVolumeInfoAttr() : VolumeInfo
	external fun getVolumeInfoAttrAsync(callback: ITestAudioControllerAttrAsyncCallback.IVolumeInfoAttrCallback)
	external fun getKeyValueByteBufferMapAttr() : KeyValueByteBuffer
	external fun getKeyValueByteBufferMapAttrAsync(callback: ITestAudioControllerAttrAsyncCallback.IKeyValueByteBufferMapAttrCallback)
	external fun getDeviceStateMapAttr() : DeviceStateMap
	external fun getDeviceStateMapAttrAsync(callback: ITestAudioControllerAttrAsyncCallback.IDeviceStateMapAttrCallback)
	external fun getStringToShortMapAttr() : StringToShortMap
	external fun getStringToShortMapAttrAsync(callback: ITestAudioControllerAttrAsyncCallback.IStringToShortMapAttrCallback)
	external fun obtainFocus(audioSourceType: String, device: AudioDeviceType) : ReplyObtainFocus
	external fun obtainFocusAsync(audioSourceType: String, device: AudioDeviceType, callback: ITestAudioControllerAsyncCallback.IObtainFocusCallback)
	external fun releaseFocus(audioSourceType: String, device: AudioDeviceType) : FocusControlError
	external fun releaseFocusAsync(audioSourceType: String, device: AudioDeviceType, callback: ITestAudioControllerAsyncCallback.IReleaseFocusCallback)
	external fun setVolume(volume: Byte) : VolumeControlError
	external fun setVolumeAsync(volume: Byte, callback: ITestAudioControllerAsyncCallback.ISetVolumeCallback)
	external fun setSourceVolume(audioSourceType: String, volume: Short, maxVolume: Int) : VolumeControlError
	external fun setSourceVolumeAsync(audioSourceType: String, volume: Short, maxVolume: Int, callback: ITestAudioControllerAsyncCallback.ISetSourceVolumeCallback)
	external fun getAudioSourceStatus(audioSourceType: String) : ReplyGetAudioSourceStatus
	external fun getAudioSourceStatusAsync(audioSourceType: String, callback: ITestAudioControllerAsyncCallback.IGetAudioSourceStatusCallback)
}
