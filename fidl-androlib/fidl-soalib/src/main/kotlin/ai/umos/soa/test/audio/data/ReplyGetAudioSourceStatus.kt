
/*
* Copyright (c) 2023 42dot All rights reserved.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package ai.umos.soa.test.audio.data

import ai.umos.soa.test.audio.data.AudioDeviceType
import ai.umos.soa.test.audio.data.AudioFocusResultType
import ai.umos.soa.test.audio.data.AudioFocusStatusType
import ai.umos.soa.test.audio.data.AudioSourceStatusInfo
import ai.umos.soa.test.audio.data.ErrorType
import ai.umos.soa.test.audio.data.FocusControlError
import ai.umos.soa.test.audio.data.FocusStatus
import ai.umos.soa.test.audio.data.VolumeInfo

import android.os.Build
import android.os.Parcel
import android.os.Parcelable

data class ReplyGetAudioSourceStatus constructor(
	val errErrorType: ErrorType,
	val replyAudioSourceStatusInfo: AudioSourceStatusInfo
) : Parcelable {

    @Suppress("DEPRECATION")
    constructor(parcel: Parcel) : this(
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
		    parcel.readParcelable<ErrorType>(ErrorType::class.java.classLoader, ErrorType::class.java)!!
		} else {
		    parcel.readParcelable<ErrorType>(ErrorType::class.java.classLoader)!!
		}, 
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
		    parcel.readParcelable<AudioSourceStatusInfo>(AudioSourceStatusInfo::class.java.classLoader, AudioSourceStatusInfo::class.java)!!
		} else {
		    parcel.readParcelable<AudioSourceStatusInfo>(AudioSourceStatusInfo::class.java.classLoader)!!
		}
	)

    companion object CREATOR : Parcelable.Creator<ReplyGetAudioSourceStatus> {
        override fun createFromParcel(parcel: Parcel): ReplyGetAudioSourceStatus {
            return ReplyGetAudioSourceStatus(parcel)
        }

        override fun newArray(size: Int): Array<ReplyGetAudioSourceStatus?> {
            return arrayOfNulls(size)
        }
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeParcelable(errErrorType, flags)
		parcel.writeParcelable(replyAudioSourceStatusInfo, flags)
	}

    override fun describeContents(): Int {
        return 0
    }

    override fun toString(): String {
        return "@TODO"
    }
}
