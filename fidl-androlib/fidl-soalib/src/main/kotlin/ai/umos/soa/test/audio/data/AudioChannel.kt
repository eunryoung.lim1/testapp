
/*
* Copyright (c) 2023 42dot All rights reserved.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package ai.umos.soa.test.audio.data

import android.os.Parcel
import android.os.Parcelable

enum class AudioChannel constructor(
    val id: Int
) : Parcelable {
	MONO(0),
	STEREO(1),
	FIVE_1(2),
	SEVEN_1(3);

    constructor(parcel: Parcel) : this(parcel.readInt())

    companion object CREATOR : Parcelable.Creator<AudioChannel> {
        override fun createFromParcel(parcel: Parcel): AudioChannel {
            return values()[parcel.readInt()]
        }

        override fun newArray(size: Int): Array<AudioChannel?> {
            return arrayOfNulls(size)
        }
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun toString(): String {
        return "AudioChannel={$id}"
    }
}
