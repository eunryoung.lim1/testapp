
/*
* Copyright (c) 2023 42dot All rights reserved.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package ai.umos.soa.test.audio.data

import ai.umos.soa.test.audio.data.FocusStatus
import ai.umos.soa.test.audio.data.VolumeInfo

import android.os.Build
import android.os.Parcel
import android.os.Parcelable

data class AudioSourceStatusInfo constructor(
	val audioSourceType: String,
	val focusStatus: FocusStatus,
	val volumeInfo: VolumeInfo
) : Parcelable {

    @Suppress("DEPRECATION")
    constructor(parcel: Parcel) : this(
		parcel.readString() ?: "", 
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
		    parcel.readParcelable<FocusStatus>(FocusStatus::class.java.classLoader, FocusStatus::class.java)!!
		} else {
		    parcel.readParcelable<FocusStatus>(FocusStatus::class.java.classLoader)!!
		}, 
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
		    parcel.readParcelable<VolumeInfo>(VolumeInfo::class.java.classLoader, VolumeInfo::class.java)!!
		} else {
		    parcel.readParcelable<VolumeInfo>(VolumeInfo::class.java.classLoader)!!
		}
	)

    companion object CREATOR : Parcelable.Creator<AudioSourceStatusInfo> {
        override fun createFromParcel(parcel: Parcel): AudioSourceStatusInfo {
            return AudioSourceStatusInfo(parcel)
        }

        override fun newArray(size: Int): Array<AudioSourceStatusInfo?> {
            return arrayOfNulls(size)
        }
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeString(audioSourceType)
		parcel.writeParcelable(focusStatus, flags)
		parcel.writeParcelable(volumeInfo, flags)
	}

    override fun describeContents(): Int {
        return 0
    }

    override fun toString(): String {
        return "@TODO"
    }
}
