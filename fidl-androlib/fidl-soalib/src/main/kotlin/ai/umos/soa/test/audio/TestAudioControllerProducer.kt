/*
 * Copyright (c) 2023 42dot All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ai.umos.soa.test.audio

import android.content.Context
import android.util.Log

import ai.umos.soa.test.audio.interfaces.ITestAudioControllerHandler
import ai.umos.soa.test.audio.data.*

class TestAudioControllerProducer {
    private var TAG = "TestAudioControllerProducer"

    private var mContext : Context
	private var mHandler : ITestAudioControllerHandler

    constructor(context: Context, handler: ITestAudioControllerHandler) : super(){
        mContext = context
        mHandler = handler
    }


    fun startService() {
        nativeStartService(mHandler)
    }

    fun stopService() {
        nativeStopService()
    }

    external fun nativeStartService(handler : ITestAudioControllerHandler)
	external fun nativeStopService()
	external fun updateIVIDeviceInfoAttr(updateIVIDeviceInfoValue: KeyValueString)
	external fun updateAudioSourceTypesAttr(updateAudioSourceTypesArrayValue: Array<String>)
	external fun updateVolumeInfoAttr(updateVolumeInfoValue: VolumeInfo)
	external fun updateKeyValueByteBufferMapAttr(updateKeyValueByteBufferMapValue: KeyValueByteBuffer)
	external fun updateDeviceStateMapAttr(updateDeviceStateMapValue: DeviceStateMap)
	external fun updateStringToShortMapAttr(updateStringToShortMapValue: StringToShortMap)
	external fun fireFocusStatusChanged(focusStatusArray: Array<FocusStatus>)
}

