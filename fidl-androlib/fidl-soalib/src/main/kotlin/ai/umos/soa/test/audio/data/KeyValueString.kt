
/*
* Copyright (c) 2023 42dot All rights reserved.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package ai.umos.soa.test.audio.data

import android.os.Parcel
import android.os.Parcelable

data class KeyValueString constructor(
    val mapData: HashMap<String, String>
) : Parcelable {
    constructor(parcel: Parcel) : this(
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.TIRAMISU) {
            parcel.readHashMap(String::class.java.classLoader) as HashMap<String, String>?
        } else {
            parcel.readSerializable() as HashMap<String, String>?
        } ?: HashMap()
    )

    companion object CREATOR : Parcelable.Creator<KeyValueString> {
        override fun createFromParcel(parcel: Parcel): KeyValueString {
            return KeyValueString(parcel)
        }

        override fun newArray(size: Int): Array<KeyValueString?> {
            return arrayOfNulls(size)
        }
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeSerializable(mapData)
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun toString(): String {
        return "@TODO"
    }
}
