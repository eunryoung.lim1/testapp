/*
* Copyright (c) 2023 42dot All rights reserved.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package ai.umos.soa.test.audio.interfaces

import ai.umos.soa.test.audio.data.*
import ai.umos.soa.common.AsyncCallStatus

abstract class ITestAudioControllerAsyncCallback {
	fun interface IObtainFocusCallback {
		fun onObtainFocus(callStatus: AsyncCallStatus, errFocusControlError: FocusControlError, replyRes: AudioFocusResultType)
	}
	
	fun interface IReleaseFocusCallback {
		fun onReleaseFocus(callStatus: AsyncCallStatus, errFocusControlError: FocusControlError)
	}
	
	fun interface ISetVolumeCallback {
		fun onSetVolume(callStatus: AsyncCallStatus, errVolumeControlError: VolumeControlError)
	}
	
	fun interface ISetSourceVolumeCallback {
		fun onSetSourceVolume(callStatus: AsyncCallStatus, errVolumeControlError: VolumeControlError)
	}
	
	fun interface IGetAudioSourceStatusCallback {
		fun onGetAudioSourceStatus(callStatus: AsyncCallStatus, errErrorType: ErrorType, replyAudioSourceStatusInfo: AudioSourceStatusInfo)
	}
}
