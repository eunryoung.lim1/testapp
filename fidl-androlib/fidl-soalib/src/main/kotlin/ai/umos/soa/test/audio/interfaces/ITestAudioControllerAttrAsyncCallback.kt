/*
* Copyright (c) 2023 42dot All rights reserved.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package ai.umos.soa.test.audio.interfaces

import ai.umos.soa.test.audio.data.*
import ai.umos.soa.common.AsyncCallStatus

abstract class ITestAudioControllerAttrAsyncCallback {
	fun interface IIVIDeviceInfoAttrCallback {
		fun onIVIDeviceInfoAttr(callStatus: AsyncCallStatus, iVIDeviceInfo: KeyValueString)
	}
	
	fun interface IAudioSourceTypesAttrCallback {
		fun onAudioSourceTypesAttr(callStatus: AsyncCallStatus, audioSourceTypesArray: Array<String>)
	}
	
	fun interface IVolumeInfoAttrCallback {
		fun onVolumeInfoAttr(callStatus: AsyncCallStatus, volumeInfo: VolumeInfo)
	}
	
	fun interface IKeyValueByteBufferMapAttrCallback {
		fun onKeyValueByteBufferMapAttr(callStatus: AsyncCallStatus, keyValueByteBufferMap: KeyValueByteBuffer)
	}
	
	fun interface IDeviceStateMapAttrCallback {
		fun onDeviceStateMapAttr(callStatus: AsyncCallStatus, deviceStateMap: DeviceStateMap)
	}
	
	fun interface IStringToShortMapAttrCallback {
		fun onStringToShortMapAttr(callStatus: AsyncCallStatus, stringToShortMap: StringToShortMap)
	}
}
