/*
* Copyright (c) 2023 42dot All rights reserved.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package ai.umos.soa.test.audio.interfaces

import ai.umos.soa.test.audio.data.*

interface ITestAudioControllerHandler {
	fun onObtainFocus(audioSourceType: String, device: AudioDeviceType): ReplyObtainFocus
	fun onReleaseFocus(audioSourceType: String, device: AudioDeviceType): FocusControlError
	fun onSetVolume(volume: Byte): VolumeControlError
	fun onSetSourceVolume(audioSourceType: String, volume: Short, maxVolume: Int): VolumeControlError
	fun onGetAudioSourceStatus(audioSourceType: String): ReplyGetAudioSourceStatus
}
