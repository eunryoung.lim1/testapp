/*
 * Copyright (c) 2023 42dot All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <logging.h>
#include <jni.h>

#include "JNIServiceStatusListener.h"

#include <common/JNIHelper.h>

JNIServiceStatusListener::JNIServiceStatusListener(JNIEnv *env, jobject callback)
    : mEnv(env) {
    LOGI("JNIServiceStatusListener::JNIServiceStatusListener(), mEnv:%p", mEnv);

    mCallbackObj = mEnv->NewGlobalRef(callback);
    jint attachResult = mEnv->GetJavaVM(&mJavaVM);
    if (attachResult != JNI_OK) {
        return;
    }

    jclass callbackClass = mEnv->GetObjectClass(mCallbackObj);
	mOnServiceAvailableId = JNIHelper::getMethodId(mEnv, callbackClass, "onServiceAvailable", "()V");
	mOnServiceNotAvailableId = JNIHelper::getMethodId(mEnv, callbackClass, "onServiceNotAvailable", "()V");
}

JNIServiceStatusListener::~JNIServiceStatusListener() {
    LOGI("JNIServiceStatusListener::~JNIServiceStatusListener()");
    mEnv->DeleteGlobalRef(mCallbackObj);
}

void JNIServiceStatusListener::onServiceAvailable() {
    LOGI("JNIServiceStatusListener::onServiceAvailable()");

    JNIEnv* attachEnv;
    if (mJavaVM->AttachCurrentThread(&attachEnv, nullptr) != JNI_OK) {
        return ;
    }


    attachEnv->CallVoidMethod(mCallbackObj, mOnServiceAvailableId);
    mJavaVM->DetachCurrentThread();
}

void JNIServiceStatusListener::onServiceNotAvailable() {
    LOGI("JNIServiceStatusListener::onServiceNotAvailable()");
    JNIEnv* attachEnv;
    if (mJavaVM->AttachCurrentThread(&attachEnv, nullptr) != JNI_OK) {
        return ;
    }

    attachEnv->CallVoidMethod(mCallbackObj, mOnServiceNotAvailableId);
    mJavaVM->DetachCurrentThread();
}