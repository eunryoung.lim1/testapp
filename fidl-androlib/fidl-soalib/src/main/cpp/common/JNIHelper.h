/*
* Copyright (c) 2023 42dot All rights reserved.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#pragma once

#include <logging.h>
#include <jni.h>

class JNIHelper final {
public:
    static jclass findClass(JNIEnv *env, const char *name) {
        if (!env) {
            return nullptr;
        }
        jclass retClass = env->FindClass(name);
        if (retClass == nullptr) {
            LOGI("JNI Error! Failed to find class %s", name);
            checkEnvException(env);
        }

        return retClass;
    }

    static jmethodID getMethodId(JNIEnv *env, jclass clazz, const char *name, const char *signature) {
        if (!env) {
            return nullptr;
        }
        jmethodID retMethodId = env->GetMethodID(clazz, name, signature);
        if (retMethodId == nullptr) {
            LOGI("JNI Error! Failed to find jmethod id (%s, %s)", name, signature);
            checkEnvException(env);
        }

        return retMethodId;
    }

    static jmethodID getStaticMethodId(JNIEnv *env, jclass clazz, const char *name, const char *signature) {
        if (!env) {
            return nullptr;
        }
        jmethodID retMethodId = env->GetStaticMethodID(clazz, name, signature);
        if (retMethodId == nullptr) {
            LOGI("JNI Error! Failed to find static jmethod id (%s, %s)", name, signature);
            checkEnvException(env);
        }

        return retMethodId;
    }

    static jfieldID getFieldId(JNIEnv *env, jclass clazz, const char *name, const char *signature) {
        if (!env) {
            return nullptr;
        }

        jfieldID retFieldId = env->GetFieldID(clazz, name, signature);
        if (retFieldId == nullptr) {
            LOGI("JNI Error! Failed to find jfield id (%s, %s)", name, signature);
            checkEnvException(env);
        }

        return retFieldId;
    }

    static void checkEnvException(JNIEnv *env) {
        if (!env) {
            return ;
        }

        if (env->ExceptionOccurred()) {
            env->ExceptionDescribe();
            env->ExceptionClear();
            // @TODO. make a rule for jni exception handling
            // throw std::exception();
        }
    }

private:
    JNIHelper() = default;
    ~JNIHelper() = default;
};