/*
 * Copyright (c) 2023 42dot All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <logging.h>
#include <jni.h>

#include <SoaServiceManager.h>

#include "JNITestAudioControllerService.h"

extern std::shared_ptr<SoaServiceManager> gSoaServiceManager;

TestAudioControllerStubImplPtr JNITestAudioControllerService::mNativeStub = nullptr;
JNITestAudioControllerHandlerPtr JNITestAudioControllerService::mHandler = nullptr;
JNITestAudioControllerDepsCachePtr JNITestAudioControllerService::mDepsCache = nullptr;

jint JNITestAudioControllerService::initializeTestAudioControllerService(JavaVM* vm) {
    LOGI("JNI::OnLoad initializeTestAudioControllerService");

    JNIEnv* env;
    if (vm->GetEnv(reinterpret_cast<void**>(&env), JNI_VERSION_1_6) != JNI_OK) {
        return JNI_ERR;
    }

    const JNINativeMethod methods[] = {
        {"nativeStartService", "(Lai/umos/soa/test/audio/interfaces/ITestAudioControllerHandler;)V",
			reinterpret_cast<void*>(nativeStartService)},
		{"nativeStopService", "()V",
			reinterpret_cast<void*>(nativeStopService)},
		{"updateIVIDeviceInfoAttr", "(Lai/umos/soa/test/audio/data/KeyValueString;)V",
			reinterpret_cast<void*>(nativeUpdateIVIDeviceInfoAttr)},
		{"updateAudioSourceTypesAttr", "([Ljava/lang/String;)V",
			reinterpret_cast<void*>(nativeUpdateAudioSourceTypesAttr)},
		{"updateVolumeInfoAttr", "(Lai/umos/soa/test/audio/data/VolumeInfo;)V",
			reinterpret_cast<void*>(nativeUpdateVolumeInfoAttr)},
		{"updateKeyValueByteBufferMapAttr", "(Lai/umos/soa/test/audio/data/KeyValueByteBuffer;)V",
			reinterpret_cast<void*>(nativeUpdateKeyValueByteBufferMapAttr)},
		{"updateDeviceStateMapAttr", "(Lai/umos/soa/test/audio/data/DeviceStateMap;)V",
			reinterpret_cast<void*>(nativeUpdateDeviceStateMapAttr)},
		{"updateStringToShortMapAttr", "(Lai/umos/soa/test/audio/data/StringToShortMap;)V",
			reinterpret_cast<void*>(nativeUpdateStringToShortMapAttr)},
		{"fireFocusStatusChanged", "([Lai/umos/soa/test/audio/data/FocusStatus;)V",
			reinterpret_cast<void*>(fireFocusStatusChanged)},
    };

    jclass clazz = env->FindClass("ai/umos/soa/test/audio/TestAudioControllerProducer");
	if (!clazz) {
		return JNI_ERR;
	}

	int result = env->RegisterNatives(clazz, methods, sizeof(methods) / sizeof(methods[0]));
	env->DeleteLocalRef(clazz);
	if (result != 0) {
		return JNI_ERR;
	}
    mDepsCache = std::make_shared<JNITestAudioControllerDepsCache>(env);

    return JNI_VERSION_1_6;
}

void JNITestAudioControllerService::nativeStartService(JNIEnv *env, jobject thiz, jobject handler) {
    if (!gSoaServiceManager) {
        LOGI("Failed to start native client, SoaClientManager is null");
        return;
    }

    mHandler = std::make_shared<JNITestAudioControllerHandler>(env, handler, mDepsCache);
    mNativeStub = gSoaServiceManager->startTestAudioControllerService(mHandler);
}

void JNITestAudioControllerService::nativeStopService(JNIEnv *env, jobject thiz) {
   gSoaServiceManager->stopTestAudioControllerService();
   mHandler.reset();
   mNativeStub.reset();
}

void JNITestAudioControllerService::nativeUpdateIVIDeviceInfoAttr(JNIEnv *env, jobject thiz, jobject jUpdateIVIDeviceInfoValue) {
    if (mNativeStub == nullptr) {
		return;
	}
	jobject jIVIDeviceInfoMapDataObj = env->GetObjectField(jUpdateIVIDeviceInfoValue, mDepsCache->mKeyValueString.mapDataId);
	jobject jIVIDeviceInfoEntrySetObj = env->CallObjectMethod(jIVIDeviceInfoMapDataObj, mDepsCache->mHashMapEntrySetId);
	jobject jIVIDeviceInfoIteratorObj = env->CallObjectMethod(jIVIDeviceInfoEntrySetObj, mDepsCache->mHashMapIteratorId);
	v2_Test_Audio::Controller::KeyValueString newIVIDeviceInfo;
	while (env->CallBooleanMethod(jIVIDeviceInfoIteratorObj, mDepsCache->mMapIteratorHasNextId)) {
		jobject jIVIDeviceInfoEntryObj = env->CallObjectMethod(jIVIDeviceInfoIteratorObj, mDepsCache->mMapIteratorNextId);
		jobject jIVIDeviceInfoEntryObjKey = env->CallObjectMethod(jIVIDeviceInfoEntryObj, mDepsCache->mHashMapGetKeyId);
		jstring jIVIDeviceInfoEntryObjKeyStr = static_cast<jstring>(jIVIDeviceInfoEntryObjKey);
		jobject jIVIDeviceInfoEntryObjValue = env->CallObjectMethod(jIVIDeviceInfoEntryObj, mDepsCache->mHashMapGetValueId);
		jstring jIVIDeviceInfoEntryObjValueStr = static_cast<jstring>(jIVIDeviceInfoEntryObjValue);
		const char *jIVIDeviceInfoEntryObjKeyStrCharStr = env->GetStringUTFChars(jIVIDeviceInfoEntryObjKeyStr,0);
		std::string newjIVIDeviceInfoEntryObjKeyStrStr(jIVIDeviceInfoEntryObjKeyStrCharStr);
		const char *jIVIDeviceInfoEntryObjValueStrCharStr = env->GetStringUTFChars(jIVIDeviceInfoEntryObjValueStr,0);
		std::string newjIVIDeviceInfoEntryObjValueStrStr(jIVIDeviceInfoEntryObjValueStrCharStr);
		newIVIDeviceInfo[newjIVIDeviceInfoEntryObjKeyStrStr] = newjIVIDeviceInfoEntryObjValueStrStr;
		env->ReleaseStringUTFChars(jIVIDeviceInfoEntryObjKeyStr, jIVIDeviceInfoEntryObjKeyStrCharStr);
		env->ReleaseStringUTFChars(jIVIDeviceInfoEntryObjValueStr, jIVIDeviceInfoEntryObjValueStrCharStr);
	}
	mNativeStub->updateIVIDeviceInfoAttr(newIVIDeviceInfo);
}

void JNITestAudioControllerService::nativeUpdateAudioSourceTypesAttr(JNIEnv *env, jobject thiz, jobjectArray jUpdateAudioSourceTypesArrayValue) {
    if (mNativeStub == nullptr) {
		return;
	}
	std::vector<std::string> newAudioSourceTypesVec;
	jint audioSourceTypesArrayLength = env->GetArrayLength(jUpdateAudioSourceTypesArrayValue);
	for (int idx = 0; idx < audioSourceTypesArrayLength; ++idx) {
		jstring jStringAudioSourceTypes = static_cast<jstring>(env->GetObjectArrayElement(jUpdateAudioSourceTypesArrayValue, idx));
		const char *jStringAudioSourceTypesCharStr = env->GetStringUTFChars(jStringAudioSourceTypes, 0);
		std::string newjStringAudioSourceTypesCharStr(jStringAudioSourceTypesCharStr);
		newAudioSourceTypesVec.push_back(newjStringAudioSourceTypesCharStr);
		env->ReleaseStringUTFChars(jStringAudioSourceTypes, jStringAudioSourceTypesCharStr);
	}
	mNativeStub->updateAudioSourceTypesAttr(newAudioSourceTypesVec);
}

void JNITestAudioControllerService::nativeUpdateVolumeInfoAttr(JNIEnv *env, jobject thiz, jobject jUpdateVolumeInfoValue) {
    if (mNativeStub == nullptr) {
		return;
	}
	jbyte jUpdateVolumeInfoValueVolume = env->GetByteField(jUpdateVolumeInfoValue, mDepsCache->mVolumeInfo.volumeId);
	jbyte jUpdateVolumeInfoValueMaxSoftVolume = env->GetByteField(jUpdateVolumeInfoValue, mDepsCache->mVolumeInfo.maxSoftVolumeId);
	jbyte jUpdateVolumeInfoValueLastVolume = env->GetByteField(jUpdateVolumeInfoValue, mDepsCache->mVolumeInfo.lastVolumeId);
	jboolean jUpdateVolumeInfoValueMute = env->GetBooleanField(jUpdateVolumeInfoValue, mDepsCache->mVolumeInfo.muteId);
	v2_Test_Audio::Controller::VolumeInfo newVolumeInfoVolumeInfo(
		(uint8_t)jUpdateVolumeInfoValueVolume,
		(uint8_t)jUpdateVolumeInfoValueMaxSoftVolume,
		(uint8_t)jUpdateVolumeInfoValueLastVolume,
		jUpdateVolumeInfoValueMute == JNI_TRUE ? true: false);
	mNativeStub->updateVolumeInfoAttr(newVolumeInfoVolumeInfo);
}

void JNITestAudioControllerService::nativeUpdateKeyValueByteBufferMapAttr(JNIEnv *env, jobject thiz, jobject jUpdateKeyValueByteBufferMapValue) {
    if (mNativeStub == nullptr) {
		return;
	}
	jobject jKeyValueByteBufferMapMapDataObj = env->GetObjectField(jUpdateKeyValueByteBufferMapValue, mDepsCache->mKeyValueByteBuffer.mapDataId);
	jobject jKeyValueByteBufferMapEntrySetObj = env->CallObjectMethod(jKeyValueByteBufferMapMapDataObj, mDepsCache->mHashMapEntrySetId);
	jobject jKeyValueByteBufferMapIteratorObj = env->CallObjectMethod(jKeyValueByteBufferMapEntrySetObj, mDepsCache->mHashMapIteratorId);
	v2_Test_Audio::Controller::KeyValueByteBuffer newKeyValueByteBufferMap;
	while (env->CallBooleanMethod(jKeyValueByteBufferMapIteratorObj, mDepsCache->mMapIteratorHasNextId)) {
		jobject jKeyValueByteBufferMapEntryObj = env->CallObjectMethod(jKeyValueByteBufferMapIteratorObj, mDepsCache->mMapIteratorNextId);
		jobject jKeyValueByteBufferMapEntryObjKey = env->CallObjectMethod(jKeyValueByteBufferMapEntryObj, mDepsCache->mHashMapGetKeyId);
		jint jKeyValueByteBufferMapEntryObjKeyInt = env->CallIntMethod(jKeyValueByteBufferMapEntryObjKey, mDepsCache->mIntValueId);
		jobject jKeyValueByteBufferMapEntryObjValue = env->CallObjectMethod(jKeyValueByteBufferMapEntryObj, mDepsCache->mHashMapGetValueId);
		jbyteArray jKeyValueByteBufferMapEntryObjValueByteArray = static_cast<jbyteArray>(jKeyValueByteBufferMapEntryObjValue);
		jbyte* jValueByteBuffer = env->GetByteArrayElements(jKeyValueByteBufferMapEntryObjValueByteArray, nullptr);
		std::vector<uint8_t> newValueByteBufferVec;
		jint valueByteBufferArrayLength = env->GetArrayLength(jKeyValueByteBufferMapEntryObjValueByteArray);
		for (int idx = 0; idx < valueByteBufferArrayLength; ++idx) {
			newValueByteBufferVec.push_back((uint8_t)jValueByteBuffer[idx]);
		}
		newKeyValueByteBufferMap[(uint32_t)jKeyValueByteBufferMapEntryObjKeyInt] = newValueByteBufferVec;
	}
	mNativeStub->updateKeyValueByteBufferMapAttr(newKeyValueByteBufferMap);
}

void JNITestAudioControllerService::nativeUpdateDeviceStateMapAttr(JNIEnv *env, jobject thiz, jobject jUpdateDeviceStateMapValue) {
    if (mNativeStub == nullptr) {
		return;
	}
	jobject jDeviceStateMapMapDataObj = env->GetObjectField(jUpdateDeviceStateMapValue, mDepsCache->mDeviceStateMap.mapDataId);
	jobject jDeviceStateMapEntrySetObj = env->CallObjectMethod(jDeviceStateMapMapDataObj, mDepsCache->mHashMapEntrySetId);
	jobject jDeviceStateMapIteratorObj = env->CallObjectMethod(jDeviceStateMapEntrySetObj, mDepsCache->mHashMapIteratorId);
	v2_Test_Audio::Controller::DeviceStateMap newDeviceStateMap;
	while (env->CallBooleanMethod(jDeviceStateMapIteratorObj, mDepsCache->mMapIteratorHasNextId)) {
		jobject jDeviceStateMapEntryObj = env->CallObjectMethod(jDeviceStateMapIteratorObj, mDepsCache->mMapIteratorNextId);
		jobject jDeviceStateMapEntryObjKey = env->CallObjectMethod(jDeviceStateMapEntryObj, mDepsCache->mHashMapGetKeyId);
		jobject jDeviceStateMapEntryObjValue = env->CallObjectMethod(jDeviceStateMapEntryObj, mDepsCache->mHashMapGetValueId);
		jint jKeyAudioDeviceTypeValue = env->CallIntMethod(jDeviceStateMapEntryObjKey, mDepsCache->mAudioDeviceTypeOrdinalMethodId);
		v2_Test_Audio::Controller::AudioDeviceType newKeyAudioDeviceType;
		newKeyAudioDeviceType.value_ = (int)jKeyAudioDeviceTypeValue;
		jint jValueAudioFocusStatusTypeValue = env->CallIntMethod(jDeviceStateMapEntryObjValue, mDepsCache->mAudioFocusStatusTypeOrdinalMethodId);
		v2_Test_Audio::Controller::AudioFocusStatusType newValueAudioFocusStatusType;
		newValueAudioFocusStatusType.value_ = (int)jValueAudioFocusStatusTypeValue;
		newDeviceStateMap[newKeyAudioDeviceType] = newValueAudioFocusStatusType;
	}
	mNativeStub->updateDeviceStateMapAttr(newDeviceStateMap);
}

void JNITestAudioControllerService::nativeUpdateStringToShortMapAttr(JNIEnv *env, jobject thiz, jobject jUpdateStringToShortMapValue) {
    if (mNativeStub == nullptr) {
		return;
	}
	jobject jStringToShortMapMapDataObj = env->GetObjectField(jUpdateStringToShortMapValue, mDepsCache->mStringToShortMap.mapDataId);
	jobject jStringToShortMapEntrySetObj = env->CallObjectMethod(jStringToShortMapMapDataObj, mDepsCache->mHashMapEntrySetId);
	jobject jStringToShortMapIteratorObj = env->CallObjectMethod(jStringToShortMapEntrySetObj, mDepsCache->mHashMapIteratorId);
	v2_Test_Audio::Controller::StringToShortMap newStringToShortMap;
	while (env->CallBooleanMethod(jStringToShortMapIteratorObj, mDepsCache->mMapIteratorHasNextId)) {
		jobject jStringToShortMapEntryObj = env->CallObjectMethod(jStringToShortMapIteratorObj, mDepsCache->mMapIteratorNextId);
		jobject jStringToShortMapEntryObjKey = env->CallObjectMethod(jStringToShortMapEntryObj, mDepsCache->mHashMapGetKeyId);
		jstring jStringToShortMapEntryObjKeyStr = static_cast<jstring>(jStringToShortMapEntryObjKey);
		jobject jStringToShortMapEntryObjValue = env->CallObjectMethod(jStringToShortMapEntryObj, mDepsCache->mHashMapGetValueId);
		jshort jStringToShortMapEntryObjValueShort = env->CallShortMethod(jStringToShortMapEntryObjValue, mDepsCache->mShortValueId);
		const char *jStringToShortMapEntryObjKeyStrCharStr = env->GetStringUTFChars(jStringToShortMapEntryObjKeyStr,0);
		std::string newjStringToShortMapEntryObjKeyStrStr(jStringToShortMapEntryObjKeyStrCharStr);
		newStringToShortMap[newjStringToShortMapEntryObjKeyStrStr] = (uint16_t)jStringToShortMapEntryObjValueShort;
		env->ReleaseStringUTFChars(jStringToShortMapEntryObjKeyStr, jStringToShortMapEntryObjKeyStrCharStr);
	}
	mNativeStub->updateStringToShortMapAttr(newStringToShortMap);
}

void JNITestAudioControllerService::fireFocusStatusChanged(JNIEnv *env, jobject thiz, jobjectArray jFocusStatusArray) {
    if (mNativeStub == nullptr) {
		return;
	}
	std::vector<v2_Test_Audio::Controller::FocusStatus> newFocusStatusFocusStatusVec;
	jint focusStatusFocusStatusArrayLength = env->GetArrayLength(jFocusStatusArray);
	for (int idx = 0; idx < focusStatusFocusStatusArrayLength; ++idx) {
		jobject jFocusStatusFocusStatusObj = env->GetObjectArrayElement(jFocusStatusArray, idx);
		jstring jFocusStatusFocusStatusObjAudioSourceType = static_cast<jstring>(env->GetObjectField(jFocusStatusFocusStatusObj, mDepsCache->mFocusStatus.audioSourceTypeId));
		jobject jFocusStatusFocusStatusObjDeviceObj = env->GetObjectField(jFocusStatusFocusStatusObj, mDepsCache->mFocusStatus.deviceId);
		jobject jFocusStatusFocusStatusObjStatusObj = env->GetObjectField(jFocusStatusFocusStatusObj, mDepsCache->mFocusStatus.statusId);
		const char *jFocusStatusFocusStatusObjAudioSourceTypeCharStr = env->GetStringUTFChars(jFocusStatusFocusStatusObjAudioSourceType,0);
		std::string newjFocusStatusFocusStatusObjAudioSourceTypeStr(jFocusStatusFocusStatusObjAudioSourceTypeCharStr);
		jint jDeviceValue = env->CallIntMethod(jFocusStatusFocusStatusObjDeviceObj, mDepsCache->mAudioDeviceTypeOrdinalMethodId);
		v2_Test_Audio::Controller::AudioDeviceType newDevice;
		newDevice.value_ = (int)jDeviceValue;
		jint jStatusValue = env->CallIntMethod(jFocusStatusFocusStatusObjStatusObj, mDepsCache->mAudioFocusStatusTypeOrdinalMethodId);
		v2_Test_Audio::Controller::AudioFocusStatusType newStatus;
		newStatus.value_ = (int)jStatusValue;
		v2_Test_Audio::Controller::FocusStatus newFocusStatusFocusStatus(
			newjFocusStatusFocusStatusObjAudioSourceTypeStr,
			newDevice,
			newStatus);
		newFocusStatusFocusStatusVec.push_back(newFocusStatusFocusStatus);
		env->ReleaseStringUTFChars(jFocusStatusFocusStatusObjAudioSourceType, jFocusStatusFocusStatusObjAudioSourceTypeCharStr);
		env->DeleteLocalRef(jFocusStatusFocusStatusObj);
	}
	mNativeStub->fireFocusStatusChanged(newFocusStatusFocusStatusVec);
}

JNITestAudioControllerService::JNITestAudioControllerService() {
}

JNITestAudioControllerService::~JNITestAudioControllerService() {
}

