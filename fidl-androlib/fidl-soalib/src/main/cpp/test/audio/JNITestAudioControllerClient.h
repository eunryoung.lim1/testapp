
/*
 * Copyright (c) 2023 42dot All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <jni.h>

#include <common/JNIHelper.h>

#include <common/JNIServiceStatusListener.h>
#include <Test/Audio/TestAudioControllerClient.h>
#include <test/audio/JNITestAudioControllerAttrListener.h>
#include <test/audio/JNITestAudioControllerEventListener.h>
#include <test/audio/JNITestAudioControllerDepsCache.h>
#include <test/audio/JNITestAudioControllerAsyncCallback.h>

using namespace ai::umos::soa::core;

namespace v2_Test_Audio = v2::Test::Audio;
using namespace v2_Test_Audio;

class JNITestAudioControllerClient {
public:
    static jint initializeTestAudioControllerClient(JavaVM* vm);

private:
    JNITestAudioControllerClient();
    virtual ~JNITestAudioControllerClient();

	static void setAttributeListener(JNIEnv *env, jobject thiz, jobject attrListener);
	static void nativeStartClient(JNIEnv *env, jobject thiz, jobject serviceStatusListener);
	static void nativeStopClient(JNIEnv *env, jobject thiz);
	static void setBroadcastEventListener(JNIEnv *env, jobject thiz, jobject EventListener);

	static bool nativeSetIVIDeviceInfoAttr(JNIEnv *env, jobject thiz, jobject jNewIVIDeviceInfoValue);
	static void nativeSetIVIDeviceInfoAttrAsync(JNIEnv *env, jobject thiz, jobject jNewIVIDeviceInfoValue, jobject jCallback);
	static jobject nativeGetIVIDeviceInfoAttr(JNIEnv *env, jobject thiz);
	static void nativeGetIVIDeviceInfoAttrAsync(JNIEnv *env, jobject thiz, jobject jCallback);
	static jobjectArray nativeGetAudioSourceTypesAttr(JNIEnv *env, jobject thiz);
	static void nativeGetAudioSourceTypesAttrAsync(JNIEnv *env, jobject thiz, jobject jCallback);
	static jobject nativeGetVolumeInfoAttr(JNIEnv *env, jobject thiz);
	static void nativeGetVolumeInfoAttrAsync(JNIEnv *env, jobject thiz, jobject jCallback);
	static jobject nativeGetKeyValueByteBufferMapAttr(JNIEnv *env, jobject thiz);
	static void nativeGetKeyValueByteBufferMapAttrAsync(JNIEnv *env, jobject thiz, jobject jCallback);
	static jobject nativeGetDeviceStateMapAttr(JNIEnv *env, jobject thiz);
	static void nativeGetDeviceStateMapAttrAsync(JNIEnv *env, jobject thiz, jobject jCallback);
	static jobject nativeGetStringToShortMapAttr(JNIEnv *env, jobject thiz);
	static void nativeGetStringToShortMapAttrAsync(JNIEnv *env, jobject thiz, jobject jCallback);
	static jobject nativeObtainFocus(JNIEnv *env, jobject thiz, jstring jAudioSourceType, jobject jDevice);
	static void nativeObtainFocusAsync(JNIEnv *env, jobject thiz, jstring jAudioSourceType, jobject jDevice, jobject jCallback);
	static jobject nativeReleaseFocus(JNIEnv *env, jobject thiz, jstring jAudioSourceType, jobject jDevice);
	static void nativeReleaseFocusAsync(JNIEnv *env, jobject thiz, jstring jAudioSourceType, jobject jDevice, jobject jCallback);
	static jobject nativeSetVolume(JNIEnv *env, jobject thiz, jbyte jVolume);
	static void nativeSetVolumeAsync(JNIEnv *env, jobject thiz, jbyte jVolume, jobject jCallback);
	static jobject nativeSetSourceVolume(JNIEnv *env, jobject thiz, jstring jAudioSourceType, jshort jVolume, jint jMaxVolume);
	static void nativeSetSourceVolumeAsync(JNIEnv *env, jobject thiz, jstring jAudioSourceType, jshort jVolume, jint jMaxVolume, jobject jCallback);
	static jobject nativeGetAudioSourceStatus(JNIEnv *env, jobject thiz, jstring jAudioSourceType);
	static void nativeGetAudioSourceStatusAsync(JNIEnv *env, jobject thiz, jstring jAudioSourceType, jobject jCallback);

    static JavaVM*    mJavaVM;
	static TestAudioControllerClientPtr				mNativeClient;
	static JNIServiceStatusListenerPtr			mServiceStatusListener;
	static JNITestAudioControllerAttrListenerPtr		mAttrListener;
	static JNITestAudioControllerEventListenerPtr		mEventListener;
	static JNITestAudioControllerDepsCachePtr		mDepsCache;
	static JNITestAudioControllerAsyncCallbackPtr	mAsyncCallback;

};