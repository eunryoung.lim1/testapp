/*
 * Copyright (c) 2023 42dot All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include <jni.h>

#include <Test/Audio/ITestAudioControllerHandler.h>

#include <test/audio/JNITestAudioControllerDepsCache.h>

using namespace ai::umos::soa::core;

namespace v2_Test_Audio = v2::Test::Audio;
using namespace v2_Test_Audio;

class JNITestAudioControllerHandler final : public ITestAudioControllerHandler {
public:
    JNITestAudioControllerHandler(JNIEnv *env, jobject callback, JNITestAudioControllerDepsCachePtr depsCache);
    ~JNITestAudioControllerHandler();

	void onObtainFocus(const std::string& audioSourceType, const v2_Test_Audio::Controller::AudioDeviceType& device, v2_Test_Audio::Controller::FocusControlError& errFocusControlError, v2_Test_Audio::Controller::AudioFocusResultType& replyRes) override;
	void onReleaseFocus(const std::string& audioSourceType, const v2_Test_Audio::Controller::AudioDeviceType& device, v2_Test_Audio::Controller::FocusControlError& errFocusControlError) override;
	void onSetVolume(uint8_t volume, v2_Test_Audio::Controller::VolumeControlError& errVolumeControlError) override;
	void onSetSourceVolume(const std::string& audioSourceType, uint16_t volume, uint32_t maxVolume, v2_Test_Audio::Controller::VolumeControlError& errVolumeControlError) override;
	void onGetAudioSourceStatus(const std::string& audioSourceType, v2_Test_Audio::Controller::ErrorType& errErrorType, v2_Test_Audio::Controller::AudioSourceStatusInfo& replyAudioSourceStatusInfo) override;

private:
    JavaVM*     mJavaVM;
    JNIEnv*     mEnv;
    jobject     mCallbackObj;
    JNITestAudioControllerDepsCachePtr    mDepsCache{ nullptr };

	jmethodID	mOnObtainFocusId;
	jmethodID	mOnReleaseFocusId;
	jmethodID	mOnSetVolumeId;
	jmethodID	mOnSetSourceVolumeId;
	jmethodID	mOnGetAudioSourceStatusId;
};

using JNITestAudioControllerHandlerPtr = std::shared_ptr<JNITestAudioControllerHandler>;