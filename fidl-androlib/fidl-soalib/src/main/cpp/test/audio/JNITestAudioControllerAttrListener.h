/*
 * Copyright (c) 2023 42dot All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include <jni.h>
#include <mutex>

#include <Test/Audio/ITestAudioControllerAttrListener.h>
#include <v2/Test/Audio/ControllerProxy.hpp>

#include <test/audio/JNITestAudioControllerDepsCache.h>

using namespace ai::umos::soa::core;

namespace v2_Test_Audio = v2::Test::Audio;
using namespace v2_Test_Audio;

class JNITestAudioControllerAttrListener final : public ITestAudioControllerAttrListener {
public:
    JNITestAudioControllerAttrListener(JNIEnv *env, jobject callback, JNITestAudioControllerDepsCachePtr depsCache);
    ~JNITestAudioControllerAttrListener();

	void onIVIDeviceInfoChanged(const v2_Test_Audio::Controller::KeyValueString& iVIDeviceInfo) override;
	void onAudioSourceTypesChanged(const std::vector<std::string>& audioSourceTypesVec) override;
	void onVolumeInfoChanged(const v2_Test_Audio::Controller::VolumeInfo& volumeInfo) override;
	void onKeyValueByteBufferMapChanged(const v2_Test_Audio::Controller::KeyValueByteBuffer& keyValueByteBufferMap) override;
	void onDeviceStateMapChanged(const v2_Test_Audio::Controller::DeviceStateMap& deviceStateMap) override;
	void onStringToShortMapChanged(const v2_Test_Audio::Controller::StringToShortMap& stringToShortMap) override;

private:
    JavaVM*    mJavaVM{ nullptr };
    JNIEnv*    mEnv{ nullptr };
    jobject    mCallbackObj{ nullptr };
    JNITestAudioControllerDepsCachePtr    mDepsCache{ nullptr };

	jmethodID	mOnIVIDeviceInfoChangedId;
	jmethodID	mOnAudioSourceTypesChangedId;
	jmethodID	mOnVolumeInfoChangedId;
	jmethodID	mOnKeyValueByteBufferMapChangedId;
	jmethodID	mOnDeviceStateMapChangedId;
	jmethodID	mOnStringToShortMapChangedId;
};

using JNITestAudioControllerAttrListenerPtr = std::shared_ptr<JNITestAudioControllerAttrListener>;
