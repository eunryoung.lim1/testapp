/*
 * Copyright (c) 2023 42dot All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <logging.h>
#include <jni.h>

#include <SoaClientManager.h>
#include "JNITestAudioControllerClient.h"

extern SoaClientManagerPtr gSoaClientManager;

TestAudioControllerClientPtr JNITestAudioControllerClient::mNativeClient = nullptr;
JNIServiceStatusListenerPtr JNITestAudioControllerClient::mServiceStatusListener = nullptr;
JNITestAudioControllerDepsCachePtr JNITestAudioControllerClient::mDepsCache = nullptr;
JNITestAudioControllerAsyncCallbackPtr JNITestAudioControllerClient::mAsyncCallback = nullptr;
JNITestAudioControllerAttrListenerPtr JNITestAudioControllerClient::mAttrListener = nullptr;
JNITestAudioControllerEventListenerPtr JNITestAudioControllerClient::mEventListener = nullptr;

JavaVM* JNITestAudioControllerClient::mJavaVM{ nullptr };

jint JNITestAudioControllerClient::initializeTestAudioControllerClient(JavaVM* vm) {
    LOGI("JNI::OnLoad initializeTestAudioControllerClient");

    mJavaVM = vm;

	JNIEnv* env;
    if (vm->GetEnv(reinterpret_cast<void**>(&env), JNI_VERSION_1_6) != JNI_OK) {
        return JNI_ERR;
    }

    const JNINativeMethod methods[] = {
        {"nativeStartClient", "(Lai/umos/soa/common/IServiceStatusListener;)V",
			reinterpret_cast<void*>(nativeStartClient)},
		{"nativeStopClient", "()V",
			reinterpret_cast<void*>(nativeStopClient)},
		{"setAttributeListener", "(Lai/umos/soa/test/audio/interfaces/ITestAudioControllerAttrListener;)V",
			reinterpret_cast<void*>(setAttributeListener)},
		{"setBroadcastEventListener", "(Lai/umos/soa/test/audio/interfaces/ITestAudioControllerEventListener;)V",
			reinterpret_cast<void*>(setBroadcastEventListener)},
		{"setIVIDeviceInfoAttr", "(Lai/umos/soa/test/audio/data/KeyValueString;)Z",
			reinterpret_cast<void*>(nativeSetIVIDeviceInfoAttr)},
		{"setIVIDeviceInfoAttrAsync", "(Lai/umos/soa/test/audio/data/KeyValueString;Lai/umos/soa/test/audio/interfaces/ITestAudioControllerAttrAsyncCallback$IIVIDeviceInfoAttrCallback;)V",
			reinterpret_cast<void*>(nativeSetIVIDeviceInfoAttrAsync)},
		{"getIVIDeviceInfoAttr", "()Lai/umos/soa/test/audio/data/KeyValueString;",
			reinterpret_cast<void*>(nativeGetIVIDeviceInfoAttr)},
		{"getIVIDeviceInfoAttrAsync", "(Lai/umos/soa/test/audio/interfaces/ITestAudioControllerAttrAsyncCallback$IIVIDeviceInfoAttrCallback;)V",
			reinterpret_cast<void*>(nativeGetIVIDeviceInfoAttrAsync)},
		{"getAudioSourceTypesAttr", "()[Ljava/lang/String;",
			reinterpret_cast<void*>(nativeGetAudioSourceTypesAttr)},
		{"getAudioSourceTypesAttrAsync", "(Lai/umos/soa/test/audio/interfaces/ITestAudioControllerAttrAsyncCallback$IAudioSourceTypesAttrCallback;)V",
			reinterpret_cast<void*>(nativeGetAudioSourceTypesAttrAsync)},
		{"getVolumeInfoAttr", "()Lai/umos/soa/test/audio/data/VolumeInfo;",
			reinterpret_cast<void*>(nativeGetVolumeInfoAttr)},
		{"getVolumeInfoAttrAsync", "(Lai/umos/soa/test/audio/interfaces/ITestAudioControllerAttrAsyncCallback$IVolumeInfoAttrCallback;)V",
			reinterpret_cast<void*>(nativeGetVolumeInfoAttrAsync)},
		{"getKeyValueByteBufferMapAttr", "()Lai/umos/soa/test/audio/data/KeyValueByteBuffer;",
			reinterpret_cast<void*>(nativeGetKeyValueByteBufferMapAttr)},
		{"getKeyValueByteBufferMapAttrAsync", "(Lai/umos/soa/test/audio/interfaces/ITestAudioControllerAttrAsyncCallback$IKeyValueByteBufferMapAttrCallback;)V",
			reinterpret_cast<void*>(nativeGetKeyValueByteBufferMapAttrAsync)},
		{"getDeviceStateMapAttr", "()Lai/umos/soa/test/audio/data/DeviceStateMap;",
			reinterpret_cast<void*>(nativeGetDeviceStateMapAttr)},
		{"getDeviceStateMapAttrAsync", "(Lai/umos/soa/test/audio/interfaces/ITestAudioControllerAttrAsyncCallback$IDeviceStateMapAttrCallback;)V",
			reinterpret_cast<void*>(nativeGetDeviceStateMapAttrAsync)},
		{"getStringToShortMapAttr", "()Lai/umos/soa/test/audio/data/StringToShortMap;",
			reinterpret_cast<void*>(nativeGetStringToShortMapAttr)},
		{"getStringToShortMapAttrAsync", "(Lai/umos/soa/test/audio/interfaces/ITestAudioControllerAttrAsyncCallback$IStringToShortMapAttrCallback;)V",
			reinterpret_cast<void*>(nativeGetStringToShortMapAttrAsync)},
		{"obtainFocus", "(Ljava/lang/String;Lai/umos/soa/test/audio/data/AudioDeviceType;)Lai/umos/soa/test/audio/data/ReplyObtainFocus;",
			reinterpret_cast<void*>(nativeObtainFocus)},
		{"obtainFocusAsync", "(Ljava/lang/String;Lai/umos/soa/test/audio/data/AudioDeviceType;Lai/umos/soa/test/audio/interfaces/ITestAudioControllerAsyncCallback$IObtainFocusCallback;)V",
			reinterpret_cast<void*>(nativeObtainFocusAsync)},
		{"releaseFocus", "(Ljava/lang/String;Lai/umos/soa/test/audio/data/AudioDeviceType;)Lai/umos/soa/test/audio/data/FocusControlError;",
			reinterpret_cast<void*>(nativeReleaseFocus)},
		{"releaseFocusAsync", "(Ljava/lang/String;Lai/umos/soa/test/audio/data/AudioDeviceType;Lai/umos/soa/test/audio/interfaces/ITestAudioControllerAsyncCallback$IReleaseFocusCallback;)V",
			reinterpret_cast<void*>(nativeReleaseFocusAsync)},
		{"setVolume", "(B)Lai/umos/soa/test/audio/data/VolumeControlError;",
			reinterpret_cast<void*>(nativeSetVolume)},
		{"setVolumeAsync", "(BLai/umos/soa/test/audio/interfaces/ITestAudioControllerAsyncCallback$ISetVolumeCallback;)V",
			reinterpret_cast<void*>(nativeSetVolumeAsync)},
		{"setSourceVolume", "(Ljava/lang/String;SI)Lai/umos/soa/test/audio/data/VolumeControlError;",
			reinterpret_cast<void*>(nativeSetSourceVolume)},
		{"setSourceVolumeAsync", "(Ljava/lang/String;SILai/umos/soa/test/audio/interfaces/ITestAudioControllerAsyncCallback$ISetSourceVolumeCallback;)V",
			reinterpret_cast<void*>(nativeSetSourceVolumeAsync)},
		{"getAudioSourceStatus", "(Ljava/lang/String;)Lai/umos/soa/test/audio/data/ReplyGetAudioSourceStatus;",
			reinterpret_cast<void*>(nativeGetAudioSourceStatus)},
		{"getAudioSourceStatusAsync", "(Ljava/lang/String;Lai/umos/soa/test/audio/interfaces/ITestAudioControllerAsyncCallback$IGetAudioSourceStatusCallback;)V",
			reinterpret_cast<void*>(nativeGetAudioSourceStatusAsync)},
    };

    jclass clazz = env->FindClass("ai/umos/soa/test/audio/TestAudioControllerConsumer");
	if (!clazz) {
		return JNI_ERR;
	}

	int result = env->RegisterNatives(clazz, methods, sizeof(methods) / sizeof(methods[0]));
	env->DeleteLocalRef(clazz);
	if (result != 0) {
		return JNI_ERR;
	}
    mDepsCache = std::make_shared<JNITestAudioControllerDepsCache>(env);
	mAsyncCallback = std::make_shared<JNITestAudioControllerAsyncCallback>(env, mDepsCache);

    return JNI_VERSION_1_6;
}

void JNITestAudioControllerClient::nativeStartClient(JNIEnv *env, jobject classObj, jobject serviceStatusListener) {
	LOGI("JNITestAudioControllerClient::nativeStartClient");

    if (!gSoaClientManager) {
        LOGI("Failed to start native client, SoaClientManager is null");
        return ;
    }

    mNativeClient = gSoaClientManager->startTestAudioControllerClient();
    if (!mNativeClient) {
        LOGI("Failed to start TestAudioController proxy");
        return ;
    }

    mServiceStatusListener = std::make_shared<JNIServiceStatusListener>(env, serviceStatusListener);
    mNativeClient->connectServer(mServiceStatusListener);
}

void JNITestAudioControllerClient::nativeStopClient(JNIEnv *env, jobject thiz) {
	LOGI("JNITestAudioControllerClient::nativeStopClient serviceStatus count:%d, mAttrListener count:%d", mServiceStatusListener.use_count(), mAttrListener.use_count());

   gSoaClientManager->stopTestAudioControllerClient();
   mServiceStatusListener.reset();
   mAttrListener.reset();
   mEventListener.reset();
   mNativeClient.reset();
}

void JNITestAudioControllerClient::setAttributeListener(JNIEnv *env, jobject classObj, jobject attrListener) {
    if (!gSoaClientManager || !mNativeClient) {
        LOGI("Failed to register JNITestAudioControllerClient::attribute changed event");
        return ;
    }

    LOGI("JNITestAudioControllerClient::%s", __func__);
    mAttrListener = std::make_shared<JNITestAudioControllerAttrListener>(env, attrListener, mDepsCache);
    mNativeClient->setAttributeListener(mAttrListener);
}

void JNITestAudioControllerClient::setBroadcastEventListener(JNIEnv *env, jobject classObj, jobject eventListener) {
    if (!gSoaClientManager || !mNativeClient) {
        LOGI("Failed to register JNITestAudioControllerClient::broadcast listener");
        return ;
    }

    LOGI("%s", __func__);
    mEventListener = std::make_shared<JNITestAudioControllerEventListener>(env, eventListener, mDepsCache);
    mNativeClient->setBroadcastEventListener(mEventListener);
}

bool JNITestAudioControllerClient::nativeSetIVIDeviceInfoAttr(JNIEnv *env, jobject thiz, jobject jNewIVIDeviceInfoValue) {
    jobject jIVIDeviceInfoMapDataObj = env->GetObjectField(jNewIVIDeviceInfoValue, mDepsCache->mKeyValueString.mapDataId);
	jobject jIVIDeviceInfoEntrySetObj = env->CallObjectMethod(jIVIDeviceInfoMapDataObj, mDepsCache->mHashMapEntrySetId);
	jobject jIVIDeviceInfoIteratorObj = env->CallObjectMethod(jIVIDeviceInfoEntrySetObj, mDepsCache->mHashMapIteratorId);
	v2_Test_Audio::Controller::KeyValueString newIVIDeviceInfo;
	while (env->CallBooleanMethod(jIVIDeviceInfoIteratorObj, mDepsCache->mMapIteratorHasNextId)) {
		jobject jIVIDeviceInfoEntryObj = env->CallObjectMethod(jIVIDeviceInfoIteratorObj, mDepsCache->mMapIteratorNextId);
		jobject jIVIDeviceInfoEntryObjKey = env->CallObjectMethod(jIVIDeviceInfoEntryObj, mDepsCache->mHashMapGetKeyId);
		jstring jIVIDeviceInfoEntryObjKeyStr = static_cast<jstring>(jIVIDeviceInfoEntryObjKey);
		jobject jIVIDeviceInfoEntryObjValue = env->CallObjectMethod(jIVIDeviceInfoEntryObj, mDepsCache->mHashMapGetValueId);
		jstring jIVIDeviceInfoEntryObjValueStr = static_cast<jstring>(jIVIDeviceInfoEntryObjValue);
		const char *jIVIDeviceInfoEntryObjKeyStrCharStr = env->GetStringUTFChars(jIVIDeviceInfoEntryObjKeyStr,0);
		std::string newjIVIDeviceInfoEntryObjKeyStrStr(jIVIDeviceInfoEntryObjKeyStrCharStr);
		const char *jIVIDeviceInfoEntryObjValueStrCharStr = env->GetStringUTFChars(jIVIDeviceInfoEntryObjValueStr,0);
		std::string newjIVIDeviceInfoEntryObjValueStrStr(jIVIDeviceInfoEntryObjValueStrCharStr);
		newIVIDeviceInfo[newjIVIDeviceInfoEntryObjKeyStrStr] = newjIVIDeviceInfoEntryObjValueStrStr;
		env->ReleaseStringUTFChars(jIVIDeviceInfoEntryObjKeyStr, jIVIDeviceInfoEntryObjKeyStrCharStr);
		env->ReleaseStringUTFChars(jIVIDeviceInfoEntryObjValueStr, jIVIDeviceInfoEntryObjValueStrCharStr);
	}
	v2_Test_Audio::Controller::KeyValueString responseValue;
	return mNativeClient->setIVIDeviceInfoAttr(newIVIDeviceInfo, responseValue) ? true : false;
}

void JNITestAudioControllerClient::nativeSetIVIDeviceInfoAttrAsync(JNIEnv *env, jobject thiz, jobject jNewIVIDeviceInfoValue, jobject jCallback) {
    jobject jIVIDeviceInfoMapDataObj = env->GetObjectField(jNewIVIDeviceInfoValue, mDepsCache->mKeyValueString.mapDataId);
	jobject jIVIDeviceInfoEntrySetObj = env->CallObjectMethod(jIVIDeviceInfoMapDataObj, mDepsCache->mHashMapEntrySetId);
	jobject jIVIDeviceInfoIteratorObj = env->CallObjectMethod(jIVIDeviceInfoEntrySetObj, mDepsCache->mHashMapIteratorId);
	v2_Test_Audio::Controller::KeyValueString newIVIDeviceInfo;
	while (env->CallBooleanMethod(jIVIDeviceInfoIteratorObj, mDepsCache->mMapIteratorHasNextId)) {
		jobject jIVIDeviceInfoEntryObj = env->CallObjectMethod(jIVIDeviceInfoIteratorObj, mDepsCache->mMapIteratorNextId);
		jobject jIVIDeviceInfoEntryObjKey = env->CallObjectMethod(jIVIDeviceInfoEntryObj, mDepsCache->mHashMapGetKeyId);
		jstring jIVIDeviceInfoEntryObjKeyStr = static_cast<jstring>(jIVIDeviceInfoEntryObjKey);
		jobject jIVIDeviceInfoEntryObjValue = env->CallObjectMethod(jIVIDeviceInfoEntryObj, mDepsCache->mHashMapGetValueId);
		jstring jIVIDeviceInfoEntryObjValueStr = static_cast<jstring>(jIVIDeviceInfoEntryObjValue);
		const char *jIVIDeviceInfoEntryObjKeyStrCharStr = env->GetStringUTFChars(jIVIDeviceInfoEntryObjKeyStr,0);
		std::string newjIVIDeviceInfoEntryObjKeyStrStr(jIVIDeviceInfoEntryObjKeyStrCharStr);
		const char *jIVIDeviceInfoEntryObjValueStrCharStr = env->GetStringUTFChars(jIVIDeviceInfoEntryObjValueStr,0);
		std::string newjIVIDeviceInfoEntryObjValueStrStr(jIVIDeviceInfoEntryObjValueStrCharStr);
		newIVIDeviceInfo[newjIVIDeviceInfoEntryObjKeyStrStr] = newjIVIDeviceInfoEntryObjValueStrStr;
		env->ReleaseStringUTFChars(jIVIDeviceInfoEntryObjKeyStr, jIVIDeviceInfoEntryObjKeyStrCharStr);
		env->ReleaseStringUTFChars(jIVIDeviceInfoEntryObjValueStr, jIVIDeviceInfoEntryObjValueStrCharStr);
	}
	if (jCallback == nullptr) {
		mNativeClient->setIVIDeviceInfoAttrAsync(newIVIDeviceInfo, nullptr);
	} else {
		uint32_t callbackId = 0;
		if (!mAsyncCallback->registerCallback(env, jCallback, "onIVIDeviceInfoAttr", "(Lai/ai/umos/soa/common/AsyncCallStatus;Lai/umos/soa/test/audio/data/KeyValueString;)V", callbackId)) {
			return;
		}
		mNativeClient->setIVIDeviceInfoAttrAsync(newIVIDeviceInfo,
			[callbackId](const CommonAPI::CallStatus& callStatus, const v2_Test_Audio::Controller::KeyValueString& replyIVIDeviceInfo) {
				mAsyncCallback->onIVIDeviceInfoAttr(callbackId, callStatus, replyIVIDeviceInfo);
			}
		);
	}
}

jobject JNITestAudioControllerClient::nativeGetIVIDeviceInfoAttr(JNIEnv *env, jobject thiz) {
    v2_Test_Audio::Controller::KeyValueString iVIDeviceInfo;
	mNativeClient->getIVIDeviceInfoAttr(iVIDeviceInfo);
	jobject jKeyValueStringIVIDeviceInfoHashMapObj = env->NewObject(mDepsCache->mHashMapClass, mDepsCache->mHashMapCtorId);
	for (const auto& mapEntry : iVIDeviceInfo){
		jstring jKeyStringStr = env->NewStringUTF(mapEntry.first.c_str());
		jstring jValueStringStr = env->NewStringUTF(mapEntry.second.c_str());
		env->CallObjectMethod(jKeyValueStringIVIDeviceInfoHashMapObj, mDepsCache->mHashMapPutId, jKeyStringStr, jValueStringStr);
		env->DeleteLocalRef(jKeyStringStr);
		env->DeleteLocalRef(jValueStringStr);
	}
	jobject jKeyValueStringIVIDeviceInfoObj = env->NewObject(mDepsCache->mKeyValueStringClass, mDepsCache->mKeyValueStringCtorId, jKeyValueStringIVIDeviceInfoHashMapObj);
	return jKeyValueStringIVIDeviceInfoObj;
}

void JNITestAudioControllerClient::nativeGetIVIDeviceInfoAttrAsync(JNIEnv *env, jobject thiz, jobject jCallback) {
    if (jCallback == nullptr) {
		mNativeClient->getIVIDeviceInfoAttrAsync(nullptr);
	} else {
		uint32_t callbackId = 0;
		if (!mAsyncCallback->registerCallback(env, jCallback, "onIVIDeviceInfoAttr", "(Lai/ai/umos/soa/common/AsyncCallStatus;Lai/umos/soa/test/audio/data/KeyValueString;)V", callbackId)) {
			return;
		}
		mNativeClient->getIVIDeviceInfoAttrAsync([callbackId](const CommonAPI::CallStatus& callStatus, const v2_Test_Audio::Controller::KeyValueString& replyIVIDeviceInfo) {
				mAsyncCallback->onIVIDeviceInfoAttr(callbackId, callStatus, replyIVIDeviceInfo);
			}
		);
	}
}

jobjectArray JNITestAudioControllerClient::nativeGetAudioSourceTypesAttr(JNIEnv *env, jobject thiz) {
    std::vector<std::string> audioSourceTypesVec;
	mNativeClient->getAudioSourceTypesAttr(audioSourceTypesVec);
	jsize jaudioSourceTypesVecSize = static_cast<jsize>(audioSourceTypesVec.size());
	jobjectArray jAudioSourceTypesStringArrayObj = env->NewObjectArray(jaudioSourceTypesVecSize, mDepsCache->mJavaStringClass, nullptr);
	for (jsize idx = 0; idx < jaudioSourceTypesVecSize; ++idx) {
		auto audioSourceTypes = audioSourceTypesVec[idx];
		jstring jAudioSourceTypesStr = env->NewStringUTF(audioSourceTypes.c_str());
		env->SetObjectArrayElement(jAudioSourceTypesStringArrayObj, idx, jAudioSourceTypesStr);
		env->DeleteLocalRef(jAudioSourceTypesStr);
	}
	return jAudioSourceTypesStringArrayObj;
}

void JNITestAudioControllerClient::nativeGetAudioSourceTypesAttrAsync(JNIEnv *env, jobject thiz, jobject jCallback) {
    if (jCallback == nullptr) {
		mNativeClient->getAudioSourceTypesAttrAsync(nullptr);
	} else {
		uint32_t callbackId = 0;
		if (!mAsyncCallback->registerCallback(env, jCallback, "onAudioSourceTypesAttr", "(Lai/ai/umos/soa/common/AsyncCallStatus;[Ljava/lang/String;)V", callbackId)) {
			return;
		}
		mNativeClient->getAudioSourceTypesAttrAsync([callbackId](const CommonAPI::CallStatus& callStatus, const std::vector<std::string>& replyAudioSourceTypesVec) {
				mAsyncCallback->onAudioSourceTypesAttr(callbackId, callStatus, replyAudioSourceTypesVec);
			}
		);
	}
}

jobject JNITestAudioControllerClient::nativeGetVolumeInfoAttr(JNIEnv *env, jobject thiz) {
    v2_Test_Audio::Controller::VolumeInfo volumeInfo;
	mNativeClient->getVolumeInfoAttr(volumeInfo);
	jobject jVolumeInfoVolumeInfoObj = env->NewObject(mDepsCache->mVolumeInfoClass, mDepsCache->mVolumeInfoCtorId,
		(uint8_t)volumeInfo.getVolume(),
		(uint8_t)volumeInfo.getMaxSoftVolume(),
		(uint8_t)volumeInfo.getLastVolume(),
		volumeInfo.getMute() ? JNI_TRUE : JNI_FALSE);
	return jVolumeInfoVolumeInfoObj;
}

void JNITestAudioControllerClient::nativeGetVolumeInfoAttrAsync(JNIEnv *env, jobject thiz, jobject jCallback) {
    if (jCallback == nullptr) {
		mNativeClient->getVolumeInfoAttrAsync(nullptr);
	} else {
		uint32_t callbackId = 0;
		if (!mAsyncCallback->registerCallback(env, jCallback, "onVolumeInfoAttr", "(Lai/ai/umos/soa/common/AsyncCallStatus;Lai/umos/soa/test/audio/data/VolumeInfo;)V", callbackId)) {
			return;
		}
		mNativeClient->getVolumeInfoAttrAsync([callbackId](const CommonAPI::CallStatus& callStatus, const v2_Test_Audio::Controller::VolumeInfo& replyVolumeInfo) {
				mAsyncCallback->onVolumeInfoAttr(callbackId, callStatus, replyVolumeInfo);
			}
		);
	}
}

jobject JNITestAudioControllerClient::nativeGetKeyValueByteBufferMapAttr(JNIEnv *env, jobject thiz) {
    v2_Test_Audio::Controller::KeyValueByteBuffer keyValueByteBufferMap;
	mNativeClient->getKeyValueByteBufferMapAttr(keyValueByteBufferMap);
	jobject jKeyValueByteBufferKeyValueByteBufferMapHashMapObj = env->NewObject(mDepsCache->mHashMapClass, mDepsCache->mHashMapCtorId);
	for (const auto& mapEntry : keyValueByteBufferMap){
		jobject jIntValueObj = env->NewObject(mDepsCache->mIntClass, mDepsCache->mIntInitId, (uint32_t)mapEntry.first);
		jsize jmapEntrySecondSize = static_cast<jsize>(mapEntry.second.size());
		jbyteArray jValueByteBufferUInt8ArrayObj = env->NewByteArray(jmapEntrySecondSize);
		for (jsize idx = 0; idx < jmapEntrySecondSize; ++idx) {
			jbyte valueByteBuffer = mapEntry.second[idx];
			env->SetByteArrayRegion(jValueByteBufferUInt8ArrayObj, idx, 1, &valueByteBuffer);
		}
		env->CallObjectMethod(jKeyValueByteBufferKeyValueByteBufferMapHashMapObj, mDepsCache->mHashMapPutId, jIntValueObj, jValueByteBufferUInt8ArrayObj);
		env->DeleteLocalRef(jIntValueObj);
		env->DeleteLocalRef(jValueByteBufferUInt8ArrayObj);
	}
	jobject jKeyValueByteBufferKeyValueByteBufferMapObj = env->NewObject(mDepsCache->mKeyValueByteBufferClass, mDepsCache->mKeyValueByteBufferCtorId, jKeyValueByteBufferKeyValueByteBufferMapHashMapObj);
	return jKeyValueByteBufferKeyValueByteBufferMapObj;
}

void JNITestAudioControllerClient::nativeGetKeyValueByteBufferMapAttrAsync(JNIEnv *env, jobject thiz, jobject jCallback) {
    if (jCallback == nullptr) {
		mNativeClient->getKeyValueByteBufferMapAttrAsync(nullptr);
	} else {
		uint32_t callbackId = 0;
		if (!mAsyncCallback->registerCallback(env, jCallback, "onKeyValueByteBufferMapAttr", "(Lai/ai/umos/soa/common/AsyncCallStatus;Lai/umos/soa/test/audio/data/KeyValueByteBuffer;)V", callbackId)) {
			return;
		}
		mNativeClient->getKeyValueByteBufferMapAttrAsync([callbackId](const CommonAPI::CallStatus& callStatus, const v2_Test_Audio::Controller::KeyValueByteBuffer& replyKeyValueByteBufferMap) {
				mAsyncCallback->onKeyValueByteBufferMapAttr(callbackId, callStatus, replyKeyValueByteBufferMap);
			}
		);
	}
}

jobject JNITestAudioControllerClient::nativeGetDeviceStateMapAttr(JNIEnv *env, jobject thiz) {
    v2_Test_Audio::Controller::DeviceStateMap deviceStateMap;
	mNativeClient->getDeviceStateMapAttr(deviceStateMap);
	jobject jDeviceStateMapDeviceStateMapHashMapObj = env->NewObject(mDepsCache->mHashMapClass, mDepsCache->mHashMapCtorId);
	for (const auto& mapEntry : deviceStateMap){
		jobjectArray jAudioDeviceTypeKeyAudioDeviceTypeEnumArray = (jobjectArray)env->CallStaticObjectMethod(mDepsCache->mAudioDeviceTypeClass, mDepsCache->mAudioDeviceTypeValuesId);
		jobjectArray jAudioFocusStatusTypeValueAudioFocusStatusTypeEnumArray = (jobjectArray)env->CallStaticObjectMethod(mDepsCache->mAudioFocusStatusTypeClass, mDepsCache->mAudioFocusStatusTypeValuesId);
		jobject jAudioDeviceTypeKeyAudioDeviceTypeObj = env->GetObjectArrayElement(jAudioDeviceTypeKeyAudioDeviceTypeEnumArray, static_cast<int>((uint8_t)(mapEntry.first.value_)));
		jobject jAudioFocusStatusTypeValueAudioFocusStatusTypeObj = env->GetObjectArrayElement(jAudioFocusStatusTypeValueAudioFocusStatusTypeEnumArray, static_cast<int>((uint8_t)(mapEntry.second.value_)));
		env->CallObjectMethod(jDeviceStateMapDeviceStateMapHashMapObj, mDepsCache->mHashMapPutId, jAudioDeviceTypeKeyAudioDeviceTypeObj, jAudioFocusStatusTypeValueAudioFocusStatusTypeObj);
		env->DeleteLocalRef(jAudioDeviceTypeKeyAudioDeviceTypeObj);
		env->DeleteLocalRef(jAudioFocusStatusTypeValueAudioFocusStatusTypeObj);
	}
	jobject jDeviceStateMapDeviceStateMapObj = env->NewObject(mDepsCache->mDeviceStateMapClass, mDepsCache->mDeviceStateMapCtorId, jDeviceStateMapDeviceStateMapHashMapObj);
	return jDeviceStateMapDeviceStateMapObj;
}

void JNITestAudioControllerClient::nativeGetDeviceStateMapAttrAsync(JNIEnv *env, jobject thiz, jobject jCallback) {
    if (jCallback == nullptr) {
		mNativeClient->getDeviceStateMapAttrAsync(nullptr);
	} else {
		uint32_t callbackId = 0;
		if (!mAsyncCallback->registerCallback(env, jCallback, "onDeviceStateMapAttr", "(Lai/ai/umos/soa/common/AsyncCallStatus;Lai/umos/soa/test/audio/data/DeviceStateMap;)V", callbackId)) {
			return;
		}
		mNativeClient->getDeviceStateMapAttrAsync([callbackId](const CommonAPI::CallStatus& callStatus, const v2_Test_Audio::Controller::DeviceStateMap& replyDeviceStateMap) {
				mAsyncCallback->onDeviceStateMapAttr(callbackId, callStatus, replyDeviceStateMap);
			}
		);
	}
}

jobject JNITestAudioControllerClient::nativeGetStringToShortMapAttr(JNIEnv *env, jobject thiz) {
    v2_Test_Audio::Controller::StringToShortMap stringToShortMap;
	mNativeClient->getStringToShortMapAttr(stringToShortMap);
	jobject jStringToShortMapStringToShortMapHashMapObj = env->NewObject(mDepsCache->mHashMapClass, mDepsCache->mHashMapCtorId);
	for (const auto& mapEntry : stringToShortMap){
		jobject jShortValueObj = env->NewObject(mDepsCache->mShortClass, mDepsCache->mShortInitId, (uint16_t)mapEntry.second);
		jstring jKeyStringStr = env->NewStringUTF(mapEntry.first.c_str());
		env->CallObjectMethod(jStringToShortMapStringToShortMapHashMapObj, mDepsCache->mHashMapPutId, jKeyStringStr, jShortValueObj);
		env->DeleteLocalRef(jKeyStringStr);
		env->DeleteLocalRef(jShortValueObj);
	}
	jobject jStringToShortMapStringToShortMapObj = env->NewObject(mDepsCache->mStringToShortMapClass, mDepsCache->mStringToShortMapCtorId, jStringToShortMapStringToShortMapHashMapObj);
	return jStringToShortMapStringToShortMapObj;
}

void JNITestAudioControllerClient::nativeGetStringToShortMapAttrAsync(JNIEnv *env, jobject thiz, jobject jCallback) {
    if (jCallback == nullptr) {
		mNativeClient->getStringToShortMapAttrAsync(nullptr);
	} else {
		uint32_t callbackId = 0;
		if (!mAsyncCallback->registerCallback(env, jCallback, "onStringToShortMapAttr", "(Lai/ai/umos/soa/common/AsyncCallStatus;Lai/umos/soa/test/audio/data/StringToShortMap;)V", callbackId)) {
			return;
		}
		mNativeClient->getStringToShortMapAttrAsync([callbackId](const CommonAPI::CallStatus& callStatus, const v2_Test_Audio::Controller::StringToShortMap& replyStringToShortMap) {
				mAsyncCallback->onStringToShortMapAttr(callbackId, callStatus, replyStringToShortMap);
			}
		);
	}
}

jobject JNITestAudioControllerClient::nativeObtainFocus(JNIEnv *env, jobject thiz, jstring jAudioSourceType, jobject jDevice) {
    const char *jAudioSourceTypeCharStr = env->GetStringUTFChars(jAudioSourceType,0);
	std::string inputjAudioSourceTypeStr(jAudioSourceTypeCharStr);
	jint jDeviceValue = env->CallIntMethod(jDevice, mDepsCache->mAudioDeviceTypeOrdinalMethodId);
	v2_Test_Audio::Controller::AudioDeviceType inputDevice;
	inputDevice.value_ = (int)jDeviceValue;
	v2_Test_Audio::Controller::FocusControlError replyErrFocusControlError;
	v2_Test_Audio::Controller::AudioFocusResultType replyRes;
	mNativeClient->ObtainFocus(inputjAudioSourceTypeStr, inputDevice, replyErrFocusControlError, replyRes);

	jobjectArray jFocusControlErrorErrFocusControlErrorEnumArray = (jobjectArray)env->CallStaticObjectMethod(mDepsCache->mFocusControlErrorClass, mDepsCache->mFocusControlErrorValuesId);
	jobjectArray jAudioFocusResultTypeReplyResEnumArray = (jobjectArray)env->CallStaticObjectMethod(mDepsCache->mAudioFocusResultTypeClass, mDepsCache->mAudioFocusResultTypeValuesId);
	jobject jFocusControlErrorErrFocusControlErrorObj = env->GetObjectArrayElement(jFocusControlErrorErrFocusControlErrorEnumArray, static_cast<int>((uint8_t)(replyErrFocusControlError.value_)));
	jobject jAudioFocusResultTypeReplyResObj = env->GetObjectArrayElement(jAudioFocusResultTypeReplyResEnumArray, static_cast<int>((uint8_t)(replyRes.value_)));
	jobject jRetReplyObtainFocusObj = env->NewObject(mDepsCache->mReplyObtainFocusClass, mDepsCache->mReplyObtainFocusCtorId,
		jFocusControlErrorErrFocusControlErrorObj,
		jAudioFocusResultTypeReplyResObj);
	return jRetReplyObtainFocusObj;
}

void JNITestAudioControllerClient::nativeObtainFocusAsync(JNIEnv *env, jobject thiz, jstring jAudioSourceType, jobject jDevice, jobject jCallback) {
    const char *jAudioSourceTypeCharStr = env->GetStringUTFChars(jAudioSourceType,0);
	std::string inputjAudioSourceTypeStr(jAudioSourceTypeCharStr);
	jint jDeviceValue = env->CallIntMethod(jDevice, mDepsCache->mAudioDeviceTypeOrdinalMethodId);
	v2_Test_Audio::Controller::AudioDeviceType inputDevice;
	inputDevice.value_ = (int)jDeviceValue;
	if (jCallback == nullptr) {
		mNativeClient->ObtainFocusAsync(inputjAudioSourceTypeStr, inputDevice, nullptr);
	} else {
		uint32_t callbackId = 0;
		if (!mAsyncCallback->registerCallback(env, jCallback, "onObtainFocus", "(Lai/ai/umos/soa/common/AsyncCallStatus;Lai/umos/soa/test/audio/data/FocusControlError;Lai/umos/soa/test/audio/data/AudioFocusResultType;)V", callbackId)) {
			return;
		}
		mNativeClient->ObtainFocusAsync(inputjAudioSourceTypeStr, inputDevice,
			[callbackId](const CommonAPI::CallStatus& callStatus, const v2_Test_Audio::Controller::FocusControlError& replyErrFocusControlError, const v2_Test_Audio::Controller::AudioFocusResultType& replyRes) {
				mAsyncCallback->onObtainFocus(callbackId, callStatus, replyErrFocusControlError, replyRes);
			}
		);
	}
}

jobject JNITestAudioControllerClient::nativeReleaseFocus(JNIEnv *env, jobject thiz, jstring jAudioSourceType, jobject jDevice) {
    const char *jAudioSourceTypeCharStr = env->GetStringUTFChars(jAudioSourceType,0);
	std::string inputjAudioSourceTypeStr(jAudioSourceTypeCharStr);
	jint jDeviceValue = env->CallIntMethod(jDevice, mDepsCache->mAudioDeviceTypeOrdinalMethodId);
	v2_Test_Audio::Controller::AudioDeviceType inputDevice;
	inputDevice.value_ = (int)jDeviceValue;
	v2_Test_Audio::Controller::FocusControlError replyErrFocusControlError;
	mNativeClient->ReleaseFocus(inputjAudioSourceTypeStr, inputDevice, replyErrFocusControlError);

	jobjectArray jFocusControlErrorErrFocusControlErrorEnumArray = (jobjectArray)env->CallStaticObjectMethod(mDepsCache->mFocusControlErrorClass, mDepsCache->mFocusControlErrorValuesId);
	jobject jFocusControlErrorErrFocusControlErrorObj = env->GetObjectArrayElement(jFocusControlErrorErrFocusControlErrorEnumArray, static_cast<int>((uint8_t)(replyErrFocusControlError.value_)));
	return jFocusControlErrorErrFocusControlErrorObj;
}

void JNITestAudioControllerClient::nativeReleaseFocusAsync(JNIEnv *env, jobject thiz, jstring jAudioSourceType, jobject jDevice, jobject jCallback) {
    const char *jAudioSourceTypeCharStr = env->GetStringUTFChars(jAudioSourceType,0);
	std::string inputjAudioSourceTypeStr(jAudioSourceTypeCharStr);
	jint jDeviceValue = env->CallIntMethod(jDevice, mDepsCache->mAudioDeviceTypeOrdinalMethodId);
	v2_Test_Audio::Controller::AudioDeviceType inputDevice;
	inputDevice.value_ = (int)jDeviceValue;
	if (jCallback == nullptr) {
		mNativeClient->ReleaseFocusAsync(inputjAudioSourceTypeStr, inputDevice, nullptr);
	} else {
		uint32_t callbackId = 0;
		if (!mAsyncCallback->registerCallback(env, jCallback, "onReleaseFocus", "(Lai/ai/umos/soa/common/AsyncCallStatus;Lai/umos/soa/test/audio/data/FocusControlError;)V", callbackId)) {
			return;
		}
		mNativeClient->ReleaseFocusAsync(inputjAudioSourceTypeStr, inputDevice,
			[callbackId](const CommonAPI::CallStatus& callStatus, const v2_Test_Audio::Controller::FocusControlError& replyErrFocusControlError) {
				mAsyncCallback->onReleaseFocus(callbackId, callStatus, replyErrFocusControlError);
			}
		);
	}
}

jobject JNITestAudioControllerClient::nativeSetVolume(JNIEnv *env, jobject thiz, jbyte jVolume) {
    v2_Test_Audio::Controller::VolumeControlError replyErrVolumeControlError;
	mNativeClient->SetVolume((uint8_t)jVolume, replyErrVolumeControlError);

	jobjectArray jVolumeControlErrorErrVolumeControlErrorEnumArray = (jobjectArray)env->CallStaticObjectMethod(mDepsCache->mVolumeControlErrorClass, mDepsCache->mVolumeControlErrorValuesId);
	jobject jVolumeControlErrorErrVolumeControlErrorObj = env->GetObjectArrayElement(jVolumeControlErrorErrVolumeControlErrorEnumArray, static_cast<int>((uint8_t)(replyErrVolumeControlError.value_)));
	return jVolumeControlErrorErrVolumeControlErrorObj;
}

void JNITestAudioControllerClient::nativeSetVolumeAsync(JNIEnv *env, jobject thiz, jbyte jVolume, jobject jCallback) {
    if (jCallback == nullptr) {
		mNativeClient->SetVolumeAsync((uint8_t)jVolume, nullptr);
	} else {
		uint32_t callbackId = 0;
		if (!mAsyncCallback->registerCallback(env, jCallback, "onSetVolume", "(Lai/ai/umos/soa/common/AsyncCallStatus;Lai/umos/soa/test/audio/data/VolumeControlError;)V", callbackId)) {
			return;
		}
		mNativeClient->SetVolumeAsync((uint8_t)jVolume,
			[callbackId](const CommonAPI::CallStatus& callStatus, const v2_Test_Audio::Controller::VolumeControlError& replyErrVolumeControlError) {
				mAsyncCallback->onSetVolume(callbackId, callStatus, replyErrVolumeControlError);
			}
		);
	}
}

jobject JNITestAudioControllerClient::nativeSetSourceVolume(JNIEnv *env, jobject thiz, jstring jAudioSourceType, jshort jVolume, jint jMaxVolume) {
    const char *jAudioSourceTypeCharStr = env->GetStringUTFChars(jAudioSourceType,0);
	std::string inputjAudioSourceTypeStr(jAudioSourceTypeCharStr);
	v2_Test_Audio::Controller::VolumeControlError replyErrVolumeControlError;
	mNativeClient->SetSourceVolume(inputjAudioSourceTypeStr, (uint16_t)jVolume, (uint32_t)jMaxVolume, replyErrVolumeControlError);

	jobjectArray jVolumeControlErrorErrVolumeControlErrorEnumArray = (jobjectArray)env->CallStaticObjectMethod(mDepsCache->mVolumeControlErrorClass, mDepsCache->mVolumeControlErrorValuesId);
	jobject jVolumeControlErrorErrVolumeControlErrorObj = env->GetObjectArrayElement(jVolumeControlErrorErrVolumeControlErrorEnumArray, static_cast<int>((uint8_t)(replyErrVolumeControlError.value_)));
	return jVolumeControlErrorErrVolumeControlErrorObj;
}

void JNITestAudioControllerClient::nativeSetSourceVolumeAsync(JNIEnv *env, jobject thiz, jstring jAudioSourceType, jshort jVolume, jint jMaxVolume, jobject jCallback) {
    const char *jAudioSourceTypeCharStr = env->GetStringUTFChars(jAudioSourceType,0);
	std::string inputjAudioSourceTypeStr(jAudioSourceTypeCharStr);
	if (jCallback == nullptr) {
		mNativeClient->SetSourceVolumeAsync(inputjAudioSourceTypeStr, (uint16_t)jVolume, (uint32_t)jMaxVolume, nullptr);
	} else {
		uint32_t callbackId = 0;
		if (!mAsyncCallback->registerCallback(env, jCallback, "onSetSourceVolume", "(Lai/ai/umos/soa/common/AsyncCallStatus;Lai/umos/soa/test/audio/data/VolumeControlError;)V", callbackId)) {
			return;
		}
		mNativeClient->SetSourceVolumeAsync(inputjAudioSourceTypeStr, (uint16_t)jVolume, (uint32_t)jMaxVolume,
			[callbackId](const CommonAPI::CallStatus& callStatus, const v2_Test_Audio::Controller::VolumeControlError& replyErrVolumeControlError) {
				mAsyncCallback->onSetSourceVolume(callbackId, callStatus, replyErrVolumeControlError);
			}
		);
	}
}

jobject JNITestAudioControllerClient::nativeGetAudioSourceStatus(JNIEnv *env, jobject thiz, jstring jAudioSourceType) {
    const char *jAudioSourceTypeCharStr = env->GetStringUTFChars(jAudioSourceType,0);
	std::string inputjAudioSourceTypeStr(jAudioSourceTypeCharStr);
	v2_Test_Audio::Controller::ErrorType replyErrErrorType;
	v2_Test_Audio::Controller::AudioSourceStatusInfo replyAudioSourceStatusInfo;
	mNativeClient->GetAudioSourceStatus(inputjAudioSourceTypeStr, replyErrErrorType, replyAudioSourceStatusInfo);

	jobjectArray jErrorTypeErrErrorTypeEnumArray = (jobjectArray)env->CallStaticObjectMethod(mDepsCache->mErrorTypeClass, mDepsCache->mErrorTypeValuesId);
	auto replyAudioSourceStatusInfoFocusStatusFocusStatus = replyAudioSourceStatusInfo.getFocusStatus();
	auto replyAudioSourceStatusInfoFocusStatusFocusStatusAudioDeviceTypeDevice = replyAudioSourceStatusInfoFocusStatusFocusStatus.getDevice();
	jobjectArray jAudioDeviceTypeDeviceEnumArray = (jobjectArray)env->CallStaticObjectMethod(mDepsCache->mAudioDeviceTypeClass, mDepsCache->mAudioDeviceTypeValuesId);
	auto replyAudioSourceStatusInfoFocusStatusFocusStatusAudioFocusStatusTypeStatus = replyAudioSourceStatusInfoFocusStatusFocusStatus.getStatus();
	jobjectArray jAudioFocusStatusTypeStatusEnumArray = (jobjectArray)env->CallStaticObjectMethod(mDepsCache->mAudioFocusStatusTypeClass, mDepsCache->mAudioFocusStatusTypeValuesId);
	auto replyAudioSourceStatusInfoVolumeInfoVolumeInfo = replyAudioSourceStatusInfo.getVolumeInfo();
	jobject jErrorTypeErrErrorTypeObj = env->GetObjectArrayElement(jErrorTypeErrErrorTypeEnumArray, static_cast<int>((uint8_t)(replyErrErrorType.value_)));
	jstring jReplyAudioSourceStatusInfoAudioSourceTypeStr = env->NewStringUTF(replyAudioSourceStatusInfo.getAudioSourceType().c_str());
	jstring jReplyAudioSourceStatusInfoFocusStatusFocusStatusAudioSourceTypeStr = env->NewStringUTF(replyAudioSourceStatusInfoFocusStatusFocusStatus.getAudioSourceType().c_str());
	jobject jAudioDeviceTypeDeviceObj = env->GetObjectArrayElement(jAudioDeviceTypeDeviceEnumArray, static_cast<int>((uint8_t)(replyAudioSourceStatusInfoFocusStatusFocusStatusAudioDeviceTypeDevice.value_)));
	jobject jAudioFocusStatusTypeStatusObj = env->GetObjectArrayElement(jAudioFocusStatusTypeStatusEnumArray, static_cast<int>((uint8_t)(replyAudioSourceStatusInfoFocusStatusFocusStatusAudioFocusStatusTypeStatus.value_)));
	jobject jFocusStatusFocusStatusObj = env->NewObject(mDepsCache->mFocusStatusClass, mDepsCache->mFocusStatusCtorId,
		jReplyAudioSourceStatusInfoFocusStatusFocusStatusAudioSourceTypeStr,
		jAudioDeviceTypeDeviceObj,
		jAudioFocusStatusTypeStatusObj);
	jobject jVolumeInfoVolumeInfoObj = env->NewObject(mDepsCache->mVolumeInfoClass, mDepsCache->mVolumeInfoCtorId,
		(uint8_t)replyAudioSourceStatusInfoVolumeInfoVolumeInfo.getVolume(),
		(uint8_t)replyAudioSourceStatusInfoVolumeInfoVolumeInfo.getMaxSoftVolume(),
		(uint8_t)replyAudioSourceStatusInfoVolumeInfoVolumeInfo.getLastVolume(),
		replyAudioSourceStatusInfoVolumeInfoVolumeInfo.getMute() ? JNI_TRUE : JNI_FALSE);
	jobject jAudioSourceStatusInfoReplyAudioSourceStatusInfoObj = env->NewObject(mDepsCache->mAudioSourceStatusInfoClass, mDepsCache->mAudioSourceStatusInfoCtorId,
		jReplyAudioSourceStatusInfoAudioSourceTypeStr,
		jFocusStatusFocusStatusObj,
		jVolumeInfoVolumeInfoObj);
	jobject jRetReplyGetAudioSourceStatusObj = env->NewObject(mDepsCache->mReplyGetAudioSourceStatusClass, mDepsCache->mReplyGetAudioSourceStatusCtorId,
		jErrorTypeErrErrorTypeObj,
		jAudioSourceStatusInfoReplyAudioSourceStatusInfoObj);
	return jRetReplyGetAudioSourceStatusObj;
}

void JNITestAudioControllerClient::nativeGetAudioSourceStatusAsync(JNIEnv *env, jobject thiz, jstring jAudioSourceType, jobject jCallback) {
    const char *jAudioSourceTypeCharStr = env->GetStringUTFChars(jAudioSourceType,0);
	std::string inputjAudioSourceTypeStr(jAudioSourceTypeCharStr);
	if (jCallback == nullptr) {
		mNativeClient->GetAudioSourceStatusAsync(inputjAudioSourceTypeStr, nullptr);
	} else {
		uint32_t callbackId = 0;
		if (!mAsyncCallback->registerCallback(env, jCallback, "onGetAudioSourceStatus", "(Lai/ai/umos/soa/common/AsyncCallStatus;Lai/umos/soa/test/audio/data/ErrorType;Lai/umos/soa/test/audio/data/AudioSourceStatusInfo;)V", callbackId)) {
			return;
		}
		mNativeClient->GetAudioSourceStatusAsync(inputjAudioSourceTypeStr,
			[callbackId](const CommonAPI::CallStatus& callStatus, const v2_Test_Audio::Controller::ErrorType& replyErrErrorType, const v2_Test_Audio::Controller::AudioSourceStatusInfo& replyAudioSourceStatusInfo) {
				mAsyncCallback->onGetAudioSourceStatus(callbackId, callStatus, replyErrErrorType, replyAudioSourceStatusInfo);
			}
		);
	}
}

JNITestAudioControllerClient::JNITestAudioControllerClient() {
	LOGI("JNITestAudioControllerClient::JNITestAudioControllerClient");
}

JNITestAudioControllerClient::~JNITestAudioControllerClient() {
	LOGI("JNITestAudioControllerClient::~JNITestAudioControllerClient");
}

