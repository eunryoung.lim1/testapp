/*
 * Copyright (c) 2023 42dot All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include <jni.h>
#include <memory>

#include <common/JNIHelper.h>

class JNITestAudioControllerDepsCache final {
public:
    JNITestAudioControllerDepsCache(JNIEnv *env) : mEnv(env) {
        initializeCache();
    }
    ~JNITestAudioControllerDepsCache() {
        mEnv->DeleteGlobalRef(mArrayListClass);
		mEnv->DeleteGlobalRef(mJavaStringClass);
		mEnv->DeleteGlobalRef(mHashMapClass);
		mEnv->DeleteGlobalRef(mSetClass);
		mEnv->DeleteGlobalRef(mMapIteratorClass);
		mEnv->DeleteGlobalRef(mMapEntryClass);
		mEnv->DeleteGlobalRef(mByteClass);
		mEnv->DeleteGlobalRef(mShortClass);
		mEnv->DeleteGlobalRef(mIntClass);
		mEnv->DeleteGlobalRef(mVolumeInfoClass);
		mEnv->DeleteGlobalRef(mReplyObtainFocusClass);
		mEnv->DeleteGlobalRef(mFocusStatusClass);
		mEnv->DeleteGlobalRef(mReplyGetAudioSourceStatusClass);
		mEnv->DeleteGlobalRef(mAudioSourceStatusInfoClass);
		mEnv->DeleteGlobalRef(mAudioDeviceTypeClass);
		mEnv->DeleteGlobalRef(mVolumeControlErrorClass);
		mEnv->DeleteGlobalRef(mErrorTypeClass);
		mEnv->DeleteGlobalRef(mFocusControlErrorClass);
		mEnv->DeleteGlobalRef(mAudioFocusResultTypeClass);
		mEnv->DeleteGlobalRef(mAudioFocusStatusTypeClass);
		mEnv->DeleteGlobalRef(mAsyncCallStatusClass);
		mEnv->DeleteGlobalRef(mKeyValueByteBufferClass);
		mEnv->DeleteGlobalRef(mStringToShortMapClass);
		mEnv->DeleteGlobalRef(mKeyValueStringClass);
		mEnv->DeleteGlobalRef(mDeviceStateMapClass);
    }
    void initializeCache() {
        jclass arrayListClass = mEnv->FindClass("java/util/ArrayList");
		mArrayListClass = static_cast<jclass>(mEnv->NewGlobalRef(arrayListClass));
		jclass stringClass = mEnv->FindClass("java/lang/String");
		mJavaStringClass = static_cast<jclass>(mEnv->NewGlobalRef(stringClass));
		jclass hashMapClass  = mEnv->FindClass("java/util/HashMap");
		mHashMapClass = static_cast<jclass>(mEnv->NewGlobalRef(hashMapClass));
		jclass setClass = mEnv->FindClass("java/util/Set");
		mSetClass = static_cast<jclass>(mEnv->NewGlobalRef(setClass));
		jclass iteratorClass = mEnv->FindClass("java/util/Iterator");
		mMapIteratorClass = static_cast<jclass>(mEnv->NewGlobalRef(iteratorClass));
		jclass mapentryClass = mEnv->FindClass("java/util/Map$Entry");
		mMapEntryClass = static_cast<jclass>(mEnv->NewGlobalRef(mapentryClass));
		jclass byteClass = JNIHelper::findClass(mEnv, "java/lang/Byte");
		mByteClass = static_cast<jclass>(mEnv->NewGlobalRef(byteClass));
		jclass shortClass = JNIHelper::findClass(mEnv, "java/lang/Short");
		mShortClass = static_cast<jclass>(mEnv->NewGlobalRef(shortClass));
		jclass intClass = JNIHelper::findClass(mEnv, "java/lang/Integer");
		mIntClass = static_cast<jclass>(mEnv->NewGlobalRef(intClass));
		jclass volumeInfoClass = JNIHelper::findClass(mEnv, "ai/umos/soa/test/audio/data/VolumeInfo");
		mVolumeInfoClass = static_cast<jclass>(mEnv->NewGlobalRef(volumeInfoClass));
		jclass replyObtainFocusClass = JNIHelper::findClass(mEnv, "ai/umos/soa/test/audio/data/ReplyObtainFocus");
		mReplyObtainFocusClass = static_cast<jclass>(mEnv->NewGlobalRef(replyObtainFocusClass));
		jclass focusStatusClass = JNIHelper::findClass(mEnv, "ai/umos/soa/test/audio/data/FocusStatus");
		mFocusStatusClass = static_cast<jclass>(mEnv->NewGlobalRef(focusStatusClass));
		jclass replyGetAudioSourceStatusClass = JNIHelper::findClass(mEnv, "ai/umos/soa/test/audio/data/ReplyGetAudioSourceStatus");
		mReplyGetAudioSourceStatusClass = static_cast<jclass>(mEnv->NewGlobalRef(replyGetAudioSourceStatusClass));
		jclass audioSourceStatusInfoClass = JNIHelper::findClass(mEnv, "ai/umos/soa/test/audio/data/AudioSourceStatusInfo");
		mAudioSourceStatusInfoClass = static_cast<jclass>(mEnv->NewGlobalRef(audioSourceStatusInfoClass));
		jclass audioDeviceTypeClass = JNIHelper::findClass(mEnv, "ai/umos/soa/test/audio/data/AudioDeviceType");
		mAudioDeviceTypeClass = static_cast<jclass>(mEnv->NewGlobalRef(audioDeviceTypeClass));
		jclass volumeControlErrorClass = JNIHelper::findClass(mEnv, "ai/umos/soa/test/audio/data/VolumeControlError");
		mVolumeControlErrorClass = static_cast<jclass>(mEnv->NewGlobalRef(volumeControlErrorClass));
		jclass errorTypeClass = JNIHelper::findClass(mEnv, "ai/umos/soa/test/audio/data/ErrorType");
		mErrorTypeClass = static_cast<jclass>(mEnv->NewGlobalRef(errorTypeClass));
		jclass focusControlErrorClass = JNIHelper::findClass(mEnv, "ai/umos/soa/test/audio/data/FocusControlError");
		mFocusControlErrorClass = static_cast<jclass>(mEnv->NewGlobalRef(focusControlErrorClass));
		jclass audioFocusResultTypeClass = JNIHelper::findClass(mEnv, "ai/umos/soa/test/audio/data/AudioFocusResultType");
		mAudioFocusResultTypeClass = static_cast<jclass>(mEnv->NewGlobalRef(audioFocusResultTypeClass));
		jclass audioFocusStatusTypeClass = JNIHelper::findClass(mEnv, "ai/umos/soa/test/audio/data/AudioFocusStatusType");
		mAudioFocusStatusTypeClass = static_cast<jclass>(mEnv->NewGlobalRef(audioFocusStatusTypeClass));
		jclass asyncCallStatusClass = JNIHelper::findClass(mEnv, "ai/umos/soa/common/AsyncCallStatus");
		mAsyncCallStatusClass = static_cast<jclass>(mEnv->NewGlobalRef(asyncCallStatusClass));
		jclass keyValueByteBufferClass = JNIHelper::findClass(mEnv, "ai/umos/soa/test/audio/data/KeyValueByteBuffer");
		mKeyValueByteBufferClass = static_cast<jclass>(mEnv->NewGlobalRef(keyValueByteBufferClass));
		jclass stringToShortMapClass = JNIHelper::findClass(mEnv, "ai/umos/soa/test/audio/data/StringToShortMap");
		mStringToShortMapClass = static_cast<jclass>(mEnv->NewGlobalRef(stringToShortMapClass));
		jclass keyValueStringClass = JNIHelper::findClass(mEnv, "ai/umos/soa/test/audio/data/KeyValueString");
		mKeyValueStringClass = static_cast<jclass>(mEnv->NewGlobalRef(keyValueStringClass));
		jclass deviceStateMapClass = JNIHelper::findClass(mEnv, "ai/umos/soa/test/audio/data/DeviceStateMap");
		mDeviceStateMapClass = static_cast<jclass>(mEnv->NewGlobalRef(deviceStateMapClass));

        mArrayListCtorId = mEnv->GetMethodID(mArrayListClass, "<init>", "()V");
		mArrayListAddId = mEnv->GetMethodID(mArrayListClass, "add", "(Ljava/lang/Object;)Z");
		mHashMapCtorId = mEnv->GetMethodID(mHashMapClass, "<init>", "()V");
		mHashMapPutId = mEnv->GetMethodID(mHashMapClass, "put", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;");
		mHashMapGetKeyId = mEnv->GetMethodID(mMapEntryClass, "getKey", "()Ljava/lang/Object;");
		mHashMapGetValueId = mEnv->GetMethodID(mMapEntryClass, "getValue", "()Ljava/lang/Object;");
		mMapIteratorHasNextId = mEnv->GetMethodID(mMapIteratorClass, "hasNext", "()Z");
		mMapIteratorNextId = mEnv->GetMethodID(mMapIteratorClass, "next", "()Ljava/lang/Object;");
		mHashMapEntrySetId = mEnv->GetMethodID(mHashMapClass, "entrySet", "()Ljava/util/Set;");
		mHashMapIteratorId = mEnv->GetMethodID(mSetClass, "iterator", "()Ljava/util/Iterator;");
		mByteInitId = mEnv->GetMethodID(mByteClass, "<init>", "(B)V");
		mByteValueId = mEnv->GetMethodID(mByteClass, "byteValue", "()B");
		mShortInitId = mEnv->GetMethodID(mShortClass, "<init>", "(S)V");
		mShortValueId = mEnv->GetMethodID(mShortClass, "shortValue", "()S");
		mIntInitId = mEnv->GetMethodID(mIntClass, "<init>", "(I)V");
		mIntValueId = mEnv->GetMethodID(mIntClass, "intValue", "()I");
		mVolumeInfoCtorId = JNIHelper::getMethodId(mEnv, volumeInfoClass, "<init>", "(BBBZ)V");
		mReplyObtainFocusCtorId = JNIHelper::getMethodId(mEnv, replyObtainFocusClass, "<init>", "(Lai/umos/soa/test/audio/data/FocusControlError;Lai/umos/soa/test/audio/data/AudioFocusResultType;)V");
		mFocusStatusCtorId = JNIHelper::getMethodId(mEnv, focusStatusClass, "<init>", "(Ljava/lang/String;Lai/umos/soa/test/audio/data/AudioDeviceType;Lai/umos/soa/test/audio/data/AudioFocusStatusType;)V");
		mReplyGetAudioSourceStatusCtorId = JNIHelper::getMethodId(mEnv, replyGetAudioSourceStatusClass, "<init>", "(Lai/umos/soa/test/audio/data/ErrorType;Lai/umos/soa/test/audio/data/AudioSourceStatusInfo;)V");
		mAudioSourceStatusInfoCtorId = JNIHelper::getMethodId(mEnv, audioSourceStatusInfoClass, "<init>", "(Ljava/lang/String;Lai/umos/soa/test/audio/data/FocusStatus;Lai/umos/soa/test/audio/data/VolumeInfo;)V");
		mAudioDeviceTypeOrdinalMethodId = JNIHelper::getMethodId(mEnv, audioDeviceTypeClass, "ordinal", "()I");
		mAudioDeviceTypeValuesId = JNIHelper::getStaticMethodId(mEnv, audioDeviceTypeClass, "values", "()[Lai/umos/soa/test/audio/data/AudioDeviceType;");
		mVolumeControlErrorOrdinalMethodId = JNIHelper::getMethodId(mEnv, volumeControlErrorClass, "ordinal", "()I");
		mVolumeControlErrorValuesId = JNIHelper::getStaticMethodId(mEnv, volumeControlErrorClass, "values", "()[Lai/umos/soa/test/audio/data/VolumeControlError;");
		mErrorTypeOrdinalMethodId = JNIHelper::getMethodId(mEnv, errorTypeClass, "ordinal", "()I");
		mErrorTypeValuesId = JNIHelper::getStaticMethodId(mEnv, errorTypeClass, "values", "()[Lai/umos/soa/test/audio/data/ErrorType;");
		mFocusControlErrorOrdinalMethodId = JNIHelper::getMethodId(mEnv, focusControlErrorClass, "ordinal", "()I");
		mFocusControlErrorValuesId = JNIHelper::getStaticMethodId(mEnv, focusControlErrorClass, "values", "()[Lai/umos/soa/test/audio/data/FocusControlError;");
		mAudioFocusResultTypeOrdinalMethodId = JNIHelper::getMethodId(mEnv, audioFocusResultTypeClass, "ordinal", "()I");
		mAudioFocusResultTypeValuesId = JNIHelper::getStaticMethodId(mEnv, audioFocusResultTypeClass, "values", "()[Lai/umos/soa/test/audio/data/AudioFocusResultType;");
		mAudioFocusStatusTypeOrdinalMethodId = JNIHelper::getMethodId(mEnv, audioFocusStatusTypeClass, "ordinal", "()I");
		mAudioFocusStatusTypeValuesId = JNIHelper::getStaticMethodId(mEnv, audioFocusStatusTypeClass, "values", "()[Lai/umos/soa/test/audio/data/AudioFocusStatusType;");
		mAsyncCallStatusOrdinalMethodId = JNIHelper::getMethodId(mEnv, asyncCallStatusClass, "ordinal", "()I");
		mAsyncCallStatusValuesId = JNIHelper::getStaticMethodId(mEnv, asyncCallStatusClass, "values", "()[Lai/umos/soa/common/AsyncCallStatus;");
		mKeyValueByteBufferCtorId = JNIHelper::getMethodId(mEnv, keyValueByteBufferClass, "<init>", "(Ljava/util/HashMap;)V");
		mStringToShortMapCtorId = JNIHelper::getMethodId(mEnv, stringToShortMapClass, "<init>", "(Ljava/util/HashMap;)V");
		mKeyValueStringCtorId = JNIHelper::getMethodId(mEnv, keyValueStringClass, "<init>", "(Ljava/util/HashMap;)V");
		mDeviceStateMapCtorId = JNIHelper::getMethodId(mEnv, deviceStateMapClass, "<init>", "(Ljava/util/HashMap;)V");

        mVolumeInfo.volumeId = JNIHelper::getFieldId(mEnv, mVolumeInfoClass, "volume", "B");
		mVolumeInfo.maxSoftVolumeId = JNIHelper::getFieldId(mEnv, mVolumeInfoClass, "maxSoftVolume", "B");
		mVolumeInfo.lastVolumeId = JNIHelper::getFieldId(mEnv, mVolumeInfoClass, "lastVolume", "B");
		mVolumeInfo.muteId = JNIHelper::getFieldId(mEnv, mVolumeInfoClass, "mute", "Z");
		mReplyObtainFocus.errFocusControlErrorId = JNIHelper::getFieldId(mEnv, mReplyObtainFocusClass, "errFocusControlError", "Lai/umos/soa/test/audio/data/FocusControlError;");
		mReplyObtainFocus.replyResId = JNIHelper::getFieldId(mEnv, mReplyObtainFocusClass, "replyRes", "Lai/umos/soa/test/audio/data/AudioFocusResultType;");
		mFocusStatus.audioSourceTypeId = JNIHelper::getFieldId(mEnv, mFocusStatusClass, "audioSourceType", "Ljava/lang/String;");
		mFocusStatus.deviceId = JNIHelper::getFieldId(mEnv, mFocusStatusClass, "device", "Lai/umos/soa/test/audio/data/AudioDeviceType;");
		mFocusStatus.statusId = JNIHelper::getFieldId(mEnv, mFocusStatusClass, "status", "Lai/umos/soa/test/audio/data/AudioFocusStatusType;");
		mReplyGetAudioSourceStatus.errErrorTypeId = JNIHelper::getFieldId(mEnv, mReplyGetAudioSourceStatusClass, "errErrorType", "Lai/umos/soa/test/audio/data/ErrorType;");
		mReplyGetAudioSourceStatus.replyAudioSourceStatusInfoId = JNIHelper::getFieldId(mEnv, mReplyGetAudioSourceStatusClass, "replyAudioSourceStatusInfo", "Lai/umos/soa/test/audio/data/AudioSourceStatusInfo;");
		mAudioSourceStatusInfo.audioSourceTypeId = JNIHelper::getFieldId(mEnv, mAudioSourceStatusInfoClass, "audioSourceType", "Ljava/lang/String;");
		mAudioSourceStatusInfo.focusStatusId = JNIHelper::getFieldId(mEnv, mAudioSourceStatusInfoClass, "focusStatus", "Lai/umos/soa/test/audio/data/FocusStatus;");
		mAudioSourceStatusInfo.volumeInfoId = JNIHelper::getFieldId(mEnv, mAudioSourceStatusInfoClass, "volumeInfo", "Lai/umos/soa/test/audio/data/VolumeInfo;");
		mKeyValueByteBuffer.mapDataId = JNIHelper::getFieldId(mEnv, mKeyValueByteBufferClass, "mapData", "Ljava/util/HashMap;");
		mStringToShortMap.mapDataId = JNIHelper::getFieldId(mEnv, mStringToShortMapClass, "mapData", "Ljava/util/HashMap;");
		mKeyValueString.mapDataId = JNIHelper::getFieldId(mEnv, mKeyValueStringClass, "mapData", "Ljava/util/HashMap;");
		mDeviceStateMap.mapDataId = JNIHelper::getFieldId(mEnv, mDeviceStateMapClass, "mapData", "Ljava/util/HashMap;");
    }

    struct JNIVolumeInfo {
		jfieldID volumeId;
		jfieldID maxSoftVolumeId;
		jfieldID lastVolumeId;
		jfieldID muteId;
	};
	struct JNIReplyObtainFocus {
		jfieldID errFocusControlErrorId;
		jfieldID replyResId;
	};
	struct JNIFocusStatus {
		jfieldID audioSourceTypeId;
		jfieldID deviceId;
		jfieldID statusId;
	};
	struct JNIReplyGetAudioSourceStatus {
		jfieldID errErrorTypeId;
		jfieldID replyAudioSourceStatusInfoId;
	};
	struct JNIAudioSourceStatusInfo {
		jfieldID audioSourceTypeId;
		jfieldID focusStatusId;
		jfieldID volumeInfoId;
	};
	struct JNIKeyValueByteBufferMap {
		jfieldID mapDataId;
	};
	struct JNIStringToShortMapMap {
		jfieldID mapDataId;
	};
	struct JNIKeyValueStringMap {
		jfieldID mapDataId;
	};
	struct JNIDeviceStateMapMap {
		jfieldID mapDataId;
	};
public:
    JNIVolumeInfo	mVolumeInfo;
	JNIReplyObtainFocus	mReplyObtainFocus;
	JNIFocusStatus	mFocusStatus;
	JNIReplyGetAudioSourceStatus	mReplyGetAudioSourceStatus;
	JNIAudioSourceStatusInfo	mAudioSourceStatusInfo;
	JNIKeyValueByteBufferMap	mKeyValueByteBuffer;
	JNIStringToShortMapMap	mStringToShortMap;
	JNIKeyValueStringMap	mKeyValueString;
	JNIDeviceStateMapMap	mDeviceStateMap;

    jclass		mArrayListClass;
	jclass		mJavaStringClass;
	jclass		mHashMapClass;
	jclass		mSetClass;
	jclass		mMapIteratorClass;
	jclass		mMapEntryClass;
	jclass		mByteClass;
	jclass		mShortClass;
	jclass		mIntClass;
	jclass		mVolumeInfoClass;
	jclass		mReplyObtainFocusClass;
	jclass		mFocusStatusClass;
	jclass		mReplyGetAudioSourceStatusClass;
	jclass		mAudioSourceStatusInfoClass;
	jclass		mAudioDeviceTypeClass;
	jclass		mVolumeControlErrorClass;
	jclass		mErrorTypeClass;
	jclass		mFocusControlErrorClass;
	jclass		mAudioFocusResultTypeClass;
	jclass		mAudioFocusStatusTypeClass;
	jclass		mAsyncCallStatusClass;
	jclass		mKeyValueByteBufferClass;
	jclass		mStringToShortMapClass;
	jclass		mKeyValueStringClass;
	jclass		mDeviceStateMapClass;

    jmethodID	mArrayListCtorId;
	jmethodID	mArrayListAddId;
	jmethodID	mHashMapCtorId;
	jmethodID	mHashMapPutId;
	jmethodID	mHashMapGetKeyId;
	jmethodID	mHashMapGetValueId;
	jmethodID	mMapIteratorHasNextId;
	jmethodID	mMapIteratorNextId;
	jmethodID	mHashMapEntrySetId;
	jmethodID	mHashMapIteratorId;
	jmethodID	mByteInitId;
	jmethodID	mByteValueId;
	jmethodID	mShortInitId;
	jmethodID	mShortValueId;
	jmethodID	mIntInitId;
	jmethodID	mIntValueId;
	jmethodID	mVolumeInfoCtorId;
	jmethodID	mReplyObtainFocusCtorId;
	jmethodID	mFocusStatusCtorId;
	jmethodID	mReplyGetAudioSourceStatusCtorId;
	jmethodID	mAudioSourceStatusInfoCtorId;
	jmethodID	mAudioDeviceTypeOrdinalMethodId;
	jmethodID	mAudioDeviceTypeValuesId;
	jmethodID	mVolumeControlErrorOrdinalMethodId;
	jmethodID	mVolumeControlErrorValuesId;
	jmethodID	mErrorTypeOrdinalMethodId;
	jmethodID	mErrorTypeValuesId;
	jmethodID	mFocusControlErrorOrdinalMethodId;
	jmethodID	mFocusControlErrorValuesId;
	jmethodID	mAudioFocusResultTypeOrdinalMethodId;
	jmethodID	mAudioFocusResultTypeValuesId;
	jmethodID	mAudioFocusStatusTypeOrdinalMethodId;
	jmethodID	mAudioFocusStatusTypeValuesId;
	jmethodID	mAsyncCallStatusOrdinalMethodId;
	jmethodID	mAsyncCallStatusValuesId;
	jmethodID	mKeyValueByteBufferCtorId;
	jmethodID	mStringToShortMapCtorId;
	jmethodID	mKeyValueStringCtorId;
	jmethodID	mDeviceStateMapCtorId;
private:
    JNIEnv*         mEnv;
};

using JNITestAudioControllerDepsCachePtr = std::shared_ptr<JNITestAudioControllerDepsCache>;