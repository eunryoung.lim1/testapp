/*
 * Copyright (c) 2023 42dot All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include <jni.h>

#include <Test/Audio/ITestAudioControllerEventListener.h>
#include <v2/Test/Audio/ControllerProxy.hpp>

#include <test/audio/JNITestAudioControllerDepsCache.h>

using namespace ai::umos::soa::core;

namespace v2_Test_Audio = v2::Test::Audio;
using namespace v2_Test_Audio;

class JNITestAudioControllerEventListener final : public ITestAudioControllerEventListener {
public:
    JNITestAudioControllerEventListener(JNIEnv *env, jobject callback, JNITestAudioControllerDepsCachePtr depsCache);
    ~JNITestAudioControllerEventListener();

	void onFocusStatusChangedReceived(const std::vector<v2_Test_Audio::Controller::FocusStatus>& focusStatusVec) override;

private:
    JavaVM*    mJavaVM{ nullptr };
    JNIEnv*    mEnv{ nullptr };
    jobject    mCallbackObj;
    JNITestAudioControllerDepsCachePtr    mDepsCache{ nullptr };

	jmethodID	mOnFocusStatusChangedReceivedId;
};

using JNITestAudioControllerEventListenerPtr = std::shared_ptr<JNITestAudioControllerEventListener>;
