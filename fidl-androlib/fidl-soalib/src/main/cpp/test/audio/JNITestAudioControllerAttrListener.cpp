/*
 * Copyright (c) 2023 42dot All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <logging.h>
#include <jni.h>

#include "JNITestAudioControllerAttrListener.h"

JNITestAudioControllerAttrListener::JNITestAudioControllerAttrListener(JNIEnv *env, jobject callback, JNITestAudioControllerDepsCachePtr depsCache)
    : mEnv(env)
	, mDepsCache(depsCache) {
    LOGI("JNITestAudioControllerAttrListener::JNITestAudioControllerAttrListener()");

    if (mDepsCache == nullptr){
        LOGE("JNITestAudioControllerAttrListener:: dependent cache info pointer is null!!");
    }

    LOGI("JNITestAudioControllerAttrListener::JNITestAudioControllerAttrListener() - NewGlobalRef mEnv:%p", mEnv);
	mCallbackObj = mEnv->NewGlobalRef(callback);
	LOGI("JNITestAudioControllerAttrListener::JNITestAudioControllerAttrListener() - mCallbackObj:%p", mCallbackObj);

	jint attachResult = mEnv->GetJavaVM(&mJavaVM);
    if (attachResult != JNI_OK) {
        return;
    }

    jclass callbackClass = mEnv->GetObjectClass(mCallbackObj);
	mOnIVIDeviceInfoChangedId = JNIHelper::getMethodId(mEnv, callbackClass, "onIVIDeviceInfoChanged", "(Lai/umos/soa/test/audio/data/KeyValueString;)V");
	mOnAudioSourceTypesChangedId = JNIHelper::getMethodId(mEnv, callbackClass, "onAudioSourceTypesChanged", "([Ljava/lang/String;)V");
	mOnVolumeInfoChangedId = JNIHelper::getMethodId(mEnv, callbackClass, "onVolumeInfoChanged", "(Lai/umos/soa/test/audio/data/VolumeInfo;)V");
	mOnKeyValueByteBufferMapChangedId = JNIHelper::getMethodId(mEnv, callbackClass, "onKeyValueByteBufferMapChanged", "(Lai/umos/soa/test/audio/data/KeyValueByteBuffer;)V");
	mOnDeviceStateMapChangedId = JNIHelper::getMethodId(mEnv, callbackClass, "onDeviceStateMapChanged", "(Lai/umos/soa/test/audio/data/DeviceStateMap;)V");
	mOnStringToShortMapChangedId = JNIHelper::getMethodId(mEnv, callbackClass, "onStringToShortMapChanged", "(Lai/umos/soa/test/audio/data/StringToShortMap;)V");
}

JNITestAudioControllerAttrListener::~JNITestAudioControllerAttrListener() {
    LOGI("JNITestAudioControllerAttrListener::~JNITestAudioControllerAttrListener()");

	LOGI("JNITestAudioControllerAttrListener::~JNITestAudioControllerAttrListener() - DeleteGlobalRef mEnv:%p", mEnv);
	LOGI("JNITestAudioControllerAttrListener::~JNITestAudioControllerAttrListener() - mCallbackObj:%p", mCallbackObj);
	if (mCallbackObj != nullptr) {
		mEnv->DeleteGlobalRef(mCallbackObj);
		mCallbackObj = nullptr;
	}
}

void JNITestAudioControllerAttrListener::onIVIDeviceInfoChanged(const v2_Test_Audio::Controller::KeyValueString& iVIDeviceInfo) {
    JNIEnv* attachEnv;
    if (mJavaVM->AttachCurrentThread(&attachEnv, nullptr) != JNI_OK) {
        return ;
    }

    jobject jKeyValueStringIVIDeviceInfoHashMapObj = attachEnv->NewObject(mDepsCache->mHashMapClass, mDepsCache->mHashMapCtorId);
	for (const auto& mapEntry : iVIDeviceInfo){
		jstring jKeyStringStr = attachEnv->NewStringUTF(mapEntry.first.c_str());
		jstring jValueStringStr = attachEnv->NewStringUTF(mapEntry.second.c_str());
		attachEnv->CallObjectMethod(jKeyValueStringIVIDeviceInfoHashMapObj, mDepsCache->mHashMapPutId, jKeyStringStr, jValueStringStr);
		attachEnv->DeleteLocalRef(jKeyStringStr);
		attachEnv->DeleteLocalRef(jValueStringStr);
	}
	jobject jKeyValueStringIVIDeviceInfoObj = attachEnv->NewObject(mDepsCache->mKeyValueStringClass, mDepsCache->mKeyValueStringCtorId, jKeyValueStringIVIDeviceInfoHashMapObj);
	attachEnv->CallVoidMethod(mCallbackObj, mOnIVIDeviceInfoChangedId, jKeyValueStringIVIDeviceInfoObj);
	attachEnv->DeleteLocalRef(jKeyValueStringIVIDeviceInfoObj);

    mJavaVM->DetachCurrentThread();
}

void JNITestAudioControllerAttrListener::onAudioSourceTypesChanged(const std::vector<std::string>& audioSourceTypesVec) {
    JNIEnv* attachEnv;
    if (mJavaVM->AttachCurrentThread(&attachEnv, nullptr) != JNI_OK) {
        return ;
    }

    jsize jaudioSourceTypesVecSize = static_cast<jsize>(audioSourceTypesVec.size());
	jobjectArray jAudioSourceTypesStringArrayObj = attachEnv->NewObjectArray(jaudioSourceTypesVecSize, mDepsCache->mJavaStringClass, nullptr);
	for (jsize idx = 0; idx < jaudioSourceTypesVecSize; ++idx) {
		auto audioSourceTypes = audioSourceTypesVec[idx];
		jstring jAudioSourceTypesStr = attachEnv->NewStringUTF(audioSourceTypes.c_str());
		attachEnv->SetObjectArrayElement(jAudioSourceTypesStringArrayObj, idx, jAudioSourceTypesStr);
		attachEnv->DeleteLocalRef(jAudioSourceTypesStr);
	}
	attachEnv->CallVoidMethod(mCallbackObj, mOnAudioSourceTypesChangedId, jAudioSourceTypesStringArrayObj);
	attachEnv->DeleteLocalRef(jAudioSourceTypesStringArrayObj);

    mJavaVM->DetachCurrentThread();
}

void JNITestAudioControllerAttrListener::onVolumeInfoChanged(const v2_Test_Audio::Controller::VolumeInfo& volumeInfo) {
    JNIEnv* attachEnv;
    if (mJavaVM->AttachCurrentThread(&attachEnv, nullptr) != JNI_OK) {
        return ;
    }

    jobject jVolumeInfoVolumeInfoObj = attachEnv->NewObject(mDepsCache->mVolumeInfoClass, mDepsCache->mVolumeInfoCtorId,
		(uint8_t)volumeInfo.getVolume(),
		(uint8_t)volumeInfo.getMaxSoftVolume(),
		(uint8_t)volumeInfo.getLastVolume(),
		volumeInfo.getMute() ? JNI_TRUE : JNI_FALSE);
	attachEnv->CallVoidMethod(mCallbackObj, mOnVolumeInfoChangedId, jVolumeInfoVolumeInfoObj);
	attachEnv->DeleteLocalRef(jVolumeInfoVolumeInfoObj);

    mJavaVM->DetachCurrentThread();
}

void JNITestAudioControllerAttrListener::onKeyValueByteBufferMapChanged(const v2_Test_Audio::Controller::KeyValueByteBuffer& keyValueByteBufferMap) {
    JNIEnv* attachEnv;
    if (mJavaVM->AttachCurrentThread(&attachEnv, nullptr) != JNI_OK) {
        return ;
    }

    jobject jKeyValueByteBufferKeyValueByteBufferMapHashMapObj = attachEnv->NewObject(mDepsCache->mHashMapClass, mDepsCache->mHashMapCtorId);
	for (const auto& mapEntry : keyValueByteBufferMap){
		jobject jIntValueObj = attachEnv->NewObject(mDepsCache->mIntClass, mDepsCache->mIntInitId, (uint32_t)mapEntry.first);
		jsize jmapEntrySecondSize = static_cast<jsize>(mapEntry.second.size());
		jbyteArray jValueByteBufferUInt8ArrayObj = attachEnv->NewByteArray(jmapEntrySecondSize);
		for (jsize idx = 0; idx < jmapEntrySecondSize; ++idx) {
			jbyte valueByteBuffer = mapEntry.second[idx];
			attachEnv->SetByteArrayRegion(jValueByteBufferUInt8ArrayObj, idx, 1, &valueByteBuffer);
		}
		attachEnv->CallObjectMethod(jKeyValueByteBufferKeyValueByteBufferMapHashMapObj, mDepsCache->mHashMapPutId, jIntValueObj, jValueByteBufferUInt8ArrayObj);
		attachEnv->DeleteLocalRef(jIntValueObj);
		attachEnv->DeleteLocalRef(jValueByteBufferUInt8ArrayObj);
	}
	jobject jKeyValueByteBufferKeyValueByteBufferMapObj = attachEnv->NewObject(mDepsCache->mKeyValueByteBufferClass, mDepsCache->mKeyValueByteBufferCtorId, jKeyValueByteBufferKeyValueByteBufferMapHashMapObj);
	attachEnv->CallVoidMethod(mCallbackObj, mOnKeyValueByteBufferMapChangedId, jKeyValueByteBufferKeyValueByteBufferMapObj);
	attachEnv->DeleteLocalRef(jKeyValueByteBufferKeyValueByteBufferMapObj);

    mJavaVM->DetachCurrentThread();
}

void JNITestAudioControllerAttrListener::onDeviceStateMapChanged(const v2_Test_Audio::Controller::DeviceStateMap& deviceStateMap) {
    JNIEnv* attachEnv;
    if (mJavaVM->AttachCurrentThread(&attachEnv, nullptr) != JNI_OK) {
        return ;
    }

    jobject jDeviceStateMapDeviceStateMapHashMapObj = attachEnv->NewObject(mDepsCache->mHashMapClass, mDepsCache->mHashMapCtorId);
	for (const auto& mapEntry : deviceStateMap){
		jobjectArray jAudioDeviceTypeKeyAudioDeviceTypeEnumArray = (jobjectArray)attachEnv->CallStaticObjectMethod(mDepsCache->mAudioDeviceTypeClass, mDepsCache->mAudioDeviceTypeValuesId);
		jobjectArray jAudioFocusStatusTypeValueAudioFocusStatusTypeEnumArray = (jobjectArray)attachEnv->CallStaticObjectMethod(mDepsCache->mAudioFocusStatusTypeClass, mDepsCache->mAudioFocusStatusTypeValuesId);
		jobject jAudioDeviceTypeKeyAudioDeviceTypeObj = attachEnv->GetObjectArrayElement(jAudioDeviceTypeKeyAudioDeviceTypeEnumArray, static_cast<int>((uint8_t)(mapEntry.first.value_)));
		jobject jAudioFocusStatusTypeValueAudioFocusStatusTypeObj = attachEnv->GetObjectArrayElement(jAudioFocusStatusTypeValueAudioFocusStatusTypeEnumArray, static_cast<int>((uint8_t)(mapEntry.second.value_)));
		attachEnv->CallObjectMethod(jDeviceStateMapDeviceStateMapHashMapObj, mDepsCache->mHashMapPutId, jAudioDeviceTypeKeyAudioDeviceTypeObj, jAudioFocusStatusTypeValueAudioFocusStatusTypeObj);
		attachEnv->DeleteLocalRef(jAudioDeviceTypeKeyAudioDeviceTypeObj);
		attachEnv->DeleteLocalRef(jAudioFocusStatusTypeValueAudioFocusStatusTypeObj);
	}
	jobject jDeviceStateMapDeviceStateMapObj = attachEnv->NewObject(mDepsCache->mDeviceStateMapClass, mDepsCache->mDeviceStateMapCtorId, jDeviceStateMapDeviceStateMapHashMapObj);
	attachEnv->CallVoidMethod(mCallbackObj, mOnDeviceStateMapChangedId, jDeviceStateMapDeviceStateMapObj);
	attachEnv->DeleteLocalRef(jDeviceStateMapDeviceStateMapObj);

    mJavaVM->DetachCurrentThread();
}

void JNITestAudioControllerAttrListener::onStringToShortMapChanged(const v2_Test_Audio::Controller::StringToShortMap& stringToShortMap) {
    JNIEnv* attachEnv;
    if (mJavaVM->AttachCurrentThread(&attachEnv, nullptr) != JNI_OK) {
        return ;
    }

    jobject jStringToShortMapStringToShortMapHashMapObj = attachEnv->NewObject(mDepsCache->mHashMapClass, mDepsCache->mHashMapCtorId);
	for (const auto& mapEntry : stringToShortMap){
		jobject jShortValueObj = attachEnv->NewObject(mDepsCache->mShortClass, mDepsCache->mShortInitId, (uint16_t)mapEntry.second);
		jstring jKeyStringStr = attachEnv->NewStringUTF(mapEntry.first.c_str());
		attachEnv->CallObjectMethod(jStringToShortMapStringToShortMapHashMapObj, mDepsCache->mHashMapPutId, jKeyStringStr, jShortValueObj);
		attachEnv->DeleteLocalRef(jKeyStringStr);
		attachEnv->DeleteLocalRef(jShortValueObj);
	}
	jobject jStringToShortMapStringToShortMapObj = attachEnv->NewObject(mDepsCache->mStringToShortMapClass, mDepsCache->mStringToShortMapCtorId, jStringToShortMapStringToShortMapHashMapObj);
	attachEnv->CallVoidMethod(mCallbackObj, mOnStringToShortMapChangedId, jStringToShortMapStringToShortMapObj);
	attachEnv->DeleteLocalRef(jStringToShortMapStringToShortMapObj);

    mJavaVM->DetachCurrentThread();
}
