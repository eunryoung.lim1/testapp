/*
* Copyright (c) 2023 42dot All rights reserved.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#include <logging.h>
#include <jni.h>

#include "JNITestAudioControllerAsyncCallback.h"

JNITestAudioControllerAsyncCallback::JNITestAudioControllerAsyncCallback(JNIEnv *env, JNITestAudioControllerDepsCachePtr depsCache)
    : mEnv(env)
    , mDepsCache(depsCache) {
    LOGI("JNITestAudioControllerAsyncCallback::JNITestAudioControllerAsyncCallback()");

    if (mDepsCache == nullptr){
        LOGE("JNITestAudioControllerAsyncCallback:: dependent cache info pointer is null!!");
    }

    jint attachResult = mEnv->GetJavaVM(&mJavaVM);
    if (attachResult != JNI_OK) {
        return;
    }
}

JNITestAudioControllerAsyncCallback::~JNITestAudioControllerAsyncCallback() {
    LOGI("JNITestAudioControllerAsyncCallback::~JNITestAudioControllerAsyncCallback()");
}

bool JNITestAudioControllerAsyncCallback::registerCallback(JNIEnv *env, jobject jCallback, const char *methodName, const char *methodSignature, uint32_t& callbackId) {
	jclass callbackClass = env->GetObjectClass(jCallback);
	jmethodID callbackMethodId = JNIHelper::getMethodId(env, callbackClass, methodName, methodSignature);
    if (callbackMethodId) {
        uint32_t id = ++mCallbackId;
        {
            std::lock_guard<std::mutex> lock(mCallbackMapMutex);
            mCallbackMap.insert(std::make_pair(id, JNICallbackInfo{env->NewGlobalRef(jCallback), callbackMethodId}));
        }
        callbackId = id;
        return true;
    }

    return false;
}

bool JNITestAudioControllerAsyncCallback::unregisterCallback(uint32_t callbackId, JNICallbackInfo& callbackInfo) {
    {
        std::lock_guard<std::mutex> lock(mCallbackMapMutex);
        auto it = mCallbackMap.find(callbackId);
        if (it != mCallbackMap.end()) {
            callbackInfo = it->second;
            mCallbackMap.erase(it);
            return true;
        }
    }
    return false;
}

void JNITestAudioControllerAsyncCallback::onIVIDeviceInfoAttr(const uint32_t callbackId, const CommonAPI::CallStatus& callStatus, const v2_Test_Audio::Controller::KeyValueString& replyIVIDeviceInfo) {
    JNIEnv* attachEnv;
	if (mJavaVM->AttachCurrentThread(&attachEnv, nullptr) != JNI_OK) {
	    LOGI("AttachCurrentThread to javaVM is not OK");
	    return;
	}
	jobjectArray jAsyncCallStatusEnumArray = (jobjectArray)attachEnv->CallStaticObjectMethod(mDepsCache->mAsyncCallStatusClass, mDepsCache->mAsyncCallStatusValuesId);
	jobject jAsyncCallStatusObj = attachEnv->GetObjectArrayElement(jAsyncCallStatusEnumArray, static_cast<int>((uint8_t)(callStatus)));
	jobject jKeyValueStringIVIDeviceInfoHashMapObj = attachEnv->NewObject(mDepsCache->mHashMapClass, mDepsCache->mHashMapCtorId);
	for (const auto& mapEntry : replyIVIDeviceInfo){
		jstring jKeyStringStr = attachEnv->NewStringUTF(mapEntry.first.c_str());
		jstring jValueStringStr = attachEnv->NewStringUTF(mapEntry.second.c_str());
		attachEnv->CallObjectMethod(jKeyValueStringIVIDeviceInfoHashMapObj, mDepsCache->mHashMapPutId, jKeyStringStr, jValueStringStr);
		attachEnv->DeleteLocalRef(jKeyStringStr);
		attachEnv->DeleteLocalRef(jValueStringStr);
	}
	jobject jKeyValueStringIVIDeviceInfoObj = attachEnv->NewObject(mDepsCache->mKeyValueStringClass, mDepsCache->mKeyValueStringCtorId, jKeyValueStringIVIDeviceInfoHashMapObj);
	
	JNICallbackInfo callbackInfo;
	if (unregisterCallback(callbackId, callbackInfo)) {
	    attachEnv->CallVoidMethod(callbackInfo.mCallbackObj, callbackInfo.mCallbackId, jAsyncCallStatusObj, jKeyValueStringIVIDeviceInfoObj);
	    attachEnv->DeleteGlobalRef(callbackInfo.mCallbackObj);
	}
	mJavaVM->DetachCurrentThread();
}

void JNITestAudioControllerAsyncCallback::onAudioSourceTypesAttr(const uint32_t callbackId, const CommonAPI::CallStatus& callStatus, const std::vector<std::string>& replyAudioSourceTypesVec) {
    JNIEnv* attachEnv;
	if (mJavaVM->AttachCurrentThread(&attachEnv, nullptr) != JNI_OK) {
	    LOGI("AttachCurrentThread to javaVM is not OK");
	    return;
	}
	jobjectArray jAsyncCallStatusEnumArray = (jobjectArray)attachEnv->CallStaticObjectMethod(mDepsCache->mAsyncCallStatusClass, mDepsCache->mAsyncCallStatusValuesId);
	jobject jAsyncCallStatusObj = attachEnv->GetObjectArrayElement(jAsyncCallStatusEnumArray, static_cast<int>((uint8_t)(callStatus)));
	jsize jreplyAudioSourceTypesVecSize = static_cast<jsize>(replyAudioSourceTypesVec.size());
	jobjectArray jAudioSourceTypesStringArrayObj = attachEnv->NewObjectArray(jreplyAudioSourceTypesVecSize, mDepsCache->mJavaStringClass, nullptr);
	for (jsize idx = 0; idx < jreplyAudioSourceTypesVecSize; ++idx) {
		auto audioSourceTypes = replyAudioSourceTypesVec[idx];
		jstring jAudioSourceTypesStr = attachEnv->NewStringUTF(audioSourceTypes.c_str());
		attachEnv->SetObjectArrayElement(jAudioSourceTypesStringArrayObj, idx, jAudioSourceTypesStr);
		attachEnv->DeleteLocalRef(jAudioSourceTypesStr);
	}
	
	JNICallbackInfo callbackInfo;
	if (unregisterCallback(callbackId, callbackInfo)) {
	    attachEnv->CallVoidMethod(callbackInfo.mCallbackObj, callbackInfo.mCallbackId, jAsyncCallStatusObj, jAudioSourceTypesStringArrayObj);
	    attachEnv->DeleteGlobalRef(callbackInfo.mCallbackObj);
	}
	mJavaVM->DetachCurrentThread();
}

void JNITestAudioControllerAsyncCallback::onVolumeInfoAttr(const uint32_t callbackId, const CommonAPI::CallStatus& callStatus, const v2_Test_Audio::Controller::VolumeInfo& replyVolumeInfo) {
    JNIEnv* attachEnv;
	if (mJavaVM->AttachCurrentThread(&attachEnv, nullptr) != JNI_OK) {
	    LOGI("AttachCurrentThread to javaVM is not OK");
	    return;
	}
	jobjectArray jAsyncCallStatusEnumArray = (jobjectArray)attachEnv->CallStaticObjectMethod(mDepsCache->mAsyncCallStatusClass, mDepsCache->mAsyncCallStatusValuesId);
	jobject jAsyncCallStatusObj = attachEnv->GetObjectArrayElement(jAsyncCallStatusEnumArray, static_cast<int>((uint8_t)(callStatus)));
	jobject jVolumeInfoVolumeInfoObj = attachEnv->NewObject(mDepsCache->mVolumeInfoClass, mDepsCache->mVolumeInfoCtorId,
		(uint8_t)replyVolumeInfo.getVolume(),
		(uint8_t)replyVolumeInfo.getMaxSoftVolume(),
		(uint8_t)replyVolumeInfo.getLastVolume(),
		replyVolumeInfo.getMute() ? JNI_TRUE : JNI_FALSE);
	
	JNICallbackInfo callbackInfo;
	if (unregisterCallback(callbackId, callbackInfo)) {
	    attachEnv->CallVoidMethod(callbackInfo.mCallbackObj, callbackInfo.mCallbackId, jAsyncCallStatusObj, jVolumeInfoVolumeInfoObj);
	    attachEnv->DeleteGlobalRef(callbackInfo.mCallbackObj);
	}
	mJavaVM->DetachCurrentThread();
}

void JNITestAudioControllerAsyncCallback::onKeyValueByteBufferMapAttr(const uint32_t callbackId, const CommonAPI::CallStatus& callStatus, const v2_Test_Audio::Controller::KeyValueByteBuffer& replyKeyValueByteBufferMap) {
    JNIEnv* attachEnv;
	if (mJavaVM->AttachCurrentThread(&attachEnv, nullptr) != JNI_OK) {
	    LOGI("AttachCurrentThread to javaVM is not OK");
	    return;
	}
	jobjectArray jAsyncCallStatusEnumArray = (jobjectArray)attachEnv->CallStaticObjectMethod(mDepsCache->mAsyncCallStatusClass, mDepsCache->mAsyncCallStatusValuesId);
	jobject jAsyncCallStatusObj = attachEnv->GetObjectArrayElement(jAsyncCallStatusEnumArray, static_cast<int>((uint8_t)(callStatus)));
	jobject jKeyValueByteBufferKeyValueByteBufferMapHashMapObj = attachEnv->NewObject(mDepsCache->mHashMapClass, mDepsCache->mHashMapCtorId);
	for (const auto& mapEntry : replyKeyValueByteBufferMap){
		jobject jIntValueObj = attachEnv->NewObject(mDepsCache->mIntClass, mDepsCache->mIntInitId, (uint32_t)mapEntry.first);
		jsize jmapEntrySecondSize = static_cast<jsize>(mapEntry.second.size());
		jbyteArray jValueByteBufferUInt8ArrayObj = attachEnv->NewByteArray(jmapEntrySecondSize);
		for (jsize idx = 0; idx < jmapEntrySecondSize; ++idx) {
			jbyte valueByteBuffer = mapEntry.second[idx];
			attachEnv->SetByteArrayRegion(jValueByteBufferUInt8ArrayObj, idx, 1, &valueByteBuffer);
		}
		attachEnv->CallObjectMethod(jKeyValueByteBufferKeyValueByteBufferMapHashMapObj, mDepsCache->mHashMapPutId, jIntValueObj, jValueByteBufferUInt8ArrayObj);
		attachEnv->DeleteLocalRef(jIntValueObj);
		attachEnv->DeleteLocalRef(jValueByteBufferUInt8ArrayObj);
	}
	jobject jKeyValueByteBufferKeyValueByteBufferMapObj = attachEnv->NewObject(mDepsCache->mKeyValueByteBufferClass, mDepsCache->mKeyValueByteBufferCtorId, jKeyValueByteBufferKeyValueByteBufferMapHashMapObj);
	
	JNICallbackInfo callbackInfo;
	if (unregisterCallback(callbackId, callbackInfo)) {
	    attachEnv->CallVoidMethod(callbackInfo.mCallbackObj, callbackInfo.mCallbackId, jAsyncCallStatusObj, jKeyValueByteBufferKeyValueByteBufferMapObj);
	    attachEnv->DeleteGlobalRef(callbackInfo.mCallbackObj);
	}
	mJavaVM->DetachCurrentThread();
}

void JNITestAudioControllerAsyncCallback::onDeviceStateMapAttr(const uint32_t callbackId, const CommonAPI::CallStatus& callStatus, const v2_Test_Audio::Controller::DeviceStateMap& replyDeviceStateMap) {
    JNIEnv* attachEnv;
	if (mJavaVM->AttachCurrentThread(&attachEnv, nullptr) != JNI_OK) {
	    LOGI("AttachCurrentThread to javaVM is not OK");
	    return;
	}
	jobjectArray jAsyncCallStatusEnumArray = (jobjectArray)attachEnv->CallStaticObjectMethod(mDepsCache->mAsyncCallStatusClass, mDepsCache->mAsyncCallStatusValuesId);
	jobject jAsyncCallStatusObj = attachEnv->GetObjectArrayElement(jAsyncCallStatusEnumArray, static_cast<int>((uint8_t)(callStatus)));
	jobject jDeviceStateMapDeviceStateMapHashMapObj = attachEnv->NewObject(mDepsCache->mHashMapClass, mDepsCache->mHashMapCtorId);
	for (const auto& mapEntry : replyDeviceStateMap){
		jobjectArray jAudioDeviceTypeKeyAudioDeviceTypeEnumArray = (jobjectArray)attachEnv->CallStaticObjectMethod(mDepsCache->mAudioDeviceTypeClass, mDepsCache->mAudioDeviceTypeValuesId);
		jobjectArray jAudioFocusStatusTypeValueAudioFocusStatusTypeEnumArray = (jobjectArray)attachEnv->CallStaticObjectMethod(mDepsCache->mAudioFocusStatusTypeClass, mDepsCache->mAudioFocusStatusTypeValuesId);
		jobject jAudioDeviceTypeKeyAudioDeviceTypeObj = attachEnv->GetObjectArrayElement(jAudioDeviceTypeKeyAudioDeviceTypeEnumArray, static_cast<int>((uint8_t)(mapEntry.first.value_)));
		jobject jAudioFocusStatusTypeValueAudioFocusStatusTypeObj = attachEnv->GetObjectArrayElement(jAudioFocusStatusTypeValueAudioFocusStatusTypeEnumArray, static_cast<int>((uint8_t)(mapEntry.second.value_)));
		attachEnv->CallObjectMethod(jDeviceStateMapDeviceStateMapHashMapObj, mDepsCache->mHashMapPutId, jAudioDeviceTypeKeyAudioDeviceTypeObj, jAudioFocusStatusTypeValueAudioFocusStatusTypeObj);
		attachEnv->DeleteLocalRef(jAudioDeviceTypeKeyAudioDeviceTypeObj);
		attachEnv->DeleteLocalRef(jAudioFocusStatusTypeValueAudioFocusStatusTypeObj);
	}
	jobject jDeviceStateMapDeviceStateMapObj = attachEnv->NewObject(mDepsCache->mDeviceStateMapClass, mDepsCache->mDeviceStateMapCtorId, jDeviceStateMapDeviceStateMapHashMapObj);
	
	JNICallbackInfo callbackInfo;
	if (unregisterCallback(callbackId, callbackInfo)) {
	    attachEnv->CallVoidMethod(callbackInfo.mCallbackObj, callbackInfo.mCallbackId, jAsyncCallStatusObj, jDeviceStateMapDeviceStateMapObj);
	    attachEnv->DeleteGlobalRef(callbackInfo.mCallbackObj);
	}
	mJavaVM->DetachCurrentThread();
}

void JNITestAudioControllerAsyncCallback::onStringToShortMapAttr(const uint32_t callbackId, const CommonAPI::CallStatus& callStatus, const v2_Test_Audio::Controller::StringToShortMap& replyStringToShortMap) {
    JNIEnv* attachEnv;
	if (mJavaVM->AttachCurrentThread(&attachEnv, nullptr) != JNI_OK) {
	    LOGI("AttachCurrentThread to javaVM is not OK");
	    return;
	}
	jobjectArray jAsyncCallStatusEnumArray = (jobjectArray)attachEnv->CallStaticObjectMethod(mDepsCache->mAsyncCallStatusClass, mDepsCache->mAsyncCallStatusValuesId);
	jobject jAsyncCallStatusObj = attachEnv->GetObjectArrayElement(jAsyncCallStatusEnumArray, static_cast<int>((uint8_t)(callStatus)));
	jobject jStringToShortMapStringToShortMapHashMapObj = attachEnv->NewObject(mDepsCache->mHashMapClass, mDepsCache->mHashMapCtorId);
	for (const auto& mapEntry : replyStringToShortMap){
		jobject jShortValueObj = attachEnv->NewObject(mDepsCache->mShortClass, mDepsCache->mShortInitId, (uint16_t)mapEntry.second);
		jstring jKeyStringStr = attachEnv->NewStringUTF(mapEntry.first.c_str());
		attachEnv->CallObjectMethod(jStringToShortMapStringToShortMapHashMapObj, mDepsCache->mHashMapPutId, jKeyStringStr, jShortValueObj);
		attachEnv->DeleteLocalRef(jKeyStringStr);
		attachEnv->DeleteLocalRef(jShortValueObj);
	}
	jobject jStringToShortMapStringToShortMapObj = attachEnv->NewObject(mDepsCache->mStringToShortMapClass, mDepsCache->mStringToShortMapCtorId, jStringToShortMapStringToShortMapHashMapObj);
	
	JNICallbackInfo callbackInfo;
	if (unregisterCallback(callbackId, callbackInfo)) {
	    attachEnv->CallVoidMethod(callbackInfo.mCallbackObj, callbackInfo.mCallbackId, jAsyncCallStatusObj, jStringToShortMapStringToShortMapObj);
	    attachEnv->DeleteGlobalRef(callbackInfo.mCallbackObj);
	}
	mJavaVM->DetachCurrentThread();
}

void JNITestAudioControllerAsyncCallback::onObtainFocus(const uint32_t callbackId, const CommonAPI::CallStatus& callStatus, const v2_Test_Audio::Controller::FocusControlError& replyErrFocusControlError, const v2_Test_Audio::Controller::AudioFocusResultType& replyRes) {
    JNIEnv* attachEnv;
	if (mJavaVM->AttachCurrentThread(&attachEnv, nullptr) != JNI_OK) {
	    LOGI("AttachCurrentThread to javaVM is not OK");
	    return;
	}
	jobjectArray jAsyncCallStatusEnumArray = (jobjectArray)attachEnv->CallStaticObjectMethod(mDepsCache->mAsyncCallStatusClass, mDepsCache->mAsyncCallStatusValuesId);
	jobject jAsyncCallStatusObj = attachEnv->GetObjectArrayElement(jAsyncCallStatusEnumArray, static_cast<int>((uint8_t)(callStatus)));
	jobjectArray jFocusControlErrorErrFocusControlErrorEnumArray = (jobjectArray)attachEnv->CallStaticObjectMethod(mDepsCache->mFocusControlErrorClass, mDepsCache->mFocusControlErrorValuesId);
	jobjectArray jAudioFocusResultTypeReplyResEnumArray = (jobjectArray)attachEnv->CallStaticObjectMethod(mDepsCache->mAudioFocusResultTypeClass, mDepsCache->mAudioFocusResultTypeValuesId);
	jobject jFocusControlErrorErrFocusControlErrorObj = attachEnv->GetObjectArrayElement(jFocusControlErrorErrFocusControlErrorEnumArray, static_cast<int>((uint8_t)(replyErrFocusControlError.value_)));
	jobject jAudioFocusResultTypeReplyResObj = attachEnv->GetObjectArrayElement(jAudioFocusResultTypeReplyResEnumArray, static_cast<int>((uint8_t)(replyRes.value_)));
	
	JNICallbackInfo callbackInfo;
	if (unregisterCallback(callbackId, callbackInfo)) {
	    attachEnv->CallVoidMethod(callbackInfo.mCallbackObj, callbackInfo.mCallbackId, 
			jAsyncCallStatusObj, 
			jFocusControlErrorErrFocusControlErrorObj, 
			jAudioFocusResultTypeReplyResObj);
	    attachEnv->DeleteGlobalRef(callbackInfo.mCallbackObj);
	}
	mJavaVM->DetachCurrentThread();
}

void JNITestAudioControllerAsyncCallback::onReleaseFocus(const uint32_t callbackId, const CommonAPI::CallStatus& callStatus, const v2_Test_Audio::Controller::FocusControlError& replyErrFocusControlError) {
    JNIEnv* attachEnv;
	if (mJavaVM->AttachCurrentThread(&attachEnv, nullptr) != JNI_OK) {
	    LOGI("AttachCurrentThread to javaVM is not OK");
	    return;
	}
	jobjectArray jAsyncCallStatusEnumArray = (jobjectArray)attachEnv->CallStaticObjectMethod(mDepsCache->mAsyncCallStatusClass, mDepsCache->mAsyncCallStatusValuesId);
	jobject jAsyncCallStatusObj = attachEnv->GetObjectArrayElement(jAsyncCallStatusEnumArray, static_cast<int>((uint8_t)(callStatus)));
	jobjectArray jFocusControlErrorErrFocusControlErrorEnumArray = (jobjectArray)attachEnv->CallStaticObjectMethod(mDepsCache->mFocusControlErrorClass, mDepsCache->mFocusControlErrorValuesId);
	jobject jFocusControlErrorErrFocusControlErrorObj = attachEnv->GetObjectArrayElement(jFocusControlErrorErrFocusControlErrorEnumArray, static_cast<int>((uint8_t)(replyErrFocusControlError.value_)));
	
	JNICallbackInfo callbackInfo;
	if (unregisterCallback(callbackId, callbackInfo)) {
	    attachEnv->CallVoidMethod(callbackInfo.mCallbackObj, callbackInfo.mCallbackId, 
			jAsyncCallStatusObj, 
			jFocusControlErrorErrFocusControlErrorObj);
	    attachEnv->DeleteGlobalRef(callbackInfo.mCallbackObj);
	}
	mJavaVM->DetachCurrentThread();
}

void JNITestAudioControllerAsyncCallback::onSetVolume(const uint32_t callbackId, const CommonAPI::CallStatus& callStatus, const v2_Test_Audio::Controller::VolumeControlError& replyErrVolumeControlError) {
    JNIEnv* attachEnv;
	if (mJavaVM->AttachCurrentThread(&attachEnv, nullptr) != JNI_OK) {
	    LOGI("AttachCurrentThread to javaVM is not OK");
	    return;
	}
	jobjectArray jAsyncCallStatusEnumArray = (jobjectArray)attachEnv->CallStaticObjectMethod(mDepsCache->mAsyncCallStatusClass, mDepsCache->mAsyncCallStatusValuesId);
	jobject jAsyncCallStatusObj = attachEnv->GetObjectArrayElement(jAsyncCallStatusEnumArray, static_cast<int>((uint8_t)(callStatus)));
	jobjectArray jVolumeControlErrorErrVolumeControlErrorEnumArray = (jobjectArray)attachEnv->CallStaticObjectMethod(mDepsCache->mVolumeControlErrorClass, mDepsCache->mVolumeControlErrorValuesId);
	jobject jVolumeControlErrorErrVolumeControlErrorObj = attachEnv->GetObjectArrayElement(jVolumeControlErrorErrVolumeControlErrorEnumArray, static_cast<int>((uint8_t)(replyErrVolumeControlError.value_)));
	
	JNICallbackInfo callbackInfo;
	if (unregisterCallback(callbackId, callbackInfo)) {
	    attachEnv->CallVoidMethod(callbackInfo.mCallbackObj, callbackInfo.mCallbackId, 
			jAsyncCallStatusObj, 
			jVolumeControlErrorErrVolumeControlErrorObj);
	    attachEnv->DeleteGlobalRef(callbackInfo.mCallbackObj);
	}
	mJavaVM->DetachCurrentThread();
}

void JNITestAudioControllerAsyncCallback::onSetSourceVolume(const uint32_t callbackId, const CommonAPI::CallStatus& callStatus, const v2_Test_Audio::Controller::VolumeControlError& replyErrVolumeControlError) {
    JNIEnv* attachEnv;
	if (mJavaVM->AttachCurrentThread(&attachEnv, nullptr) != JNI_OK) {
	    LOGI("AttachCurrentThread to javaVM is not OK");
	    return;
	}
	jobjectArray jAsyncCallStatusEnumArray = (jobjectArray)attachEnv->CallStaticObjectMethod(mDepsCache->mAsyncCallStatusClass, mDepsCache->mAsyncCallStatusValuesId);
	jobject jAsyncCallStatusObj = attachEnv->GetObjectArrayElement(jAsyncCallStatusEnumArray, static_cast<int>((uint8_t)(callStatus)));
	jobjectArray jVolumeControlErrorErrVolumeControlErrorEnumArray = (jobjectArray)attachEnv->CallStaticObjectMethod(mDepsCache->mVolumeControlErrorClass, mDepsCache->mVolumeControlErrorValuesId);
	jobject jVolumeControlErrorErrVolumeControlErrorObj = attachEnv->GetObjectArrayElement(jVolumeControlErrorErrVolumeControlErrorEnumArray, static_cast<int>((uint8_t)(replyErrVolumeControlError.value_)));
	
	JNICallbackInfo callbackInfo;
	if (unregisterCallback(callbackId, callbackInfo)) {
	    attachEnv->CallVoidMethod(callbackInfo.mCallbackObj, callbackInfo.mCallbackId, 
			jAsyncCallStatusObj, 
			jVolumeControlErrorErrVolumeControlErrorObj);
	    attachEnv->DeleteGlobalRef(callbackInfo.mCallbackObj);
	}
	mJavaVM->DetachCurrentThread();
}

void JNITestAudioControllerAsyncCallback::onGetAudioSourceStatus(const uint32_t callbackId, const CommonAPI::CallStatus& callStatus, const v2_Test_Audio::Controller::ErrorType& replyErrErrorType, const v2_Test_Audio::Controller::AudioSourceStatusInfo& replyAudioSourceStatusInfo) {
    JNIEnv* attachEnv;
	if (mJavaVM->AttachCurrentThread(&attachEnv, nullptr) != JNI_OK) {
	    LOGI("AttachCurrentThread to javaVM is not OK");
	    return;
	}
	jobjectArray jAsyncCallStatusEnumArray = (jobjectArray)attachEnv->CallStaticObjectMethod(mDepsCache->mAsyncCallStatusClass, mDepsCache->mAsyncCallStatusValuesId);
	jobject jAsyncCallStatusObj = attachEnv->GetObjectArrayElement(jAsyncCallStatusEnumArray, static_cast<int>((uint8_t)(callStatus)));
	jobjectArray jErrorTypeErrErrorTypeEnumArray = (jobjectArray)attachEnv->CallStaticObjectMethod(mDepsCache->mErrorTypeClass, mDepsCache->mErrorTypeValuesId);
	auto replyAudioSourceStatusInfoFocusStatusFocusStatus = replyAudioSourceStatusInfo.getFocusStatus();
	auto replyAudioSourceStatusInfoFocusStatusFocusStatusAudioDeviceTypeDevice = replyAudioSourceStatusInfoFocusStatusFocusStatus.getDevice();
	jobjectArray jAudioDeviceTypeDeviceEnumArray = (jobjectArray)attachEnv->CallStaticObjectMethod(mDepsCache->mAudioDeviceTypeClass, mDepsCache->mAudioDeviceTypeValuesId);
	auto replyAudioSourceStatusInfoFocusStatusFocusStatusAudioFocusStatusTypeStatus = replyAudioSourceStatusInfoFocusStatusFocusStatus.getStatus();
	jobjectArray jAudioFocusStatusTypeStatusEnumArray = (jobjectArray)attachEnv->CallStaticObjectMethod(mDepsCache->mAudioFocusStatusTypeClass, mDepsCache->mAudioFocusStatusTypeValuesId);
	auto replyAudioSourceStatusInfoVolumeInfoVolumeInfo = replyAudioSourceStatusInfo.getVolumeInfo();
	jobject jErrorTypeErrErrorTypeObj = attachEnv->GetObjectArrayElement(jErrorTypeErrErrorTypeEnumArray, static_cast<int>((uint8_t)(replyErrErrorType.value_)));
	jstring jReplyAudioSourceStatusInfoAudioSourceTypeStr = attachEnv->NewStringUTF(replyAudioSourceStatusInfo.getAudioSourceType().c_str());
	jstring jReplyAudioSourceStatusInfoFocusStatusFocusStatusAudioSourceTypeStr = attachEnv->NewStringUTF(replyAudioSourceStatusInfoFocusStatusFocusStatus.getAudioSourceType().c_str());
	jobject jAudioDeviceTypeDeviceObj = attachEnv->GetObjectArrayElement(jAudioDeviceTypeDeviceEnumArray, static_cast<int>((uint8_t)(replyAudioSourceStatusInfoFocusStatusFocusStatusAudioDeviceTypeDevice.value_)));
	jobject jAudioFocusStatusTypeStatusObj = attachEnv->GetObjectArrayElement(jAudioFocusStatusTypeStatusEnumArray, static_cast<int>((uint8_t)(replyAudioSourceStatusInfoFocusStatusFocusStatusAudioFocusStatusTypeStatus.value_)));
	jobject jFocusStatusFocusStatusObj = attachEnv->NewObject(mDepsCache->mFocusStatusClass, mDepsCache->mFocusStatusCtorId,
		jReplyAudioSourceStatusInfoFocusStatusFocusStatusAudioSourceTypeStr,
		jAudioDeviceTypeDeviceObj,
		jAudioFocusStatusTypeStatusObj);
	jobject jVolumeInfoVolumeInfoObj = attachEnv->NewObject(mDepsCache->mVolumeInfoClass, mDepsCache->mVolumeInfoCtorId,
		(uint8_t)replyAudioSourceStatusInfoVolumeInfoVolumeInfo.getVolume(),
		(uint8_t)replyAudioSourceStatusInfoVolumeInfoVolumeInfo.getMaxSoftVolume(),
		(uint8_t)replyAudioSourceStatusInfoVolumeInfoVolumeInfo.getLastVolume(),
		replyAudioSourceStatusInfoVolumeInfoVolumeInfo.getMute() ? JNI_TRUE : JNI_FALSE);
	jobject jAudioSourceStatusInfoReplyAudioSourceStatusInfoObj = attachEnv->NewObject(mDepsCache->mAudioSourceStatusInfoClass, mDepsCache->mAudioSourceStatusInfoCtorId,
		jReplyAudioSourceStatusInfoAudioSourceTypeStr,
		jFocusStatusFocusStatusObj,
		jVolumeInfoVolumeInfoObj);
	
	JNICallbackInfo callbackInfo;
	if (unregisterCallback(callbackId, callbackInfo)) {
	    attachEnv->CallVoidMethod(callbackInfo.mCallbackObj, callbackInfo.mCallbackId, 
			jAsyncCallStatusObj, 
			jErrorTypeErrErrorTypeObj, 
			jAudioSourceStatusInfoReplyAudioSourceStatusInfoObj);
	    attachEnv->DeleteGlobalRef(callbackInfo.mCallbackObj);
	}
	mJavaVM->DetachCurrentThread();
}
