/*
 * Copyright (c) 2023 42dot All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <logging.h>
#include <jni.h>
#include <thread>

#include "JNITestAudioControllerHandler.h"

JNITestAudioControllerHandler::JNITestAudioControllerHandler(JNIEnv *env, jobject callback, JNITestAudioControllerDepsCachePtr depsCache)
    : mEnv(env)
	, mDepsCache(depsCache) {
    LOGI("JNITestAudioControllerHandler::JNITestAudioControllerHandler()");

    if (mDepsCache == nullptr){
        LOGE("JNITestAudioControllerHandler:: is null!!");
    }

    mCallbackObj = mEnv->NewGlobalRef(callback);
    jint attachResult = mEnv->GetJavaVM(&mJavaVM);
    if (attachResult != JNI_OK) {
        return;
    }

    jclass callbackClass = mEnv->GetObjectClass(mCallbackObj);
	mOnObtainFocusId = JNIHelper::getMethodId(mEnv, callbackClass, "onObtainFocus", "(Ljava/lang/String;Lai/umos/soa/test/audio/data/AudioDeviceType;)Lai/umos/soa/test/audio/data/ReplyObtainFocus;");
	mOnReleaseFocusId = JNIHelper::getMethodId(mEnv, callbackClass, "onReleaseFocus", "(Ljava/lang/String;Lai/umos/soa/test/audio/data/AudioDeviceType;)Lai/umos/soa/test/audio/data/FocusControlError;");
	mOnSetVolumeId = JNIHelper::getMethodId(mEnv, callbackClass, "onSetVolume", "(B)Lai/umos/soa/test/audio/data/VolumeControlError;");
	mOnSetSourceVolumeId = JNIHelper::getMethodId(mEnv, callbackClass, "onSetSourceVolume", "(Ljava/lang/String;SI)Lai/umos/soa/test/audio/data/VolumeControlError;");
	mOnGetAudioSourceStatusId = JNIHelper::getMethodId(mEnv, callbackClass, "onGetAudioSourceStatus", "(Ljava/lang/String;)Lai/umos/soa/test/audio/data/ReplyGetAudioSourceStatus;");
}

JNITestAudioControllerHandler::~JNITestAudioControllerHandler() {
    LOGI("JNITestAudioControllerHandler::~JNITestAudioControllerHandler()");
    mEnv->DeleteGlobalRef(mCallbackObj);
}

void JNITestAudioControllerHandler::onObtainFocus(const std::string& audioSourceType, const v2_Test_Audio::Controller::AudioDeviceType& device, v2_Test_Audio::Controller::FocusControlError& errFocusControlError, v2_Test_Audio::Controller::AudioFocusResultType& replyRes) {
    JNIEnv* attachEnv;
    if (mJavaVM->AttachCurrentThread(&attachEnv, nullptr) != JNI_OK) {
        return ;
    }

    jobjectArray jAudioDeviceTypeDeviceEnumArray = (jobjectArray)attachEnv->CallStaticObjectMethod(mDepsCache->mAudioDeviceTypeClass, mDepsCache->mAudioDeviceTypeValuesId);
	jstring jAudioSourceTypeStr = attachEnv->NewStringUTF(audioSourceType.c_str());
	jobject jAudioDeviceTypeDeviceObj = attachEnv->GetObjectArrayElement(jAudioDeviceTypeDeviceEnumArray, static_cast<int>((uint8_t)(device.value_)));
	jobject jReplyObtainFocusReplyObtainFocus = attachEnv->CallObjectMethod(mCallbackObj, mOnObtainFocusId, jAudioSourceTypeStr, jAudioDeviceTypeDeviceObj);
	
	jobject jReplyObtainFocusReplyObtainFocusErrFocusControlErrorObj = attachEnv->GetObjectField(jReplyObtainFocusReplyObtainFocus, mDepsCache->mReplyObtainFocus.errFocusControlErrorId);
	jobject jReplyObtainFocusReplyObtainFocusReplyResObj = attachEnv->GetObjectField(jReplyObtainFocusReplyObtainFocus, mDepsCache->mReplyObtainFocus.replyResId);
	jint jErrFocusControlErrorValue = attachEnv->CallIntMethod(jReplyObtainFocusReplyObtainFocusErrFocusControlErrorObj, mDepsCache->mFocusControlErrorOrdinalMethodId);
	v2_Test_Audio::Controller::FocusControlError retErrFocusControlError;
	retErrFocusControlError.value_ = (int)jErrFocusControlErrorValue;
	jint jReplyResValue = attachEnv->CallIntMethod(jReplyObtainFocusReplyObtainFocusReplyResObj, mDepsCache->mAudioFocusResultTypeOrdinalMethodId);
	v2_Test_Audio::Controller::AudioFocusResultType retReplyRes;
	retReplyRes.value_ = (int)jReplyResValue;
	attachEnv->DeleteLocalRef(jAudioSourceTypeStr);
	attachEnv->DeleteLocalRef(jAudioDeviceTypeDeviceObj);
	
	errFocusControlError = retErrFocusControlError;
	replyRes = retReplyRes;
    mJavaVM->DetachCurrentThread();
}

void JNITestAudioControllerHandler::onReleaseFocus(const std::string& audioSourceType, const v2_Test_Audio::Controller::AudioDeviceType& device, v2_Test_Audio::Controller::FocusControlError& errFocusControlError) {
    JNIEnv* attachEnv;
    if (mJavaVM->AttachCurrentThread(&attachEnv, nullptr) != JNI_OK) {
        return ;
    }

    jobjectArray jAudioDeviceTypeDeviceEnumArray = (jobjectArray)attachEnv->CallStaticObjectMethod(mDepsCache->mAudioDeviceTypeClass, mDepsCache->mAudioDeviceTypeValuesId);
	jstring jAudioSourceTypeStr = attachEnv->NewStringUTF(audioSourceType.c_str());
	jobject jAudioDeviceTypeDeviceObj = attachEnv->GetObjectArrayElement(jAudioDeviceTypeDeviceEnumArray, static_cast<int>((uint8_t)(device.value_)));
	jobject jFocusControlErrorErrFocusControlError = attachEnv->CallObjectMethod(mCallbackObj, mOnReleaseFocusId, jAudioSourceTypeStr, jAudioDeviceTypeDeviceObj);
	
	jint jErrFocusControlErrorValue = attachEnv->CallIntMethod(jFocusControlErrorErrFocusControlError, mDepsCache->mFocusControlErrorOrdinalMethodId);
	v2_Test_Audio::Controller::FocusControlError retErrFocusControlError;
	retErrFocusControlError.value_ = (int)jErrFocusControlErrorValue;
	attachEnv->DeleteLocalRef(jAudioSourceTypeStr);
	attachEnv->DeleteLocalRef(jAudioDeviceTypeDeviceObj);
	
	errFocusControlError = retErrFocusControlError;
    mJavaVM->DetachCurrentThread();
}

void JNITestAudioControllerHandler::onSetVolume(uint8_t volume, v2_Test_Audio::Controller::VolumeControlError& errVolumeControlError) {
    JNIEnv* attachEnv;
    if (mJavaVM->AttachCurrentThread(&attachEnv, nullptr) != JNI_OK) {
        return ;
    }

    jobject jVolumeControlErrorErrVolumeControlError = attachEnv->CallObjectMethod(mCallbackObj, mOnSetVolumeId, (uint8_t)volume);
	
	jint jErrVolumeControlErrorValue = attachEnv->CallIntMethod(jVolumeControlErrorErrVolumeControlError, mDepsCache->mVolumeControlErrorOrdinalMethodId);
	v2_Test_Audio::Controller::VolumeControlError retErrVolumeControlError;
	retErrVolumeControlError.value_ = (int)jErrVolumeControlErrorValue;
	
	errVolumeControlError = retErrVolumeControlError;
    mJavaVM->DetachCurrentThread();
}

void JNITestAudioControllerHandler::onSetSourceVolume(const std::string& audioSourceType, uint16_t volume, uint32_t maxVolume, v2_Test_Audio::Controller::VolumeControlError& errVolumeControlError) {
    JNIEnv* attachEnv;
    if (mJavaVM->AttachCurrentThread(&attachEnv, nullptr) != JNI_OK) {
        return ;
    }

    jstring jAudioSourceTypeStr = attachEnv->NewStringUTF(audioSourceType.c_str());
	jobject jVolumeControlErrorErrVolumeControlError = attachEnv->CallObjectMethod(mCallbackObj, mOnSetSourceVolumeId, jAudioSourceTypeStr, (uint16_t)volume, (uint32_t)maxVolume);
	
	jint jErrVolumeControlErrorValue = attachEnv->CallIntMethod(jVolumeControlErrorErrVolumeControlError, mDepsCache->mVolumeControlErrorOrdinalMethodId);
	v2_Test_Audio::Controller::VolumeControlError retErrVolumeControlError;
	retErrVolumeControlError.value_ = (int)jErrVolumeControlErrorValue;
	attachEnv->DeleteLocalRef(jAudioSourceTypeStr);
	
	errVolumeControlError = retErrVolumeControlError;
    mJavaVM->DetachCurrentThread();
}

void JNITestAudioControllerHandler::onGetAudioSourceStatus(const std::string& audioSourceType, v2_Test_Audio::Controller::ErrorType& errErrorType, v2_Test_Audio::Controller::AudioSourceStatusInfo& replyAudioSourceStatusInfo) {
    JNIEnv* attachEnv;
    if (mJavaVM->AttachCurrentThread(&attachEnv, nullptr) != JNI_OK) {
        return ;
    }

    jstring jAudioSourceTypeStr = attachEnv->NewStringUTF(audioSourceType.c_str());
	jobject jReplyGetAudioSourceStatusReplyGetAudioSourceStatus = attachEnv->CallObjectMethod(mCallbackObj, mOnGetAudioSourceStatusId, jAudioSourceTypeStr);
	
	jobject jReplyGetAudioSourceStatusReplyGetAudioSourceStatusErrErrorTypeObj = attachEnv->GetObjectField(jReplyGetAudioSourceStatusReplyGetAudioSourceStatus, mDepsCache->mReplyGetAudioSourceStatus.errErrorTypeId);
	jobject jReplyGetAudioSourceStatusReplyGetAudioSourceStatusAudioSourceStatusInfoReplyAudioSourceStatusInfoObj = attachEnv->GetObjectField(jReplyGetAudioSourceStatusReplyGetAudioSourceStatus, mDepsCache->mReplyGetAudioSourceStatus.replyAudioSourceStatusInfoId);
	jstring jReplyGetAudioSourceStatusReplyGetAudioSourceStatusAudioSourceStatusInfoReplyAudioSourceStatusInfoObjAudioSourceType = static_cast<jstring>(attachEnv->GetObjectField(jReplyGetAudioSourceStatusReplyGetAudioSourceStatusAudioSourceStatusInfoReplyAudioSourceStatusInfoObj, mDepsCache->mAudioSourceStatusInfo.audioSourceTypeId));
	jobject jReplyGetAudioSourceStatusReplyGetAudioSourceStatusAudioSourceStatusInfoReplyAudioSourceStatusInfoObjFocusStatusFocusStatusObj = attachEnv->GetObjectField(jReplyGetAudioSourceStatusReplyGetAudioSourceStatusAudioSourceStatusInfoReplyAudioSourceStatusInfoObj, mDepsCache->mAudioSourceStatusInfo.focusStatusId);
	jstring jReplyGetAudioSourceStatusReplyGetAudioSourceStatusAudioSourceStatusInfoReplyAudioSourceStatusInfoObjFocusStatusFocusStatusObjAudioSourceType = static_cast<jstring>(attachEnv->GetObjectField(jReplyGetAudioSourceStatusReplyGetAudioSourceStatusAudioSourceStatusInfoReplyAudioSourceStatusInfoObjFocusStatusFocusStatusObj, mDepsCache->mFocusStatus.audioSourceTypeId));
	jobject jReplyGetAudioSourceStatusReplyGetAudioSourceStatusAudioSourceStatusInfoReplyAudioSourceStatusInfoObjFocusStatusFocusStatusObjDeviceObj = attachEnv->GetObjectField(jReplyGetAudioSourceStatusReplyGetAudioSourceStatusAudioSourceStatusInfoReplyAudioSourceStatusInfoObjFocusStatusFocusStatusObj, mDepsCache->mFocusStatus.deviceId);
	jobject jReplyGetAudioSourceStatusReplyGetAudioSourceStatusAudioSourceStatusInfoReplyAudioSourceStatusInfoObjFocusStatusFocusStatusObjStatusObj = attachEnv->GetObjectField(jReplyGetAudioSourceStatusReplyGetAudioSourceStatusAudioSourceStatusInfoReplyAudioSourceStatusInfoObjFocusStatusFocusStatusObj, mDepsCache->mFocusStatus.statusId);
	jobject jReplyGetAudioSourceStatusReplyGetAudioSourceStatusAudioSourceStatusInfoReplyAudioSourceStatusInfoObjVolumeInfoVolumeInfoObj = attachEnv->GetObjectField(jReplyGetAudioSourceStatusReplyGetAudioSourceStatusAudioSourceStatusInfoReplyAudioSourceStatusInfoObj, mDepsCache->mAudioSourceStatusInfo.volumeInfoId);
	jbyte jReplyGetAudioSourceStatusReplyGetAudioSourceStatusAudioSourceStatusInfoReplyAudioSourceStatusInfoObjVolumeInfoVolumeInfoObjVolume = attachEnv->GetByteField(jReplyGetAudioSourceStatusReplyGetAudioSourceStatusAudioSourceStatusInfoReplyAudioSourceStatusInfoObjVolumeInfoVolumeInfoObj, mDepsCache->mVolumeInfo.volumeId);
	jbyte jReplyGetAudioSourceStatusReplyGetAudioSourceStatusAudioSourceStatusInfoReplyAudioSourceStatusInfoObjVolumeInfoVolumeInfoObjMaxSoftVolume = attachEnv->GetByteField(jReplyGetAudioSourceStatusReplyGetAudioSourceStatusAudioSourceStatusInfoReplyAudioSourceStatusInfoObjVolumeInfoVolumeInfoObj, mDepsCache->mVolumeInfo.maxSoftVolumeId);
	jbyte jReplyGetAudioSourceStatusReplyGetAudioSourceStatusAudioSourceStatusInfoReplyAudioSourceStatusInfoObjVolumeInfoVolumeInfoObjLastVolume = attachEnv->GetByteField(jReplyGetAudioSourceStatusReplyGetAudioSourceStatusAudioSourceStatusInfoReplyAudioSourceStatusInfoObjVolumeInfoVolumeInfoObj, mDepsCache->mVolumeInfo.lastVolumeId);
	jboolean jReplyGetAudioSourceStatusReplyGetAudioSourceStatusAudioSourceStatusInfoReplyAudioSourceStatusInfoObjVolumeInfoVolumeInfoObjMute = attachEnv->GetBooleanField(jReplyGetAudioSourceStatusReplyGetAudioSourceStatusAudioSourceStatusInfoReplyAudioSourceStatusInfoObjVolumeInfoVolumeInfoObj, mDepsCache->mVolumeInfo.muteId);
	jint jErrErrorTypeValue = attachEnv->CallIntMethod(jReplyGetAudioSourceStatusReplyGetAudioSourceStatusErrErrorTypeObj, mDepsCache->mErrorTypeOrdinalMethodId);
	v2_Test_Audio::Controller::ErrorType retErrErrorType;
	retErrErrorType.value_ = (int)jErrErrorTypeValue;
	const char *jReplyGetAudioSourceStatusReplyGetAudioSourceStatusAudioSourceStatusInfoReplyAudioSourceStatusInfoObjAudioSourceTypeCharStr = attachEnv->GetStringUTFChars(jReplyGetAudioSourceStatusReplyGetAudioSourceStatusAudioSourceStatusInfoReplyAudioSourceStatusInfoObjAudioSourceType,0);
	std::string retjReplyGetAudioSourceStatusReplyGetAudioSourceStatusAudioSourceStatusInfoReplyAudioSourceStatusInfoObjAudioSourceTypeStr(jReplyGetAudioSourceStatusReplyGetAudioSourceStatusAudioSourceStatusInfoReplyAudioSourceStatusInfoObjAudioSourceTypeCharStr);
	const char *jReplyGetAudioSourceStatusReplyGetAudioSourceStatusAudioSourceStatusInfoReplyAudioSourceStatusInfoObjFocusStatusFocusStatusObjAudioSourceTypeCharStr = attachEnv->GetStringUTFChars(jReplyGetAudioSourceStatusReplyGetAudioSourceStatusAudioSourceStatusInfoReplyAudioSourceStatusInfoObjFocusStatusFocusStatusObjAudioSourceType,0);
	std::string retjReplyGetAudioSourceStatusReplyGetAudioSourceStatusAudioSourceStatusInfoReplyAudioSourceStatusInfoObjFocusStatusFocusStatusObjAudioSourceTypeStr(jReplyGetAudioSourceStatusReplyGetAudioSourceStatusAudioSourceStatusInfoReplyAudioSourceStatusInfoObjFocusStatusFocusStatusObjAudioSourceTypeCharStr);
	jint jDeviceValue = attachEnv->CallIntMethod(jReplyGetAudioSourceStatusReplyGetAudioSourceStatusAudioSourceStatusInfoReplyAudioSourceStatusInfoObjFocusStatusFocusStatusObjDeviceObj, mDepsCache->mAudioDeviceTypeOrdinalMethodId);
	v2_Test_Audio::Controller::AudioDeviceType retDevice;
	retDevice.value_ = (int)jDeviceValue;
	jint jStatusValue = attachEnv->CallIntMethod(jReplyGetAudioSourceStatusReplyGetAudioSourceStatusAudioSourceStatusInfoReplyAudioSourceStatusInfoObjFocusStatusFocusStatusObjStatusObj, mDepsCache->mAudioFocusStatusTypeOrdinalMethodId);
	v2_Test_Audio::Controller::AudioFocusStatusType retStatus;
	retStatus.value_ = (int)jStatusValue;
	v2_Test_Audio::Controller::FocusStatus retFocusStatusFocusStatus(
		retjReplyGetAudioSourceStatusReplyGetAudioSourceStatusAudioSourceStatusInfoReplyAudioSourceStatusInfoObjFocusStatusFocusStatusObjAudioSourceTypeStr,
		retDevice,
		retStatus);
	v2_Test_Audio::Controller::VolumeInfo retVolumeInfoVolumeInfo(
		(uint8_t)jReplyGetAudioSourceStatusReplyGetAudioSourceStatusAudioSourceStatusInfoReplyAudioSourceStatusInfoObjVolumeInfoVolumeInfoObjVolume,
		(uint8_t)jReplyGetAudioSourceStatusReplyGetAudioSourceStatusAudioSourceStatusInfoReplyAudioSourceStatusInfoObjVolumeInfoVolumeInfoObjMaxSoftVolume,
		(uint8_t)jReplyGetAudioSourceStatusReplyGetAudioSourceStatusAudioSourceStatusInfoReplyAudioSourceStatusInfoObjVolumeInfoVolumeInfoObjLastVolume,
		jReplyGetAudioSourceStatusReplyGetAudioSourceStatusAudioSourceStatusInfoReplyAudioSourceStatusInfoObjVolumeInfoVolumeInfoObjMute == JNI_TRUE ? true: false);
	v2_Test_Audio::Controller::AudioSourceStatusInfo retAudioSourceStatusInfoReplyAudioSourceStatusInfo(
		retjReplyGetAudioSourceStatusReplyGetAudioSourceStatusAudioSourceStatusInfoReplyAudioSourceStatusInfoObjAudioSourceTypeStr,
		retFocusStatusFocusStatus,
		retVolumeInfoVolumeInfo);
	attachEnv->DeleteLocalRef(jAudioSourceTypeStr);
	
	errErrorType = retErrErrorType;
	replyAudioSourceStatusInfo = retAudioSourceStatusInfoReplyAudioSourceStatusInfo;
    mJavaVM->DetachCurrentThread();
}

