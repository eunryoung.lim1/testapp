/*
 * Copyright (c) 2023 42dot All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <logging.h>
#include <jni.h>

#include "JNITestAudioControllerEventListener.h"

JNITestAudioControllerEventListener::JNITestAudioControllerEventListener(JNIEnv *env, jobject callback, JNITestAudioControllerDepsCachePtr depsCache)
    : mEnv(env)
	, mDepsCache(depsCache) {
    LOGI("JNITestAudioControllerEventListener::JNITestAudioControllerEventListener()");

    if (mDepsCache == nullptr){
        LOGE("JNITestAudioControllerEventListener:: dependent cache info pointer is null!!");
    }

    mCallbackObj = mEnv->NewGlobalRef(callback);
    jint attachResult = mEnv->GetJavaVM(&mJavaVM);
    if (attachResult != JNI_OK) {
        return;
    }

    jclass callbackClass = mEnv->GetObjectClass(mCallbackObj);
	mOnFocusStatusChangedReceivedId = JNIHelper::getMethodId(mEnv, callbackClass, "onFocusStatusChangedReceived", "([Lai/umos/soa/test/audio/data/FocusStatus;)V");
}

JNITestAudioControllerEventListener::~JNITestAudioControllerEventListener() {
    LOGI("JNITestAudioControllerEventListener::~JNITestAudioControllerEventListener()");
    mEnv->DeleteGlobalRef(mCallbackObj);
}

void JNITestAudioControllerEventListener::onFocusStatusChangedReceived(const std::vector<v2_Test_Audio::Controller::FocusStatus>& focusStatusVec) {
    JNIEnv* attachEnv;
    if (mJavaVM->AttachCurrentThread(&attachEnv, nullptr) != JNI_OK) {
        return ;
    }

    jsize jfocusStatusVecSize = static_cast<jsize>(focusStatusVec.size());
	jobjectArray jFocusStatusFocusStatusFocusStatusArrayObj = attachEnv->NewObjectArray(jfocusStatusVecSize, mDepsCache->mFocusStatusClass, nullptr);
	for (jsize idx = 0; idx < jfocusStatusVecSize; ++idx) {
		auto focusStatusFocusStatus = focusStatusVec[idx];
		auto focusStatusFocusStatusAudioDeviceTypeDevice = focusStatusFocusStatus.getDevice();
		jobjectArray jAudioDeviceTypeDeviceEnumArray = (jobjectArray)attachEnv->CallStaticObjectMethod(mDepsCache->mAudioDeviceTypeClass, mDepsCache->mAudioDeviceTypeValuesId);
		auto focusStatusFocusStatusAudioFocusStatusTypeStatus = focusStatusFocusStatus.getStatus();
		jobjectArray jAudioFocusStatusTypeStatusEnumArray = (jobjectArray)attachEnv->CallStaticObjectMethod(mDepsCache->mAudioFocusStatusTypeClass, mDepsCache->mAudioFocusStatusTypeValuesId);
		
		jstring jFocusStatusFocusStatusAudioSourceTypeStr = attachEnv->NewStringUTF(focusStatusFocusStatus.getAudioSourceType().c_str());
		jobject jAudioDeviceTypeDeviceObj = attachEnv->GetObjectArrayElement(jAudioDeviceTypeDeviceEnumArray, static_cast<int>((uint8_t)(focusStatusFocusStatusAudioDeviceTypeDevice.value_)));
		jobject jAudioFocusStatusTypeStatusObj = attachEnv->GetObjectArrayElement(jAudioFocusStatusTypeStatusEnumArray, static_cast<int>((uint8_t)(focusStatusFocusStatusAudioFocusStatusTypeStatus.value_)));
		jobject jFocusStatusFocusStatusObj = attachEnv->NewObject(mDepsCache->mFocusStatusClass, mDepsCache->mFocusStatusCtorId,
			jFocusStatusFocusStatusAudioSourceTypeStr,
			jAudioDeviceTypeDeviceObj,
			jAudioFocusStatusTypeStatusObj);
		
		attachEnv->SetObjectArrayElement(jFocusStatusFocusStatusFocusStatusArrayObj, idx, jFocusStatusFocusStatusObj);
		attachEnv->DeleteLocalRef(jFocusStatusFocusStatusAudioSourceTypeStr);
		attachEnv->DeleteLocalRef(jAudioDeviceTypeDeviceObj);
		attachEnv->DeleteLocalRef(jAudioFocusStatusTypeStatusObj);
		attachEnv->DeleteLocalRef(jFocusStatusFocusStatusObj);
	}
	attachEnv->CallVoidMethod(mCallbackObj, mOnFocusStatusChangedReceivedId, 
			jFocusStatusFocusStatusFocusStatusArrayObj);
	attachEnv->DeleteLocalRef(jFocusStatusFocusStatusFocusStatusArrayObj);
	
    mJavaVM->DetachCurrentThread();
}
