
/*
 * Copyright (c) 2023 42dot All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include <jni.h>
#include <unordered_map>

#include <common/JNIHelper.h>

#include <CommonAPI/CommonAPI.hpp>
#include <v2/Test/Audio/ControllerProxy.hpp>

#include <test/audio/JNITestAudioControllerDepsCache.h>

namespace v2_Test_Audio = v2::Test::Audio;
using namespace v2_Test_Audio;

class JNITestAudioControllerAsyncCallback final {
public:
    JNITestAudioControllerAsyncCallback(JNIEnv *env, JNITestAudioControllerDepsCachePtr depsCache);
    ~JNITestAudioControllerAsyncCallback();

    struct JNICallbackInfo{
		jobject mCallbackObj;
		jmethodID mCallbackId;
	};

    bool registerCallback(JNIEnv *env, jobject jCallback, const char *methodName, const char *methodSignature, uint32_t& callbackId);
    bool unregisterCallback(uint32_t callbackId, JNICallbackInfo& callbackInfo);

	void onIVIDeviceInfoAttr(uint32_t callbackId, const CommonAPI::CallStatus& callStatus, const v2_Test_Audio::Controller::KeyValueString& replyIVIDeviceInfo);
	void onAudioSourceTypesAttr(uint32_t callbackId, const CommonAPI::CallStatus& callStatus, const std::vector<std::string>& replyAudioSourceTypesVec);
	void onVolumeInfoAttr(uint32_t callbackId, const CommonAPI::CallStatus& callStatus, const v2_Test_Audio::Controller::VolumeInfo& replyVolumeInfo);
	void onKeyValueByteBufferMapAttr(uint32_t callbackId, const CommonAPI::CallStatus& callStatus, const v2_Test_Audio::Controller::KeyValueByteBuffer& replyKeyValueByteBufferMap);
	void onDeviceStateMapAttr(uint32_t callbackId, const CommonAPI::CallStatus& callStatus, const v2_Test_Audio::Controller::DeviceStateMap& replyDeviceStateMap);
	void onStringToShortMapAttr(uint32_t callbackId, const CommonAPI::CallStatus& callStatus, const v2_Test_Audio::Controller::StringToShortMap& replyStringToShortMap);

	void onObtainFocus(uint32_t callbackId, const CommonAPI::CallStatus& callStatus, const v2_Test_Audio::Controller::FocusControlError& replyErrFocusControlError, const v2_Test_Audio::Controller::AudioFocusResultType& replyRes);
	void onReleaseFocus(uint32_t callbackId, const CommonAPI::CallStatus& callStatus, const v2_Test_Audio::Controller::FocusControlError& replyErrFocusControlError);
	void onSetVolume(uint32_t callbackId, const CommonAPI::CallStatus& callStatus, const v2_Test_Audio::Controller::VolumeControlError& replyErrVolumeControlError);
	void onSetSourceVolume(uint32_t callbackId, const CommonAPI::CallStatus& callStatus, const v2_Test_Audio::Controller::VolumeControlError& replyErrVolumeControlError);
	void onGetAudioSourceStatus(uint32_t callbackId, const CommonAPI::CallStatus& callStatus, const v2_Test_Audio::Controller::ErrorType& replyErrErrorType, const v2_Test_Audio::Controller::AudioSourceStatusInfo& replyAudioSourceStatusInfo);

private:
    JavaVM*    mJavaVM{ nullptr };
    JNIEnv*    mEnv{ nullptr };
    JNITestAudioControllerDepsCachePtr    mDepsCache{ nullptr };

    std::atomic<uint32_t> mCallbackId;
	using CallbackInfoMap = std::unordered_map<uint32_t, JNICallbackInfo>;
    std::mutex mCallbackMapMutex;
    CallbackInfoMap mCallbackMap;
};

using JNITestAudioControllerAsyncCallbackPtr = std::shared_ptr<JNITestAudioControllerAsyncCallback>;