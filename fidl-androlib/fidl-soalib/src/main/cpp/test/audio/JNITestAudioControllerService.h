/*
 * Copyright (c) 2023 42dot All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <logging.h>
#include <jni.h>

#include <common/JNIHelper.h>

#include <Test/Audio/TestAudioControllerStubImpl.h>
#include <test/audio/JNITestAudioControllerHandler.h>
#include <test/audio/JNITestAudioControllerDepsCache.h>

using namespace ai::umos::soa::core;

namespace v2_Test_Audio = v2::Test::Audio;
using namespace v2_Test_Audio;

class JNITestAudioControllerService {
public:
    static jint initializeTestAudioControllerService(JavaVM* vm);

private:
    JNITestAudioControllerService();
    virtual ~JNITestAudioControllerService();

	static void nativeStartService(JNIEnv *env, jobject thiz, jobject handler);
	static void nativeStopService(JNIEnv *env, jobject thiz);

	static void nativeUpdateIVIDeviceInfoAttr(JNIEnv *env, jobject thiz, jobject jUpdateIVIDeviceInfoValue);
	static void nativeUpdateAudioSourceTypesAttr(JNIEnv *env, jobject thiz, jobjectArray jUpdateAudioSourceTypesArrayValue);
	static void nativeUpdateVolumeInfoAttr(JNIEnv *env, jobject thiz, jobject jUpdateVolumeInfoValue);
	static void nativeUpdateKeyValueByteBufferMapAttr(JNIEnv *env, jobject thiz, jobject jUpdateKeyValueByteBufferMapValue);
	static void nativeUpdateDeviceStateMapAttr(JNIEnv *env, jobject thiz, jobject jUpdateDeviceStateMapValue);
	static void nativeUpdateStringToShortMapAttr(JNIEnv *env, jobject thiz, jobject jUpdateStringToShortMapValue);

	static void fireFocusStatusChanged(JNIEnv *env, jobject thiz, jobjectArray jFocusStatusArray);

	static TestAudioControllerStubImplPtr		mNativeStub;
	static JNITestAudioControllerHandlerPtr		mHandler;
	static JNITestAudioControllerDepsCachePtr		mDepsCache;

};