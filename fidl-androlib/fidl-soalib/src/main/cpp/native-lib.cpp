/*
 * Copyright 2021 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <logging.h>
#include <jni.h>

#include <SoaServiceManager.h>
#include <SoaClientManager.h>

#include <test/audio/JNITestAudioControllerService.h>

#include <test/audio/JNITestAudioControllerClient.h>

extern jint initializeTestAudioControllerService(JavaVM* vm);

extern jint initializeTestAudioControllerClient(JavaVM* vm);


using namespace ai::umos::soa::core;

SoaServiceManagerPtr    gSoaServiceManager;
SoaClientManagerPtr     gSoaClientManager;

JNIEXPORT jint JNI_OnLoad(JavaVM* vm, void* /*reserved*/) {
    LOGI("%s", __func__);

    JNIEnv* env;
    if (vm->GetEnv(reinterpret_cast<void**>(&env), JNI_VERSION_1_6) != JNI_OK) {
        return JNI_ERR;
    }

    gSoaServiceManager = std::make_shared<SoaServiceManager>();
    gSoaClientManager = std::make_shared<SoaClientManager>();

	JNITestAudioControllerService::initializeTestAudioControllerService(vm);

	JNITestAudioControllerClient::initializeTestAudioControllerClient(vm);

    return JNI_VERSION_1_6;
}

JNIEXPORT void JNI_OnUnload(JavaVM* vm, void* /*reserved*/) {
    JNIEnv* env;
    if (vm->GetEnv(reinterpret_cast<void**>(&env), JNI_VERSION_1_6) != JNI_OK) {
        LOGI("%s : Failed to get the environment.", __func__);
        return;
    }
}

