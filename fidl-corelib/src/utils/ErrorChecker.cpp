/*
 * Copyright (c) 2023 42dot All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <utils/ErrorChecker.h>

namespace ai {
namespace umos {
namespace soa {
namespace core {

bool ErrorChecker::checkProxyCallStatus(const CommonAPI::CallStatus& callStatus) {
    switch(callStatus) {
        case CommonAPI::CallStatus::SUCCESS:
            return true;
        case CommonAPI::CallStatus::OUT_OF_MEMORY:
        case CommonAPI::CallStatus::NOT_AVAILABLE:
        case CommonAPI::CallStatus::CONNECTION_FAILED:
        case CommonAPI::CallStatus::REMOTE_ERROR:
        case CommonAPI::CallStatus::UNKNOWN:
        case CommonAPI::CallStatus::INVALID_VALUE:
        case CommonAPI::CallStatus::SUBSCRIPTION_REFUSED:
        case CommonAPI::CallStatus::SERIALIZATION_ERROR:
            std::cout << "Callstatus has error, errorType: " << (int)callStatus << std::endl;
            return false;
        default:
            break;
    }

    return false;
}

void ErrorChecker::printErrorMessage(const std::string& errorMessage) {
    std::cout << "Proxy server is not available, causing: " << errorMessage << std::endl;
}

} //namespace core
} //namespace ai
} //namespace umos
} //namespace soa
