/*
 * Copyright (c) 2023 42dot All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License";
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <Test/Audio/TestAudioControllerClient.h>

#include <logging.h>
#include <thread>

#include <SoaConstants.h>
#include <utils/ErrorChecker.h>

namespace ai {
namespace umos {
namespace soa {
namespace core {

using namespace v2::Test::Audio;

const uint32_t SERVICE_CONNECT_MAX_RETRY = 500;

TestAudioControllerClient::TestAudioControllerClient(const std::shared_ptr<CommonAPI::Runtime>& commonApiRuntime)
    : mCommonApiRuntime(commonApiRuntime) {
    init();
}

TestAudioControllerClient::~TestAudioControllerClient() {
    if (mServerConnectionThread.joinable()) {
        mServerConnectionThread.join();
    }

    disconnectServer();
}

void TestAudioControllerClient::connectServer(const IServiceStatusListenerPtr& serviceStatusListener) {
    if (mProxyServer == nullptr) {
        init();
    }

    mServiceStatusListener = serviceStatusListener;

    mServerConnectionThread = std::thread([this]() {
        uint32_t tryCount = 0;
        while (!mProxyServer->isAvailable() && ++tryCount <= SERVICE_CONNECT_MAX_RETRY) {
            LOGI("Waiting for the TestAudioController server to become available");
            std::this_thread::sleep_for(std::chrono::milliseconds(200));
        }
        if (!mProxyServer->isAvailable()) {
            LOGI("TestAudioController server is NOT available, even after max retry");
            if(auto listener = mServiceStatusListener.lock()) {
                listener->onServiceNotAvailable();
            }
        } else {
            subscribeServerEvents();
        }
    });
}

void TestAudioControllerClient::disconnectServer() {
    if (mProxyServer == nullptr) {
        return ;
    }
	//resetAttributeListener();
	resetBroadcastEventListener();

    mAttrListener.reset();
    mServiceStatusListener.reset();
    mProxyServer.reset();
}

bool TestAudioControllerClient::setIVIDeviceInfoAttr(const v2_Test_Audio::Controller::KeyValueString& newIVIDeviceInfoValue, v2_Test_Audio::Controller::KeyValueString& responseValue){
    LOGI("%s", __func__);
    if (mProxyServer->isAvailable()) {
        CommonAPI::CallStatus callStatus;
        mProxyServer->getIVIDeviceInfoAttribute().setValue(newIVIDeviceInfoValue, callStatus, responseValue);
        if (!ErrorChecker::checkProxyCallStatus(callStatus)) {
            ErrorChecker::printErrorMessage(std::string("Failed to call setIVIDeviceInfoAttr(), you need to check arguments"));
            return false;
        }
    } else {
        ErrorChecker::printErrorMessage(std::string("Failed to call setIVIDeviceInfoAttr(), server is not available"));
        return false;
    }
    return true;
}

void TestAudioControllerClient::setIVIDeviceInfoAttrAsync(const v2_Test_Audio::Controller::KeyValueString& newIVIDeviceInfoValue, v2_Test_Audio::ControllerProxyBase::IVIDeviceInfoAttribute::AttributeAsyncCallback callback){
    LOGI("%s", __func__);
    if (mProxyServer->isAvailable()) {
        mProxyServer->getIVIDeviceInfoAttribute().setValueAsync(newIVIDeviceInfoValue, callback);
    } else {
        ErrorChecker::printErrorMessage(std::string("Failed to call setIVIDeviceInfoAttrAsync(), server is not available"));
    }
}

bool TestAudioControllerClient::getIVIDeviceInfoAttr(v2_Test_Audio::Controller::KeyValueString& getValue){
    LOGI("%s", __func__);
    if (mProxyServer->isAvailable()) {
        CommonAPI::CallStatus callStatus;
        mProxyServer->getIVIDeviceInfoAttribute().getValue(callStatus, getValue);
        if (!ErrorChecker::checkProxyCallStatus(callStatus)) {
            ErrorChecker::printErrorMessage(std::string("Failed to call getIVIDeviceInfoAttr(), you need to check arguments"));
            return false;
        }
    } else {
        ErrorChecker::printErrorMessage(std::string("Failed to call getIVIDeviceInfoAttr(), server is not available"));
        return false;
    }
    return true;
}

void TestAudioControllerClient::getIVIDeviceInfoAttrAsync(v2_Test_Audio::ControllerProxyBase::IVIDeviceInfoAttribute::AttributeAsyncCallback callback){
    LOGI("%s", __func__);
    if (mProxyServer->isAvailable()) {
        mProxyServer->getIVIDeviceInfoAttribute().getValueAsync(callback);
    } else {
        ErrorChecker::printErrorMessage(std::string("Failed to call getIVIDeviceInfoAttrAsync(), server is not available"));
    }
}

bool TestAudioControllerClient::getAudioSourceTypesAttr(std::vector<std::string>& getValue){
    LOGI("%s", __func__);
    if (mProxyServer->isAvailable()) {
        CommonAPI::CallStatus callStatus;
        mProxyServer->getAudioSourceTypesAttribute().getValue(callStatus, getValue);
        if (!ErrorChecker::checkProxyCallStatus(callStatus)) {
            ErrorChecker::printErrorMessage(std::string("Failed to call getAudioSourceTypesAttr(), you need to check arguments"));
            return false;
        }
    } else {
        ErrorChecker::printErrorMessage(std::string("Failed to call getAudioSourceTypesAttr(), server is not available"));
        return false;
    }
    return true;
}

void TestAudioControllerClient::getAudioSourceTypesAttrAsync(v2_Test_Audio::ControllerProxyBase::AudioSourceTypesAttribute::AttributeAsyncCallback callback){
    LOGI("%s", __func__);
    if (mProxyServer->isAvailable()) {
        mProxyServer->getAudioSourceTypesAttribute().getValueAsync(callback);
    } else {
        ErrorChecker::printErrorMessage(std::string("Failed to call getAudioSourceTypesAttrAsync(), server is not available"));
    }
}

bool TestAudioControllerClient::getVolumeInfoAttr(v2_Test_Audio::Controller::VolumeInfo& getValue){
    LOGI("%s", __func__);
    if (mProxyServer->isAvailable()) {
        CommonAPI::CallStatus callStatus;
        mProxyServer->getVolumeInfoAttribute().getValue(callStatus, getValue);
        if (!ErrorChecker::checkProxyCallStatus(callStatus)) {
            ErrorChecker::printErrorMessage(std::string("Failed to call getVolumeInfoAttr(), you need to check arguments"));
            return false;
        }
    } else {
        ErrorChecker::printErrorMessage(std::string("Failed to call getVolumeInfoAttr(), server is not available"));
        return false;
    }
    return true;
}

void TestAudioControllerClient::getVolumeInfoAttrAsync(v2_Test_Audio::ControllerProxyBase::VolumeInfoAttribute::AttributeAsyncCallback callback){
    LOGI("%s", __func__);
    if (mProxyServer->isAvailable()) {
        mProxyServer->getVolumeInfoAttribute().getValueAsync(callback);
    } else {
        ErrorChecker::printErrorMessage(std::string("Failed to call getVolumeInfoAttrAsync(), server is not available"));
    }
}

bool TestAudioControllerClient::getKeyValueByteBufferMapAttr(v2_Test_Audio::Controller::KeyValueByteBuffer& getValue){
    LOGI("%s", __func__);
    if (mProxyServer->isAvailable()) {
        CommonAPI::CallStatus callStatus;
        mProxyServer->getKeyValueByteBufferMapAttribute().getValue(callStatus, getValue);
        if (!ErrorChecker::checkProxyCallStatus(callStatus)) {
            ErrorChecker::printErrorMessage(std::string("Failed to call getKeyValueByteBufferMapAttr(), you need to check arguments"));
            return false;
        }
    } else {
        ErrorChecker::printErrorMessage(std::string("Failed to call getKeyValueByteBufferMapAttr(), server is not available"));
        return false;
    }
    return true;
}

void TestAudioControllerClient::getKeyValueByteBufferMapAttrAsync(v2_Test_Audio::ControllerProxyBase::KeyValueByteBufferMapAttribute::AttributeAsyncCallback callback){
    LOGI("%s", __func__);
    if (mProxyServer->isAvailable()) {
        mProxyServer->getKeyValueByteBufferMapAttribute().getValueAsync(callback);
    } else {
        ErrorChecker::printErrorMessage(std::string("Failed to call getKeyValueByteBufferMapAttrAsync(), server is not available"));
    }
}

bool TestAudioControllerClient::getDeviceStateMapAttr(v2_Test_Audio::Controller::DeviceStateMap& getValue){
    LOGI("%s", __func__);
    if (mProxyServer->isAvailable()) {
        CommonAPI::CallStatus callStatus;
        mProxyServer->getDeviceStateMapAttribute().getValue(callStatus, getValue);
        if (!ErrorChecker::checkProxyCallStatus(callStatus)) {
            ErrorChecker::printErrorMessage(std::string("Failed to call getDeviceStateMapAttr(), you need to check arguments"));
            return false;
        }
    } else {
        ErrorChecker::printErrorMessage(std::string("Failed to call getDeviceStateMapAttr(), server is not available"));
        return false;
    }
    return true;
}

void TestAudioControllerClient::getDeviceStateMapAttrAsync(v2_Test_Audio::ControllerProxyBase::DeviceStateMapAttribute::AttributeAsyncCallback callback){
    LOGI("%s", __func__);
    if (mProxyServer->isAvailable()) {
        mProxyServer->getDeviceStateMapAttribute().getValueAsync(callback);
    } else {
        ErrorChecker::printErrorMessage(std::string("Failed to call getDeviceStateMapAttrAsync(), server is not available"));
    }
}

bool TestAudioControllerClient::getStringToShortMapAttr(v2_Test_Audio::Controller::StringToShortMap& getValue){
    LOGI("%s", __func__);
    if (mProxyServer->isAvailable()) {
        CommonAPI::CallStatus callStatus;
        mProxyServer->getStringToShortMapAttribute().getValue(callStatus, getValue);
        if (!ErrorChecker::checkProxyCallStatus(callStatus)) {
            ErrorChecker::printErrorMessage(std::string("Failed to call getStringToShortMapAttr(), you need to check arguments"));
            return false;
        }
    } else {
        ErrorChecker::printErrorMessage(std::string("Failed to call getStringToShortMapAttr(), server is not available"));
        return false;
    }
    return true;
}

void TestAudioControllerClient::getStringToShortMapAttrAsync(v2_Test_Audio::ControllerProxyBase::StringToShortMapAttribute::AttributeAsyncCallback callback){
    LOGI("%s", __func__);
    if (mProxyServer->isAvailable()) {
        mProxyServer->getStringToShortMapAttribute().getValueAsync(callback);
    } else {
        ErrorChecker::printErrorMessage(std::string("Failed to call getStringToShortMapAttrAsync(), server is not available"));
    }
}

bool TestAudioControllerClient::ObtainFocus(const std::string& audioSourceType, const v2_Test_Audio::Controller::AudioDeviceType& device, v2_Test_Audio::Controller::FocusControlError& errFocusControlError, v2_Test_Audio::Controller::AudioFocusResultType& replyRes){
    LOGI("%s", __func__);
    if (mProxyServer->isAvailable()) {
        CommonAPI::CallStatus callStatus;
        mProxyServer->ObtainFocus(audioSourceType, device, callStatus, errFocusControlError, replyRes);
        if (!ErrorChecker::checkProxyCallStatus(callStatus)) {
            ErrorChecker::printErrorMessage(std::string("Failed to call ObtainFocus(), you need to check arguments"));
            return false;
        }
    } else {
        ErrorChecker::printErrorMessage(std::string("Failed to call ObtainFocus(), server is not available"));
        return false;
    }
    return true;
}

void TestAudioControllerClient::ObtainFocusAsync(const std::string& audioSourceType, const v2_Test_Audio::Controller::AudioDeviceType& device, v2_Test_Audio::ControllerProxyBase::ObtainFocusAsyncCallback callback){
    LOGI("%s", __func__);
    if (mProxyServer->isAvailable()) {
        mProxyServer->ObtainFocusAsync(audioSourceType, device, callback);
    } else {
        ErrorChecker::printErrorMessage(std::string("Failed to call ObtainFocusAsync(), server is not available"));
    }
}

bool TestAudioControllerClient::ReleaseFocus(const std::string& audioSourceType, const v2_Test_Audio::Controller::AudioDeviceType& device, v2_Test_Audio::Controller::FocusControlError& errFocusControlError){
    LOGI("%s", __func__);
    if (mProxyServer->isAvailable()) {
        CommonAPI::CallStatus callStatus;
        mProxyServer->ReleaseFocus(audioSourceType, device, callStatus, errFocusControlError);
        if (!ErrorChecker::checkProxyCallStatus(callStatus)) {
            ErrorChecker::printErrorMessage(std::string("Failed to call ReleaseFocus(), you need to check arguments"));
            return false;
        }
    } else {
        ErrorChecker::printErrorMessage(std::string("Failed to call ReleaseFocus(), server is not available"));
        return false;
    }
    return true;
}

void TestAudioControllerClient::ReleaseFocusAsync(const std::string& audioSourceType, const v2_Test_Audio::Controller::AudioDeviceType& device, v2_Test_Audio::ControllerProxyBase::ReleaseFocusAsyncCallback callback){
    LOGI("%s", __func__);
    if (mProxyServer->isAvailable()) {
        mProxyServer->ReleaseFocusAsync(audioSourceType, device, callback);
    } else {
        ErrorChecker::printErrorMessage(std::string("Failed to call ReleaseFocusAsync(), server is not available"));
    }
}

bool TestAudioControllerClient::SetVolume(uint8_t volume, v2_Test_Audio::Controller::VolumeControlError& errVolumeControlError){
    LOGI("%s", __func__);
    if (mProxyServer->isAvailable()) {
        CommonAPI::CallStatus callStatus;
        mProxyServer->SetVolume(volume, callStatus, errVolumeControlError);
        if (!ErrorChecker::checkProxyCallStatus(callStatus)) {
            ErrorChecker::printErrorMessage(std::string("Failed to call SetVolume(), you need to check arguments"));
            return false;
        }
    } else {
        ErrorChecker::printErrorMessage(std::string("Failed to call SetVolume(), server is not available"));
        return false;
    }
    return true;
}

void TestAudioControllerClient::SetVolumeAsync(uint8_t volume, v2_Test_Audio::ControllerProxyBase::SetVolumeAsyncCallback callback){
    LOGI("%s", __func__);
    if (mProxyServer->isAvailable()) {
        mProxyServer->SetVolumeAsync(volume, callback);
    } else {
        ErrorChecker::printErrorMessage(std::string("Failed to call SetVolumeAsync(), server is not available"));
    }
}

bool TestAudioControllerClient::SetSourceVolume(const std::string& audioSourceType, uint16_t volume, uint32_t maxVolume, v2_Test_Audio::Controller::VolumeControlError& errVolumeControlError){
    LOGI("%s", __func__);
    if (mProxyServer->isAvailable()) {
        CommonAPI::CallStatus callStatus;
        mProxyServer->SetSourceVolume(audioSourceType, volume, maxVolume, callStatus, errVolumeControlError);
        if (!ErrorChecker::checkProxyCallStatus(callStatus)) {
            ErrorChecker::printErrorMessage(std::string("Failed to call SetSourceVolume(), you need to check arguments"));
            return false;
        }
    } else {
        ErrorChecker::printErrorMessage(std::string("Failed to call SetSourceVolume(), server is not available"));
        return false;
    }
    return true;
}

void TestAudioControllerClient::SetSourceVolumeAsync(const std::string& audioSourceType, uint16_t volume, uint32_t maxVolume, v2_Test_Audio::ControllerProxyBase::SetSourceVolumeAsyncCallback callback){
    LOGI("%s", __func__);
    if (mProxyServer->isAvailable()) {
        mProxyServer->SetSourceVolumeAsync(audioSourceType, volume, maxVolume, callback);
    } else {
        ErrorChecker::printErrorMessage(std::string("Failed to call SetSourceVolumeAsync(), server is not available"));
    }
}

bool TestAudioControllerClient::GetAudioSourceStatus(const std::string& audioSourceType, v2_Test_Audio::Controller::ErrorType& errErrorType, v2_Test_Audio::Controller::AudioSourceStatusInfo& replyAudioSourceStatusInfo){
    LOGI("%s", __func__);
    if (mProxyServer->isAvailable()) {
        CommonAPI::CallStatus callStatus;
        mProxyServer->GetAudioSourceStatus(audioSourceType, callStatus, errErrorType, replyAudioSourceStatusInfo);
        if (!ErrorChecker::checkProxyCallStatus(callStatus)) {
            ErrorChecker::printErrorMessage(std::string("Failed to call GetAudioSourceStatus(), you need to check arguments"));
            return false;
        }
    } else {
        ErrorChecker::printErrorMessage(std::string("Failed to call GetAudioSourceStatus(), server is not available"));
        return false;
    }
    return true;
}

void TestAudioControllerClient::GetAudioSourceStatusAsync(const std::string& audioSourceType, v2_Test_Audio::ControllerProxyBase::GetAudioSourceStatusAsyncCallback callback){
    LOGI("%s", __func__);
    if (mProxyServer->isAvailable()) {
        mProxyServer->GetAudioSourceStatusAsync(audioSourceType, callback);
    } else {
        ErrorChecker::printErrorMessage(std::string("Failed to call GetAudioSourceStatusAsync(), server is not available"));
    }
}

void TestAudioControllerClient::init() {
    mProxyServer = mCommonApiRuntime->buildProxy<ControllerProxy>(SOA_SERVICE_DOMAIN, SOA_TEST_AUDIO_CONTROLLER_SERVICE_INSTANCE);
}

void TestAudioControllerClient::subscribeServerEvents() {
    if (!mProxyServer->isAvailable()) {
        return ;
    }
    mProxyServer->getProxyStatusEvent().subscribe(
        [this](CommonAPI::AvailabilityStatus status) {
            switch(status) {
                case CommonAPI::AvailabilityStatus::AVAILABLE:
                    LOGI("TestAudioController proxy server status is AVAILABLE");
                    if(auto listener = mServiceStatusListener.lock()) {
                        listener->onServiceAvailable();
                    }
					// if (!mAttrListener.expired()) {
					// 	setAttributeListener(mAttrListener);
					// }
					// if (!mEventListener.expired()) {
					// 	setBroadcastEventListener(mEventListener);
					// }
                    break;
                case CommonAPI::AvailabilityStatus::NOT_AVAILABLE:
                    LOGI("TestAudioController proxy server status is NOT_AVAILABLE");
                    if(auto listener = mServiceStatusListener.lock()) {
                        listener->onServiceNotAvailable();
                    }
					//resetAttributeListener();
					resetBroadcastEventListener();
                    break;
                default:
                    LOGI("TestAudioController proxy server status is UNKNOWN");
                    if(auto listener = mServiceStatusListener.lock()) {
                        listener->onServiceNotAvailable();
                    }
                    break;
            }
        }
    );
}

void TestAudioControllerClient::setAttributeListener(const ITestAudioControllerAttrListenerPtr& attrListener) {
    LOGI("TestAudioControllerClient::%s", __func__);
    // if (attrListener.expired()) {
    //     LOGI("Failed to set attribute listener because the given listener is expired");
    //     return ;
    // }

    mAttrListener = attrListener;
    if (!mProxyServer->isAvailable()) {
        return ;
    }

    std::lock_guard<std::mutex> lock(mAttrSubscriptionMapMutex);

    mAttrSubscriptionMap["IVIDeviceInfo"] = mProxyServer->getIVIDeviceInfoAttribute().getChangedEvent().subscribe(
        [this](const v2_Test_Audio::Controller::KeyValueString& iVIDeviceInfo){
            if(auto listener = mAttrListener.lock()) {
                listener->onIVIDeviceInfoChanged(iVIDeviceInfo);
            }
        }
    );

    mAttrSubscriptionMap["AudioSourceTypes"] = mProxyServer->getAudioSourceTypesAttribute().getChangedEvent().subscribe(
        [this](const std::vector<std::string>& audioSourceTypesVec){
            if(auto listener = mAttrListener.lock()) {
                listener->onAudioSourceTypesChanged(audioSourceTypesVec);
            }
        }
    );

    mAttrSubscriptionMap["VolumeInfo"] = mProxyServer->getVolumeInfoAttribute().getChangedEvent().subscribe(
        [this](const v2_Test_Audio::Controller::VolumeInfo& volumeInfo){
            if(auto listener = mAttrListener.lock()) {
                listener->onVolumeInfoChanged(volumeInfo);
            }
        }
    );

    mAttrSubscriptionMap["KeyValueByteBufferMap"] = mProxyServer->getKeyValueByteBufferMapAttribute().getChangedEvent().subscribe(
        [this](const v2_Test_Audio::Controller::KeyValueByteBuffer& keyValueByteBufferMap){
            if(auto listener = mAttrListener.lock()) {
                listener->onKeyValueByteBufferMapChanged(keyValueByteBufferMap);
            }
        }
    );

    mAttrSubscriptionMap["DeviceStateMap"] = mProxyServer->getDeviceStateMapAttribute().getChangedEvent().subscribe(
        [this](const v2_Test_Audio::Controller::DeviceStateMap& deviceStateMap){
            if(auto listener = mAttrListener.lock()) {
                listener->onDeviceStateMapChanged(deviceStateMap);
            }
        }
    );

    mAttrSubscriptionMap["StringToShortMap"] = mProxyServer->getStringToShortMapAttribute().getChangedEvent().subscribe(
        [this](const v2_Test_Audio::Controller::StringToShortMap& stringToShortMap){
            if(auto listener = mAttrListener.lock()) {
                listener->onStringToShortMapChanged(stringToShortMap);
            }
        }
    );

}

void TestAudioControllerClient::resetAttributeListener() {
    LOGI("%s", __func__);
    // mAttrListener.reset();
    if (!mProxyServer->isAvailable()) {
        return ;
    }

    std::lock_guard<std::mutex> lock(mAttrSubscriptionMapMutex);
    if (!mAttrSubscriptionMap.empty()) {
	    if (mAttrSubscriptionMap.find("IVIDeviceInfo") != mAttrSubscriptionMap.end()) {
	        mProxyServer->getIVIDeviceInfoAttribute().getChangedEvent().unsubscribe(mAttrSubscriptionMap["IVIDeviceInfo"]);
	        mAttrSubscriptionMap.erase("IVIDeviceInfo");
	    }

	    if (mAttrSubscriptionMap.find("AudioSourceTypes") != mAttrSubscriptionMap.end()) {
	        mProxyServer->getAudioSourceTypesAttribute().getChangedEvent().unsubscribe(mAttrSubscriptionMap["AudioSourceTypes"]);
	        mAttrSubscriptionMap.erase("AudioSourceTypes");
	    }

	    if (mAttrSubscriptionMap.find("VolumeInfo") != mAttrSubscriptionMap.end()) {
	        mProxyServer->getVolumeInfoAttribute().getChangedEvent().unsubscribe(mAttrSubscriptionMap["VolumeInfo"]);
	        mAttrSubscriptionMap.erase("VolumeInfo");
	    }

	    if (mAttrSubscriptionMap.find("KeyValueByteBufferMap") != mAttrSubscriptionMap.end()) {
	        mProxyServer->getKeyValueByteBufferMapAttribute().getChangedEvent().unsubscribe(mAttrSubscriptionMap["KeyValueByteBufferMap"]);
	        mAttrSubscriptionMap.erase("KeyValueByteBufferMap");
	    }

	    if (mAttrSubscriptionMap.find("DeviceStateMap") != mAttrSubscriptionMap.end()) {
	        mProxyServer->getDeviceStateMapAttribute().getChangedEvent().unsubscribe(mAttrSubscriptionMap["DeviceStateMap"]);
	        mAttrSubscriptionMap.erase("DeviceStateMap");
	    }

	    if (mAttrSubscriptionMap.find("StringToShortMap") != mAttrSubscriptionMap.end()) {
	        mProxyServer->getStringToShortMapAttribute().getChangedEvent().unsubscribe(mAttrSubscriptionMap["StringToShortMap"]);
	        mAttrSubscriptionMap.erase("StringToShortMap");
	    }
    }
}

void TestAudioControllerClient::setBroadcastEventListener(const ITestAudioControllerEventListenerWPtr& eventListener) {
    LOGI("%s", __func__);
    if (eventListener.expired()) {
        LOGI("Failed to set broadcast event listener because the given event listener is expired");
        return ;
    }

    mEventListener = eventListener;
    if (!mProxyServer->isAvailable()) {
        return ;
    }

    std::lock_guard<std::mutex> lock(mEventSubscriptionMapMutex);

    mEventSubscriptionMap["FocusStatusChanged"] = mProxyServer->getFocusStatusChangedEvent().subscribe(
        [this](const std::vector<v2_Test_Audio::Controller::FocusStatus>& focusStatusVec){
            if(auto listener = mEventListener.lock()) {
                listener->onFocusStatusChangedReceived(focusStatusVec);
            }
        }
    );

}

void TestAudioControllerClient::resetBroadcastEventListener() {
    LOGI("%s", __func__);
    mEventListener.reset();
    if (!mProxyServer->isAvailable()) {
        return ;
    }

    std::lock_guard<std::mutex> lock(mEventSubscriptionMapMutex);
    if (!mEventSubscriptionMap.empty()) {
	    if (mEventSubscriptionMap.find("FocusStatusChanged") != mEventSubscriptionMap.end()) {
	        mProxyServer->getFocusStatusChangedEvent().unsubscribe(mEventSubscriptionMap["FocusStatusChanged"]);
	        mEventSubscriptionMap.erase("FocusStatusChanged");
	    }
    }
}

} //namespace core
} //namespace ai
} //namespace umos
} //namespace soa
