/*
 * Copyright (c) 2023 42dot All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <Test/Audio/TestAudioControllerStubImpl.h>

#include <logging.h>

namespace ai {
namespace umos {
namespace soa {
namespace core {

using namespace v2::Test::Audio;

TestAudioControllerStubImpl::TestAudioControllerStubImpl(const ITestAudioControllerHandlerPtr& handler) {
    mHandler = handler;
}

TestAudioControllerStubImpl::~TestAudioControllerStubImpl() {
    //@TODO
}

void TestAudioControllerStubImpl::updateIVIDeviceInfoAttr(const v2_Test_Audio::Controller::KeyValueString& updateIVIDeviceInfoValue) {
    setIVIDeviceInfoAttribute(updateIVIDeviceInfoValue);
}

void TestAudioControllerStubImpl::updateAudioSourceTypesAttr(const std::vector<std::string>& updateAudioSourceTypesVecValue) {
    setAudioSourceTypesAttribute(updateAudioSourceTypesVecValue);
}

void TestAudioControllerStubImpl::updateVolumeInfoAttr(const v2_Test_Audio::Controller::VolumeInfo& updateVolumeInfoValue) {
    setVolumeInfoAttribute(updateVolumeInfoValue);
}

void TestAudioControllerStubImpl::updateKeyValueByteBufferMapAttr(const v2_Test_Audio::Controller::KeyValueByteBuffer& updateKeyValueByteBufferMapValue) {
    setKeyValueByteBufferMapAttribute(updateKeyValueByteBufferMapValue);
}

void TestAudioControllerStubImpl::updateDeviceStateMapAttr(const v2_Test_Audio::Controller::DeviceStateMap& updateDeviceStateMapValue) {
    setDeviceStateMapAttribute(updateDeviceStateMapValue);
}

void TestAudioControllerStubImpl::updateStringToShortMapAttr(const v2_Test_Audio::Controller::StringToShortMap& updateStringToShortMapValue) {
    setStringToShortMapAttribute(updateStringToShortMapValue);
}

void TestAudioControllerStubImpl::fireFocusStatusChanged(const std::vector<v2_Test_Audio::Controller::FocusStatus>& focusStatusVec) {
    fireFocusStatusChangedEvent(focusStatusVec);
}

void TestAudioControllerStubImpl::ObtainFocus(const std::shared_ptr<CommonAPI::ClientId> client, std::string audioSourceType, v2_Test_Audio::Controller::AudioDeviceType device, ObtainFocusReply_t reply) {
    if (auto handler = mHandler.lock()) {
    	v2_Test_Audio::Controller::FocusControlError errFocusControlError;
		v2_Test_Audio::Controller::AudioFocusResultType replyRes;
		handler->onObtainFocus(audioSourceType, device, errFocusControlError, replyRes);
		reply(errFocusControlError, replyRes);
    }
}

void TestAudioControllerStubImpl::ReleaseFocus(const std::shared_ptr<CommonAPI::ClientId> client, std::string audioSourceType, v2_Test_Audio::Controller::AudioDeviceType device, ReleaseFocusReply_t reply) {
    if (auto handler = mHandler.lock()) {
    	v2_Test_Audio::Controller::FocusControlError errFocusControlError;
		handler->onReleaseFocus(audioSourceType, device, errFocusControlError);
		reply(errFocusControlError);
    }
}

void TestAudioControllerStubImpl::SetVolume(const std::shared_ptr<CommonAPI::ClientId> client, uint8_t volume, SetVolumeReply_t reply) {
    if (auto handler = mHandler.lock()) {
    	v2_Test_Audio::Controller::VolumeControlError errVolumeControlError;
		handler->onSetVolume(volume, errVolumeControlError);
		reply(errVolumeControlError);
    }
}

void TestAudioControllerStubImpl::SetSourceVolume(const std::shared_ptr<CommonAPI::ClientId> client, std::string audioSourceType, uint16_t volume, uint32_t maxVolume, SetSourceVolumeReply_t reply) {
    if (auto handler = mHandler.lock()) {
    	v2_Test_Audio::Controller::VolumeControlError errVolumeControlError;
		handler->onSetSourceVolume(audioSourceType, volume, maxVolume, errVolumeControlError);
		reply(errVolumeControlError);
    }
}

void TestAudioControllerStubImpl::GetAudioSourceStatus(const std::shared_ptr<CommonAPI::ClientId> client, std::string audioSourceType, GetAudioSourceStatusReply_t reply) {
    if (auto handler = mHandler.lock()) {
    	v2_Test_Audio::Controller::ErrorType errErrorType;
		v2_Test_Audio::Controller::AudioSourceStatusInfo replyAudioSourceStatusInfo;
		handler->onGetAudioSourceStatus(audioSourceType, errErrorType, replyAudioSourceStatusInfo);
		reply(errErrorType, replyAudioSourceStatusInfo);
    }
}

} //namespace core
} //namespace ai
} //namespace umos
} //namespace soa
