/*
 * Copyright (c) 2023 42dot All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <SoaClientManager.h>

#include <logging.h>

namespace ai {
namespace umos {
namespace soa {
namespace core {

SoaClientManager::SoaClientManager()
    : mCommonApiRuntime(CommonAPI::Runtime::get()) {
}

TestAudioControllerClientPtr SoaClientManager::startTestAudioControllerClient() {
    LOGI("SoaClientManager::%s", __func__);
    if (mTestAudioControllerClient) {
        return mTestAudioControllerClient;
    }

    mTestAudioControllerClient = std::make_shared<TestAudioControllerClient>(mCommonApiRuntime);
    return mTestAudioControllerClient;
}

void SoaClientManager::stopTestAudioControllerClient() {
    LOGI("SoaClientManager::%s", __func__);
    if (mTestAudioControllerClient == nullptr) {
        return ;
    }

    mTestAudioControllerClient->disconnectServer();
    mTestAudioControllerClient.reset();
}

} //namespace core
} //namespace ai
} //namespace umos
} //namespace soa
