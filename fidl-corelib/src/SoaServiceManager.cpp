/*
 * Copyright (c) 2023 42dot All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <SoaConstants.h>
#include <SoaServiceManager.h>

#include <logging.h>
#include <thread>

namespace ai {
namespace umos {
namespace soa {
namespace core {

SoaServiceManager::SoaServiceManager()
    : mCommonApiRuntime(CommonAPI::Runtime::get()) {
}

SoaServiceManager::~SoaServiceManager() {
    stopAllService();
}

TestAudioControllerStubImplPtr SoaServiceManager::startTestAudioControllerService(const ITestAudioControllerHandlerPtr& handler) {
    if (mTestAudioControllerStub) {
        LOGI("TestAudioControllerService is already running");
        return mTestAudioControllerStub;
    }

    mTestAudioControllerStub = std::make_shared<TestAudioControllerStubImpl>(handler);
    registerService(SOA_TEST_AUDIO_CONTROLLER_SERVICE_INSTANCE, mTestAudioControllerStub);
    return mTestAudioControllerStub;
}

void SoaServiceManager::stopTestAudioControllerService() {
    if (mTestAudioControllerStub == nullptr) {
        return ;
    }

    unregisterService(TestAudioControllerStubImpl::StubInterface::getInterface(), SOA_TEST_AUDIO_CONTROLLER_SERVICE_INSTANCE);
    mTestAudioControllerStub.reset();
}

// ---private
void SoaServiceManager::stopAllService() {
	stopTestAudioControllerService();
}

template <typename CommonApiStub>
void SoaServiceManager::registerService(const std::string& instance, std::shared_ptr<CommonApiStub> service) {
    while (!mCommonApiRuntime->registerService(SOA_SERVICE_DOMAIN,
            instance, service)) {
        LOGI("Waiting for the registration to %s", instance.c_str());
        std::this_thread::sleep_for(std::chrono::milliseconds(200));
    }
    LOGI("%s Service has been successfully registered", instance.c_str());
    mServiceMap[instance] = service;
}

void SoaServiceManager::unregisterService(const std::string& interface, const std::string& instance) {
    if (mCommonApiRuntime->unregisterService(SOA_SERVICE_DOMAIN, interface, instance)) {
        LOGI("Service [%s:%s] has been unregistered", interface.c_str(), instance.c_str());
    }
}

} //namespace core
} //namespace ai
} //namespace umos
} //namespace soa
