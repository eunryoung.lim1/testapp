/*
 * Copyright (c) 2023 42dot All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include <unordered_map>

#include <CommonAPI/CommonAPI.hpp>
#include <v2/Test/Audio/ControllerProxy.hpp>
#include <v2/Test/Audio/ControllerProxyBase.hpp>

#include <common/IServiceStatusListener.h>
#include <Test/Audio/ITestAudioControllerAttrListener.h>
#include <Test/Audio/ITestAudioControllerEventListener.h>

namespace ai {
namespace umos {
namespace soa {
namespace core {

namespace v2_Test_Audio = v2::Test::Audio;

class TestAudioControllerClient {
public:
    explicit TestAudioControllerClient(const std::shared_ptr<CommonAPI::Runtime>& runtime);
    ~TestAudioControllerClient();

    using ConnectionCallback = std::function<void(const bool connected)>;
    void connectServer(const IServiceStatusListenerPtr& serviceStatusListener);
	void disconnectServer();

	void setAttributeListener(const ITestAudioControllerAttrListenerPtr& listener);
	void setBroadcastEventListener(const ITestAudioControllerEventListenerWPtr& EventListener);

	bool setIVIDeviceInfoAttr(const v2_Test_Audio::Controller::KeyValueString& newIVIDeviceInfoValue, v2_Test_Audio::Controller::KeyValueString& responseValue);
	void setIVIDeviceInfoAttrAsync(const v2_Test_Audio::Controller::KeyValueString& newIVIDeviceInfoValue, v2_Test_Audio::ControllerProxyBase::IVIDeviceInfoAttribute::AttributeAsyncCallback callback);
	bool getIVIDeviceInfoAttr(v2_Test_Audio::Controller::KeyValueString& getValue);
	void getIVIDeviceInfoAttrAsync(v2_Test_Audio::ControllerProxyBase::IVIDeviceInfoAttribute::AttributeAsyncCallback callback);
	bool getAudioSourceTypesAttr(std::vector<std::string>& getValue);
	void getAudioSourceTypesAttrAsync(v2_Test_Audio::ControllerProxyBase::AudioSourceTypesAttribute::AttributeAsyncCallback callback);
	bool getVolumeInfoAttr(v2_Test_Audio::Controller::VolumeInfo& getValue);
	void getVolumeInfoAttrAsync(v2_Test_Audio::ControllerProxyBase::VolumeInfoAttribute::AttributeAsyncCallback callback);
	bool getKeyValueByteBufferMapAttr(v2_Test_Audio::Controller::KeyValueByteBuffer& getValue);
	void getKeyValueByteBufferMapAttrAsync(v2_Test_Audio::ControllerProxyBase::KeyValueByteBufferMapAttribute::AttributeAsyncCallback callback);
	bool getDeviceStateMapAttr(v2_Test_Audio::Controller::DeviceStateMap& getValue);
	void getDeviceStateMapAttrAsync(v2_Test_Audio::ControllerProxyBase::DeviceStateMapAttribute::AttributeAsyncCallback callback);
	bool getStringToShortMapAttr(v2_Test_Audio::Controller::StringToShortMap& getValue);
	void getStringToShortMapAttrAsync(v2_Test_Audio::ControllerProxyBase::StringToShortMapAttribute::AttributeAsyncCallback callback);
	bool ObtainFocus(const std::string& audioSourceType, const v2_Test_Audio::Controller::AudioDeviceType& device, v2_Test_Audio::Controller::FocusControlError& errFocusControlError, v2_Test_Audio::Controller::AudioFocusResultType& replyRes);
	void ObtainFocusAsync(const std::string& audioSourceType, const v2_Test_Audio::Controller::AudioDeviceType& device, v2_Test_Audio::ControllerProxyBase::ObtainFocusAsyncCallback callback);
	bool ReleaseFocus(const std::string& audioSourceType, const v2_Test_Audio::Controller::AudioDeviceType& device, v2_Test_Audio::Controller::FocusControlError& errFocusControlError);
	void ReleaseFocusAsync(const std::string& audioSourceType, const v2_Test_Audio::Controller::AudioDeviceType& device, v2_Test_Audio::ControllerProxyBase::ReleaseFocusAsyncCallback callback);
	bool SetVolume(uint8_t volume, v2_Test_Audio::Controller::VolumeControlError& errVolumeControlError);
	void SetVolumeAsync(uint8_t volume, v2_Test_Audio::ControllerProxyBase::SetVolumeAsyncCallback callback);
	bool SetSourceVolume(const std::string& audioSourceType, uint16_t volume, uint32_t maxVolume, v2_Test_Audio::Controller::VolumeControlError& errVolumeControlError);
	void SetSourceVolumeAsync(const std::string& audioSourceType, uint16_t volume, uint32_t maxVolume, v2_Test_Audio::ControllerProxyBase::SetSourceVolumeAsyncCallback callback);
	bool GetAudioSourceStatus(const std::string& audioSourceType, v2_Test_Audio::Controller::ErrorType& errErrorType, v2_Test_Audio::Controller::AudioSourceStatusInfo& replyAudioSourceStatusInfo);
	void GetAudioSourceStatusAsync(const std::string& audioSourceType, v2_Test_Audio::ControllerProxyBase::GetAudioSourceStatusAsyncCallback callback);
private:
    void init();
	void subscribeServerEvents();
	void resetAttributeListener();
	void resetBroadcastEventListener();

    std::shared_ptr<CommonAPI::Runtime> mCommonApiRuntime;
    std::shared_ptr<v2_Test_Audio::ControllerProxy<>>	mProxyServer;
    std::thread								mServerConnectionThread;
	IServiceStatusListenerWPtr				mServiceStatusListener;
	// attribute event listener
	ITestAudioControllerAttrListenerWPtr		mAttrListener;
	std::mutex									mAttrSubscriptionMapMutex;
	std::unordered_map<std::string, CommonAPI::Event<uint8_t>::Subscription> mAttrSubscriptionMap;
	// broadcast event listener
	ITestAudioControllerEventListenerWPtr		mEventListener;
	std::mutex									mEventSubscriptionMapMutex;
	std::unordered_map<std::string, CommonAPI::Event<uint8_t>::Subscription> mEventSubscriptionMap;
};

using TestAudioControllerClientPtr = std::shared_ptr<TestAudioControllerClient>;

} //namespace core
} //namespace ai
} //namespace umos
} //namespace soa

