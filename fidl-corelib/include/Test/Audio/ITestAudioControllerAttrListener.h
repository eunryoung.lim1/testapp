/*
 * Copyright (c) 2023 42dot All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include <v2/Test/Audio/ControllerProxy.hpp>

namespace v2_Test_Audio = v2::Test::Audio;
using namespace v2_Test_Audio;

namespace ai {
namespace umos {
namespace soa {
namespace core {

class ITestAudioControllerAttrListener {
public:
    ITestAudioControllerAttrListener() = default;
    virtual ~ITestAudioControllerAttrListener() = default;

	/**
	@description: test-purposed attribute which is read-write and map data type
	**/
	virtual void onIVIDeviceInfoChanged(const v2_Test_Audio::Controller::KeyValueString& iVIDeviceInfo) = 0;

	/**
	@description: Provides a list of Audio Source Types for ccRC(Rear Seat) System which is provided by the head unit system. 
	The list provided to the entire system depending on the resource configuration may be different from the example below.
	The list has the following format and is a string with upcase letters.
	-"YOUTUBE_STREAMING"
	-"TVING_STREAMING"
	-"WATCHAR_STREAMING"
	**/
	virtual void onAudioSourceTypesChanged(const std::vector<std::string>& audioSourceTypesVec) = 0;

	/**
	@description: Contains the Volume Information include min and max levels.
	The range of these variables depends on each platform specification.
	-Volume range is defined by audio specification.
	-MaxSoftVolume is setted by application which has specific requirements for volume limitation.
	-LastVolume can be used when ccRC need to store current volume by launching specific functions.
	-Mute notify the current mute status. (true : Mute / false : Unmute)
	**/
	virtual void onVolumeInfoChanged(const v2_Test_Audio::Controller::VolumeInfo& volumeInfo) = 0;

	/**
	@description: Contains the Volume Information include min and max levels.
	The range of these variables depends on each platform specification.
	-Volume range is defined by audio specification.
	-MaxSoftVolume is setted by application which has specific requirements for volume limitation.
	-LastVolume can be used when ccRC need to store current volume by launching specific functions.
	-Mute notify the current mute status. (true : Mute / false : Unmute)
	**>
	attribute VolumeInfo VolumeInfo readonly
	**/
	virtual void onKeyValueByteBufferMapChanged(const v2_Test_Audio::Controller::KeyValueByteBuffer& keyValueByteBufferMap) = 0;

	/**
	@description: Contains the Volume Information include min and max levels.
	The range of these variables depends on each platform specification.
	-Volume range is defined by audio specification.
	-MaxSoftVolume is setted by application which has specific requirements for volume limitation.
	-LastVolume can be used when ccRC need to store current volume by launching specific functions.
	-Mute notify the current mute status. (true : Mute / false : Unmute)
	**>
	attribute VolumeInfo VolumeInfo readonly
	attribute KeyValueByteBuffer KeyValueByteBufferMap readonly
	**/
	virtual void onDeviceStateMapChanged(const v2_Test_Audio::Controller::DeviceStateMap& deviceStateMap) = 0;

	/**
	@description: Contains the Volume Information include min and max levels.
	The range of these variables depends on each platform specification.
	-Volume range is defined by audio specification.
	-MaxSoftVolume is setted by application which has specific requirements for volume limitation.
	-LastVolume can be used when ccRC need to store current volume by launching specific functions.
	-Mute notify the current mute status. (true : Mute / false : Unmute)
	**>
	attribute VolumeInfo VolumeInfo readonly
	attribute KeyValueByteBuffer KeyValueByteBufferMap readonly
	attribute DeviceStateMap DeviceStateMap readonly
	**/
	virtual void onStringToShortMapChanged(const v2_Test_Audio::Controller::StringToShortMap& stringToShortMap) = 0;

};

using ITestAudioControllerAttrListenerPtr = std::shared_ptr<ITestAudioControllerAttrListener>;
using ITestAudioControllerAttrListenerWPtr = std::weak_ptr<ITestAudioControllerAttrListener>;

} //namespace core
} //namespace ai
} //namespace umos
} //namespace soa

