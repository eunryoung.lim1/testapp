/*
 * Copyright (c) 2023 42dot All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include <CommonAPI/CommonAPI.hpp>
#include <CommonAPI/Export.hpp>
#include <v2/Test/Audio/ControllerStub.hpp>
#include <v2/Test/Audio/ControllerStubDefault.hpp>

#include <Test/Audio/ITestAudioControllerHandler.h>

#include <SoaBaseService.h>

namespace ai {
namespace umos {
namespace soa {
namespace core {

namespace v2_Test_Audio = v2::Test::Audio;

class TestAudioControllerStubImpl final
    : public SoaBaseService
    , public v2_Test_Audio::ControllerStubDefault {
public:
    explicit TestAudioControllerStubImpl(const ITestAudioControllerHandlerPtr& handler);
    ~TestAudioControllerStubImpl();

	void updateIVIDeviceInfoAttr(const v2_Test_Audio::Controller::KeyValueString& updateIVIDeviceInfoValue);
	void updateAudioSourceTypesAttr(const std::vector<std::string>& updateAudioSourceTypesVecValue);
	void updateVolumeInfoAttr(const v2_Test_Audio::Controller::VolumeInfo& updateVolumeInfoValue);
	void updateKeyValueByteBufferMapAttr(const v2_Test_Audio::Controller::KeyValueByteBuffer& updateKeyValueByteBufferMapValue);
	void updateDeviceStateMapAttr(const v2_Test_Audio::Controller::DeviceStateMap& updateDeviceStateMapValue);
	void updateStringToShortMapAttr(const v2_Test_Audio::Controller::StringToShortMap& updateStringToShortMapValue);
	void fireFocusStatusChanged(const std::vector<v2_Test_Audio::Controller::FocusStatus>& focusStatusVec);

	/**
	@description: The ccRC system requests to obtain audio focus.
	**/
	void ObtainFocus(const std::shared_ptr<CommonAPI::ClientId> client, std::string audioSourceType, v2_Test_Audio::Controller::AudioDeviceType device, ObtainFocusReply_t reply) override;

	/**
	@description: The ccRC system requests to release audio focus.  Requests that cannot be released return errors.
	**/
	void ReleaseFocus(const std::shared_ptr<CommonAPI::ClientId> client, std::string audioSourceType, v2_Test_Audio::Controller::AudioDeviceType device, ReleaseFocusReply_t reply) override;

	/**
	@description: Set the volume for outside the system.
	**/
	void SetVolume(const std::shared_ptr<CommonAPI::ClientId> client, uint8_t volume, SetVolumeReply_t reply) override;

	/**
	@description: request to set source volume.
	There are two member field in the Input parameter.
	- AudioSourceType: please see "AudioSourceTypes []" for finding detail description
	- volume : it holds volume level and its level is depending on platform's audio  spec. (varies based on platform, model, misc.)
	 : when this method is invoked, audio manager need to update VolumeInfo.LastVolume
	- maxVolume : it defines volume limitation which is described from the requirement
	- VolumeControlError : please see "setVolume" for more detail explanation
	**/
	void SetSourceVolume(const std::shared_ptr<CommonAPI::ClientId> client, std::string audioSourceType, uint16_t volume, uint32_t maxVolume, SetSourceVolumeReply_t reply) override;

	/**
	@description: Get FocusStatus and VolumeInfo for audio source type
	**/
	void GetAudioSourceStatus(const std::shared_ptr<CommonAPI::ClientId> client, std::string audioSourceType, GetAudioSourceStatusReply_t reply) override;

private:
    ITestAudioControllerHandlerWPtr				mHandler;

};

using TestAudioControllerStubImplPtr = std::shared_ptr<TestAudioControllerStubImpl>;

} //namespace core
} //namespace ai
} //namespace umos
} //namespace soa
