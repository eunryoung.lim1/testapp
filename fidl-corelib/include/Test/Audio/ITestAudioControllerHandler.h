/*
 * Copyright (c) 2023 42dot All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include <v2/Test/Audio/Controller.hpp>

namespace v2_Test_Audio = v2::Test::Audio;
using namespace v2_Test_Audio;

namespace ai {
namespace umos {
namespace soa {
namespace core {

class ITestAudioControllerHandler {
public:
    virtual ~ ITestAudioControllerHandler() = default;

	/**
	@description: The ccRC system requests to obtain audio focus.
	**/
	virtual void onObtainFocus(const std::string& audioSourceType, const v2_Test_Audio::Controller::AudioDeviceType& device, v2_Test_Audio::Controller::FocusControlError& errFocusControlError, v2_Test_Audio::Controller::AudioFocusResultType& replyRes) = 0;

	/**
	@description: The ccRC system requests to release audio focus.  Requests that cannot be released return errors.
	**/
	virtual void onReleaseFocus(const std::string& audioSourceType, const v2_Test_Audio::Controller::AudioDeviceType& device, v2_Test_Audio::Controller::FocusControlError& errFocusControlError) = 0;

	/**
	@description: Set the volume for outside the system.
	**/
	virtual void onSetVolume(uint8_t volume, v2_Test_Audio::Controller::VolumeControlError& errVolumeControlError) = 0;

	/**
	@description: request to set source volume.
	There are two member field in the Input parameter.
	- AudioSourceType: please see "AudioSourceTypes []" for finding detail description
	- volume : it holds volume level and its level is depending on platform's audio  spec. (varies based on platform, model, misc.)
	 : when this method is invoked, audio manager need to update VolumeInfo.LastVolume
	- maxVolume : it defines volume limitation which is described from the requirement
	- VolumeControlError : please see "setVolume" for more detail explanation
	**/
	virtual void onSetSourceVolume(const std::string& audioSourceType, uint16_t volume, uint32_t maxVolume, v2_Test_Audio::Controller::VolumeControlError& errVolumeControlError) = 0;

	/**
	@description: Get FocusStatus and VolumeInfo for audio source type
	**/
	virtual void onGetAudioSourceStatus(const std::string& audioSourceType, v2_Test_Audio::Controller::ErrorType& errErrorType, v2_Test_Audio::Controller::AudioSourceStatusInfo& replyAudioSourceStatusInfo) = 0;

};

using ITestAudioControllerHandlerPtr = std::shared_ptr<ITestAudioControllerHandler>;
using ITestAudioControllerHandlerWPtr = std::weak_ptr<ITestAudioControllerHandler>;

} //namespace core
} //namespace ai
} //namespace umos
} //namespace soa

