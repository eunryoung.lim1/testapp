/*
 * Copyright (c) 2023 42dot All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include <v2/Test/Audio/ControllerProxy.hpp>

namespace v2_Test_Audio = v2::Test::Audio;
using namespace v2_Test_Audio;

namespace ai {
namespace umos {
namespace soa {
namespace core {

class ITestAudioControllerEventListener {
public:
    ITestAudioControllerEventListener() = default;
    virtual ~ITestAudioControllerEventListener() = default;

	/**
	@description: broadcast when audio source type changed
	**/
	virtual void onFocusStatusChangedReceived(const std::vector<v2_Test_Audio::Controller::FocusStatus>& focusStatusVec) = 0;

};

using ITestAudioControllerEventListenerPtr = std::shared_ptr<ITestAudioControllerEventListener>;
using ITestAudioControllerEventListenerWPtr = std::weak_ptr<ITestAudioControllerEventListener>;

} //namespace core
} //namespace ai
} //namespace umos
} //namespace soa

