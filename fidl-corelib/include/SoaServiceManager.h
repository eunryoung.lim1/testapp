/*
 * Copyright (c) 2023 42dot All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include <unordered_map>

#include <CommonAPI/CommonAPI.hpp>

#include <Test/Audio/TestAudioControllerStubImpl.h>

#include "SoaBaseService.h"

namespace ai {
namespace umos {
namespace soa {
namespace core {

class SoaServiceManager final {
public:
    SoaServiceManager();
    ~SoaServiceManager();

    TestAudioControllerStubImplPtr			startTestAudioControllerService(const ITestAudioControllerHandlerPtr& handler);
	
	void stopTestAudioControllerService();
private:
    void stopAllService();

    template <typename CommonApiStub>
    void registerService(const std::string& instance, std::shared_ptr<CommonApiStub> service);
    void unregisterService(const std::string& interface, const std::string& instance);

    std::shared_ptr<CommonAPI::Runtime>     mCommonApiRuntime;
    TestAudioControllerStubImplPtr				mTestAudioControllerStub;

    //instance service, service
    std::unordered_map<std::string, SoaBaseServicePtr> mServiceMap;
};

using SoaServiceManagerUPtr = std::unique_ptr<SoaServiceManager>;
using SoaServiceManagerPtr = std::shared_ptr<SoaServiceManager>;

} //namespace core
} //namespace ai
} //namespace umos
} //namespace soa
