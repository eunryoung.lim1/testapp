/*
 * Copyright (c) 2023 42dot All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#define  LOG_TAG    "FIDL_SOA_LOG"

#ifdef ANDROID
    #include <android/log.h>
    #define  LOGUNK(...)  __android_log_print(ANDROID_LOG_UNKNOWN,LOG_TAG,__VA_ARGS__)
    #define  LOGDEF(...)  __android_log_print(ANDROID_LOG_DEFAULT,LOG_TAG,__VA_ARGS__)
    #define  LOGV(...)  __android_log_print(ANDROID_LOG_VERBOSE,LOG_TAG,__VA_ARGS__)
    #define  LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG,LOG_TAG,__VA_ARGS__)
    #define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
    #define  LOGW(...)  __android_log_print(ANDROID_LOG_WARN,LOG_TAG,__VA_ARGS__)
    #define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)
    #define  LOGF(...)  __android_log_print(ANDROID_FATAL_ERROR,LOG_TAG,__VA_ARGS__)
    #define  LOGS(...)  __android_log_print(ANDROID_SILENT_ERROR,LOG_TAG,__VA_ARGS__)
#else
    #include <stdio.h>
    #include <stdarg.h>

    #define LOG_UNKNOWN 0
    #define LOG_DEFAULT 1
    #define LOG_VERBOSE 2
    #define LOG_DEBUG   3
    #define LOG_INFO    4
    #define LOG_WARN    5
    #define LOG_ERROR   6
    #define LOG_FATAL   7
    #define LOG_SILENT  8

    #define LOGUNK(fmt, ...) fprintf(stderr, fmt, ##__VA_ARGS__)
    #define LOGDEF(fmt, ...) fprintf(stderr, fmt, ##__VA_ARGS__)
    #define LOGV(fmt, ...) logWithLevel(LOG_VERBOSE, fmt, ##__VA_ARGS__)
    #define LOGD(fmt, ...) logWithLevel(LOG_DEBUG, fmt, ##__VA_ARGS__)
    #define LOGI(fmt, ...) logWithLevel(LOG_INFO, fmt, ##__VA_ARGS__)
    #define LOGW(fmt, ...) logWithLevel(LOG_WARN, fmt, ##__VA_ARGS__)
    #define LOGE(fmt, ...) logWithLevel(LOG_ERROR, fmt, ##__VA_ARGS__)
    #define LOGF(fmt, ...) logWithLevel(LOG_FATAL, fmt, ##__VA_ARGS__)
    #define LOGS(fmt, ...) logWithLevel(LOG_SILENT, fmt, ##__VA_ARGS__)

    #define logWithLevel(level, fmt, ...)                          \
        do {                                                       \
            if (level >= LOG_DEFAULT) {                            \
                fprintf(stderr, "[%s] ", LOG_TAG);                 \
            }                                                      \
            fprintf(stderr, fmt, ##__VA_ARGS__);                   \
        } while (0)
#endif