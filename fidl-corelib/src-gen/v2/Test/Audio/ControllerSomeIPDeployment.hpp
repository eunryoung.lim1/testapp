/*
 * This file was generated by the CommonAPI Generators.
 * Used org.genivi.commonapi.someip 3.2.0.v202012010944.
 * Used org.franca.core 0.13.1.201807231814.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * http://mozilla.org/MPL/2.0/.
 */

#ifndef V2_TEST_AUDIO_Controller_SOMEIP_DEPLOYMENT_HPP_
#define V2_TEST_AUDIO_Controller_SOMEIP_DEPLOYMENT_HPP_


#if !defined (COMMONAPI_INTERNAL_COMPILATION)
#define COMMONAPI_INTERNAL_COMPILATION
#define HAS_DEFINED_COMMONAPI_INTERNAL_COMPILATION_HERE
#endif
#include <CommonAPI/SomeIP/Deployment.hpp>
#if defined (HAS_DEFINED_COMMONAPI_INTERNAL_COMPILATION_HERE)
#undef COMMONAPI_INTERNAL_COMPILATION
#undef HAS_DEFINED_COMMONAPI_INTERNAL_COMPILATION_HERE
#endif

namespace v2 {
namespace Test {
namespace Audio {
namespace Controller_ {

// Interface-specific deployment types
typedef CommonAPI::SomeIP::EnumerationDeployment<uint32_t> ErrorTypeDeployment_t;
typedef CommonAPI::SomeIP::EnumerationDeployment<uint32_t> AudioDeviceTypeDeployment_t;
typedef CommonAPI::SomeIP::EnumerationDeployment<uint32_t> AudioFocusResultTypeDeployment_t;
typedef CommonAPI::SomeIP::EnumerationDeployment<uint32_t> FocusControlErrorDeployment_t;
typedef CommonAPI::SomeIP::EnumerationDeployment<uint32_t> AudioFocusStatusTypeDeployment_t;
typedef CommonAPI::SomeIP::EnumerationDeployment<uint32_t> VolumeControlErrorDeployment_t;
typedef CommonAPI::SomeIP::EnumerationDeployment<uint32_t> VolumeControlDeployment_t;
typedef CommonAPI::SomeIP::EnumerationDeployment<uint32_t> AudioChannelDeployment_t;
typedef CommonAPI::SomeIP::StructDeployment<
    CommonAPI::SomeIP::StringDeployment,
    CommonAPI::SomeIP::StructDeployment<
        CommonAPI::SomeIP::StringDeployment,
        ::v2::Test::Audio::Controller_::AudioDeviceTypeDeployment_t,
        ::v2::Test::Audio::Controller_::AudioFocusStatusTypeDeployment_t
    >,
    CommonAPI::SomeIP::StructDeployment<
        CommonAPI::SomeIP::IntegerDeployment<uint8_t>,
        CommonAPI::SomeIP::IntegerDeployment<uint8_t>,
        CommonAPI::SomeIP::IntegerDeployment<uint8_t>,
        CommonAPI::EmptyDeployment
    >
> AudioSourceStatusInfoDeployment_t;
typedef CommonAPI::SomeIP::StructDeployment<
    CommonAPI::SomeIP::StringDeployment,
    ::v2::Test::Audio::Controller_::AudioDeviceTypeDeployment_t,
    ::v2::Test::Audio::Controller_::AudioFocusStatusTypeDeployment_t
> FocusStatusDeployment_t;
typedef CommonAPI::SomeIP::StructDeployment<
    CommonAPI::SomeIP::IntegerDeployment<uint8_t>,
    CommonAPI::SomeIP::IntegerDeployment<uint8_t>,
    CommonAPI::SomeIP::IntegerDeployment<uint8_t>,
    CommonAPI::EmptyDeployment
> VolumeInfoDeployment_t;
typedef CommonAPI::SomeIP::MapDeployment<
    CommonAPI::SomeIP::StringDeployment,
    CommonAPI::SomeIP::StringDeployment
> KeyValueStringDeployment_t;
typedef CommonAPI::SomeIP::MapDeployment<
    ::v2::Test::Audio::Controller_::AudioDeviceTypeDeployment_t,
    ::v2::Test::Audio::Controller_::AudioFocusStatusTypeDeployment_t
> DeviceStateMapDeployment_t;
typedef CommonAPI::SomeIP::MapDeployment<
    CommonAPI::SomeIP::IntegerDeployment<uint32_t>,
    CommonAPI::SomeIP::ByteBufferDeployment
> KeyValueByteBufferDeployment_t;
typedef CommonAPI::SomeIP::MapDeployment<
    CommonAPI::SomeIP::StringDeployment,
    CommonAPI::SomeIP::IntegerDeployment<uint16_t>
> StringToShortMapDeployment_t;

// Type-specific deployments

// Attribute-specific deployments

// Argument-specific deployment

// Broadcast-specific deployments

} // namespace Controller_
} // namespace Audio
} // namespace Test
} // namespace v2

#endif // V2_TEST_AUDIO_Controller_SOMEIP_DEPLOYMENT_HPP_
